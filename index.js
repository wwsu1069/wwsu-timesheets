"use strict";

// Initialize unhandled exceptions handler first
const unhandled = require("electron-unhandled");
unhandled();

// Modules to control application life and create native browser window
const {
  app,
  BrowserWindow,
  Menu,
  ipcMain,
  session,
  shell,
  screen,
  powerMonitor,
  powerSaveBlocker,
} = require("electron");
const path = require("path");

const debug = require("electron-debug");
const contextMenu = require("electron-context-menu");
const packageJson = require("./package.json");
const { machineIdSync } = require("./assets/wwsu-host-id");
const semver = require("semver");
const jwt = require("jsonwebtoken");

// Initialize debug tools
debug();

// Initialize context menu
contextMenu();

// Set app ID
app.setAppUserModelId(packageJson.appId);

let mainWindow;

// Create the main window, and also create the other renderer processes
const createWindows = () => {
  if (mainWindow) return;

  // Create the browser window.
  mainWindow = new BrowserWindow({
    show: false, // Do not show until we are ready to show via ready-to-show event
    title: app.name,
    autoHideMenuBar: true, // Do not show manu bar unless alt is pressed
    fullscreen: true,
    kiosk: true,
    webPreferences: {
      contextIsolation: true,
      enableRemoteModule: false, // electron's remote module is insecure
      backgroundThrottling: false, // Do not throttle this process because it causes timer problems when hidden for the lockddown screen.
      preload: path.join(__dirname, "preload-renderer.js"),
      disableBlinkFeatures: "Auxclick", // AUXCLICK_JS_CHECK
      sandbox: true,
    },
  });

  // Do not show the window until DOM has loaded. Otherwise, we will get a white flash effect that is not pleasant.
  // Also, do not load other processes until renderer is ready.
  mainWindow.once("ready-to-show", () => {
    // Show the window and check for updates
    mainWindow.show();
  });

  // and load the renderer.html of the app.
  mainWindow.loadFile("index.html");

  // When mainWindow is closed, all other processes should also be closed
  mainWindow.on("closed", function () {
    mainWindow = null;
  });

  // Renderer should not allow navigation to any external websites
  mainWindow.webContents.on("will-navigate", (event, newURL) => {
    event.preventDefault(); // AUXCLICK_JS_CHECK
  });
};

// Enforce sandboxing for security (every process is in its own isolated environment)
app.enableSandbox();

// Prevent multiple instances of the app from running
if (!app.requestSingleInstanceLock()) {
  app.quit();
}

// If a second instance is spawned, show the current instance in the event it is minimized or hidden
app.on("second-instance", () => {
  if (mainWindow) {
    if (mainWindow.isMinimized()) {
      mainWindow.restore();
    }

    mainWindow.show();
  }
});

// Quit the app if all windows are closed
app.on("window-all-closed", () => {
  app.quit();
});

// If we do not have a mainWindow for whatever reason when the app is activated, restart the main processes and windows
app.on("activate", () => {
  if (!mainWindow) {
    createWindows();
  }
});

// Prevent opening new windows in the app browsers; use the default browser instead
app.on("web-contents-created", (event, contents) => {
  contents.on("new-window", async (event, navigationUrl) => {
    // In this example, we'll ask the operating system
    // to open this event's url in the default browser.
    event.preventDefault();

    // Do not navigate to anything other than https protocol sites
    if (!navigationUrl.startsWith("https://")) return;

    await shell.openExternal(navigationUrl);
  });
});

// Start loading the app
app.whenReady().then(createWindows);

/*
	IPC COMMUNICATIONS
*/

// Sync get the machine ID string for this installation
ipcMain.on("get-machine-id", (event) => {
	event.returnValue = machineIdSync();
});

// Sync Get the app and version info
ipcMain.on("get-app-version", (event) => {
	event.returnValue = `${packageJson.name} v${packageJson.version}`;
});

// Sync check if a version is newer than our version
ipcMain.on("check-version", (event, arg) => {
	if (semver.gt(arg, packageJson.version)) {
		event.returnValue = { current: packageJson.version, isOld: true };
	}
	event.returnValue = { current: packageJson.version, isOld: false };
});