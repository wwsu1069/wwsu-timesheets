"use strict";

// This class manages WWSU hosts

// REQUIRES these WWSUmodules: WWSUMeta, WWSUrecipients, WWSUdjs, hostReq (WWSUreq), directorReq (WWSUreq), djReq (WWSUreq), WWSUutil, WWSUanimations, WWSUdirectors (when editing hosts)
class WWSUhosts extends WWSUdb {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 * @param {string} options.machineID The ID of this machine / host
	 * @param {string} options.app The app name and version running on this host
	 */
	constructor(manager, options) {
		super("WWSUhosts"); // Create the db

		this.manager = manager;
		this.options = options;
		this.host = this.options.machineID;

		this.endpoints = {
			edit: "PUT /api/hosts/:ID",
			get: "GET /api/hosts/:host",
			remove: "DELETE /api/hosts/:ID",
		};

		this.table;

		this.assignSocketEvent("hosts", this.manager.socket);

		// Contains information about the current host
		this.client = {};

		// Information pertaining to the lockdown system
		this.lockdown = {
			type: null, // Either "calendar" or "director"
			ID: null, // unique event string for calendar, or ID for director.
		};

		// Update client info if it changed
		this.on("update", "WWSUhosts", (record) => {
			if (record.host === this.host) {
				this.client = record;
				this.emitEvent("clientChanged", [record]);
				this.emitEvent("clientLockdown", [record.lockDown]);
				$(".host-friendlyname").html(record.friendlyname);
			}
		});
		this.on("remove", "WWSUhosts", (record) => {
			if (record.host === this.host) {
				this.client = {};
				this.emitEvent("clientChanged", [null]);
				this.emitEvent("clientLockdown", [null]);
				$(".host-friendlyname").html("Unknown Host");
			}
		});
		this.on("change", "WWSUhosts", (db) => {
			this.updateTable();
		});

		this.hostModal = new WWSUmodal(`Host`, null, ``, true, {
			overlayClose: false,
			zindex: 1100,
		});
	}

	/**
	 * Get / authorize this host in the WWSU API.
	 * This should be called BEFORE any other WWSU init functions are called.
	 *
	 * @param {function} cb Callback w/ parameter. 1 = authorized and connected. 0 = not authorized, -1 = authorized, but already connected
	 */
	get(cb) {
		this.manager
			.get("hostReq")
			.request(
				this.endpoints.get,
				{ data: { host: this.options.machineID, app: this.options.app } },
				{},
				(body, resp) => {
					if (resp.statusCode >= 400) {
						cb(0);
					} else {
						this.client = body;
						$(".host-friendlyname").html(body.friendlyname);

						if (!this.client.authorized) {
							cb(0);
						} else {
							this.emitEvent("clientLockdown", [this.client.lockDown]);
							if (body.otherHosts) {
								this.query(body.otherHosts, true);
								delete this.client.otherHosts;
							}
							this.manager
								.get("WWSUrecipients")
								.addRecipientComputer(
									this.client.host,
									(recipient, success) => {
										if (success) {
											cb(1);
										} else {
											cb(-1);
										}
									}
								);
						}
					}
				}
			);
	}

	/**
	 * Edit a host in the system
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	edit(data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.edit,
			{
				data: data,
			},
			{ domModal: this.hostModal.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Host Edited",
						autohide: true,
						delay: 15000,
						body: `Host has been edited. <strong>You may need to restart the application on the host for changes to take effect.</strong>`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Remove a host in the system
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	remove(data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.remove,
			{
				data: data,
			},
			{},
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Host Removed",
						autohide: true,
						delay: 10000,
						body: `Host has been removed`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Make a "Edit Host" Alpaca form in a modal.
	 */
	showHostForm(data) {
		this.hostModal.iziModal("open");
		this.hostModal.body = ``;

		let _djs = this.manager.get("WWSUdjs").find();
		_djs.push({
			ID: 0,
			name: "(does not belong to any org member nor WWSU)",
			realName: null,
		});

		let _directors = this.manager.get("WWSUdirectors").find();

		let rSilence = this.find({ silenceDetection: true }, true);
		let rRecord = this.find({ recordAudio: true }, true);
		let rDelay = this.find({ delaySystem: true }, true);
		let rEAS = this.find({ EAS: true }, true);

		$(this.hostModal.body).alpaca({
			schema: {
				title: data ? "Edit Host" : "New Host",
				type: "object",
				properties: {
					ID: {
						type: "number",
					},
					host: {
						type: "string",
						title: "Host ID",
						readonly: true,
					},
					app: {
						type: "string",
						title: "Application / Version",
						readonly: true,
					},
					friendlyname: {
						type: "string",
						required: true,
						title: "Friendly Name",
						maxLength: 255,
					},
					belongsTo: {
						type: "number",
						enum: _djs.map((dj) => dj.ID),
						title: "Belongs to Org Member",
					},
					authorized: {
						type: "boolean",
						title: "Is Authorized?",
					},
					admin: {
						type: "boolean",
						title: "Allow access to Administration Menu?",
					},
					lockDown: {
						type: "string",
						enum: ["prod", "onair", "director"],
						title: "Lock down the computer",
					},
					answerCalls: {
						type: "boolean",
						title: "Can broadcast remote audio?",
					},
					silenceDetection: {
						type: "boolean",
						title: "Monitor / Report Silence?",
						readonly: rSilence && data && rSilence.ID !== data.ID,
					},
					recordAudio: {
						type: "boolean",
						title: "Record audio?",
						readonly: rRecord && data && rRecord.ID !== data.ID,
					},
					delaySystem: {
						type: "boolean",
						title: "Delay System Connected?",
						readonly: rDelay && data && rDelay.ID !== data.ID,
					},
					EAS: {
						type: "boolean",
						title: "Emergency Alert System (EAS) Connected?",
						readonly: rEAS && data && rEAS.ID !== data.ID,
					},
				},
			},
			view: {
				wizard: {
					bindings: {
						ID: 1,
						host: 1,
						app: 1,
						friendlyname: 1,
						belongsTo: 2,
						authorized: 2,
						admin: 2,
						lockDown: 2,
						answerCalls: 2,
						silenceDetection: 3,
						recordAudio: 3,
						delaySystem: 3,
						EAS: 3,
					},
					steps: [
						{
							title: "Information",
						},
						{
							title: "Permissions",
						},
						{
							title: "Responsibilities",
						},
					],
				},
			},
			options: {
				fields: {
					ID: {
						type: "hidden",
					},
					host: {
						helpers: [
							"This string hash identifies the host depending on the application and the machine. It cannot be edited.",
							"<strong>The host ID should NEVER be given out publicly in full! Treat as a password.</strong>",
						],
					},
					app: {
						helper:
							"This is the name of the application and its version running on this host. It cannot be edited.",
					},
					friendlyname: {
						helper:
							"This is the visible name for this host. You should use something that easily identifies the computer / machine to others.",
					},
					belongsTo: {
						type: "select",
						helpers: [
							"(wwsu-dj-controls only) Choose which org member this host belongs to, if this host is not running on a WWSU computer. If the org member is not listed, please add or mark active the member in Administration -> Org Members.",
							"If the selected member is later marked inactive or deleted, the authorized setting for this host will automatically be turned off, and they can no longer connect to WWSU from their host.",
							"If the selected member is also a director, and the director is later deleted, it is assumed they are no longer a director, and access to the administration menu for this host will automatically be turned off.",
						],
						optionLabels: _djs.map((dj) => `${dj.name} (${dj.realName})`),
					},
					authorized: {
						rightLabel: "Yes",
						helper:
							"Check this box to allow this host to connect to WWSU. Otherwise, they cannot connect / do anything. You should not authorize unfamiliar hosts until you know it's a legitimate WWSU-affiliated person.",
					},
					admin: {
						rightLabel: "Yes",
						helper:
							"(wwsu-dj-controls only) If checked, administration menu items will be visible and accessible on this host. DO NOT allow this on hosts other than ones running on director computers.",
						validator: function (callback) {
							let value = this.getValue();
							let belongsTo =
								this.getParent().childrenByPropertyId["belongsTo"].getValue();

							if (belongsTo && belongsTo !== "" && value) {
								let _director;
								let _dj = _djs.find((dj) => dj.ID === belongsTo);
								if (_dj) {
									_director = _directors.find(
										(director) => director.name === _dj.realName
									);
								}

								if (!_director) {
									callback({
										message:
											"For security, the specified member this host belongs to is not a director. Therefore, you cannot allow access to the administration menu for this host.",
										status: false,
									});
									return;
								}
							}
							callback({
								status: true,
							});
						},
					},
					lockDown: {
						helpers: [
							"(wwsu-dj-controls only) none: This DJ Controls does not lock out the machine; anyone can use it at any time.",
							"prod: This DJ Controls locks out the machine and only allows directors, or DJs with a scheduled prod booking, to use it.",
							"onair: This DJ Controls locks out the machine and only allows directors, or DJs with a scheduled onair booking or show or sports broadcast, to use it. <strong>NOTE:</strong> only hosts with this option can start live broadcasts.</strong>",
							"director: This DJ Controls locks out the machine and only allows directors to use it.",
							"<strong>WARNING</strong>: DO NOT apply a lock down to a non-WWSU computer! You might unintentionally lock someone out of their computer. In addition, this feature will cause DJ Controls to block power saving settings (such as automatic sleep and OS lock screens) so they do not interfere with the lockdown system.",
						],
						validator: function (callback) {
							let value = this.getValue();
							let belongsTo =
								this.getParent().childrenByPropertyId["belongsTo"].getValue();

							if (belongsTo && belongsTo !== "" && value) {
								callback({
									message:
										"To help prevent accidentally locking others out of their computer, you cannot set a lockdown on a host who has belongs to org member defined.",
									status: false,
								});
								return;
							}
							callback({
								status: true,
							});
						},
					},
					answerCalls: {
						rightLabel: "Yes",
						helpers: [
							"(wwsu-dj-controls only) Can this host be called for remote broadcasts, and broadcast the incoming audio over the air?",
							"<strong>You should not check this box except for hosts which can stream the audio over WWSU's airwaves.</strong> You should also ensure the correct output audio device is selected in the Audio Settings of the DJ Controls running on this host.",
						],
					},
					silenceDetection: {
						rightLabel: "Yes",
						helpers: [
							"(wwsu-dj-controls only) Should this host be responsible for monitoring and reporting silence? <strong>Make sure the input device is properly set in audio settings on this host</strong>; it should receive audio from the WWSU airwaves.",
							"If this check box is disabled / read-only, then another host is already set for silence detection. Please turn off silence detection on the other host first.",
						],
					},
					recordAudio: {
						rightLabel: "Yes",
						helpers: [
							"(wwsu-dj-controls only) Should this host be responsible for recording on-air programming? <strong>Make sure the input device is properly set in audio settings</strong>; it should receive audio from the WWSU airwaves.",
							"If this check box is disabled / read-only, then another host is already set for recording audio. Please turn off audio recording on the other host first.",
						],
					},
					delaySystem: {
						rightLabel: "Yes",
						helpers: [
							"(wwsu-dj-controls only) Is the delay system connected to this host's computer? <strong>Make sure the serial port is correctly chosen in serial port settings</strong>.",
							"If this check box is disabled / read-only, then another host is already set for the delay system. Please turn off delay system on the other host first.",
						],
					},
					EAS: {
						rightLabel: "Yes",
						helpers: [
							"(wwsu-dj-controls only) Is the emergency alert system connected to this host's computer? <strong>Make sure the serial port is correctly chosen in serial port settings</strong>.",
							"If this check box is disabled / read-only, then another host is already set for the EAS. Please turn off EAS on the other host first.",
						],
					},
				},
				form: {
					buttons: {
						submit: {
							title: data ? "Edit Host" : "Add Host",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								// Bug; when belongsTo has no value, it should be set to null. Without this, it is undefined instead.
								if (typeof value.belongsTo === "undefined")
									value.belongsTo = null;

								this.edit(value, (success) => {
									if (success) {
										this.hostModal.iziModal("close");
									}
								});
							},
						},
					},
				},
			},
			data: data ? data : [],
		});
	}

	/**
	 * Is this DJ Controls the host of the current broadcast?
	 *
	 * @return {boolean} True if this host started the current broadcast, false otherwise
	 */
	get isHost() {
		return this.client.ID === this.manager.get("WWSUMeta").meta.host;
	}

	/**
	 * Is this host allowed to start a remote broadcast?
	 *
	 * @param {string} type Either sports (for remote sports broadcasts) or remote (for general remote broadcasts)
	 * @return {boolean} True if yes, false if no.
	 */
	canStartRemoteBroadcast(type) {
		// Failsafes
		if (["sports", "remote"].indexOf(type) === -1) return false;
		if (!this.client.authorized) return false;

		// If no belongsTo is set, it is assumed this host is a WWSU computer. Allow remote broadcasting.
		if (!this.client.belongsTo) return true;

		if (this.client.belongsTo) {
			// Locate the member the host belongs to. If not found, this host cannot broadcast remotely.
			let dj = this.manager
				.get("WWSUdjs")
				.find({ ID: this.client.belongsTo }, true);
			if (!dj) return false;

			// Member has permission to start remote broadcasts from their own computer. Allow it.
			if (dj.permissions.indexOf(type) !== -1) return true;
		}

		// By this point, remote broadcasting is not allowed
		return false;
	}

	/**
	 * If another host started the current broadcast, display a confirmation prompt to prevent accidental interference with another broadcast.
	 *
	 * @param {string} action Description of the action being taken
	 * @param {function} cb Callback when we are the host, or "yes" is chosen on the confirmation dialog.
	 */
	promptIfNotHost(action, cb) {
		if (this.manager.get("WWSUMeta").meta.host && !this.isHost) {
			this.manager
				.get("WWSUutil")
				.confirmDialog(
					`<strong>Your host did not start the current broadcast</strong>. Are you sure you want to ${action}? You may be interfering with someone else's broadcast.`,
					null,
					() => {
						cb();
					}
				);
		} else {
			cb();
		}
	}

	/**
	 * Initialize the data table for managing hosts.
	 *
	 * @param {string} table DOM query string div container where the table should be placed.
	 */
	initTable(table) {
		this.manager.get("WWSUanimations").add("hosts-init-table", () => {
			// Init html
			$(table).html(
				`<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
					this.manager.get("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				}.</p><table id="section-hosts-table" class="table table-striped display responsive" style="width: 100%;"></table>`
			);

			this.manager
				.get("WWSUutil")
				.waitForElement(`#section-hosts-table`, () => {
					// Generate table
					this.table = $(`#section-hosts-table`).DataTable({
						paging: true,
						data: [],
						columns: [
							{ title: "Name" },
							{ title: "Belongs To" },
							{ title: "Authorized?" },
							{ title: "Admin Menu?" },
							{ title: "Properties" },
							{ title: "Actions" },
						],
						columnDefs: [{ responsivePriority: 1, targets: 5 }],
						order: [
							[2, "asc"],
							[0, "asc"],
						],
						pageLength: 100,
						buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
						drawCallback: () => {
							// Action button click events
							$(".btn-host-lockdown-logs").unbind("click");
							$(".btn-host-edit").unbind("click");
							$(".btn-host-delete").unbind("click");

							$(".btn-host-lockdown-logs").click((e) => {
								this.manager.get("WWSUlockdown").showLog({
									host: parseInt($(e.currentTarget).data("id")),
								});
							});

							$(".btn-host-edit").click((e) => {
								let host = this.find().find(
									(host) => host.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.showHostForm(host);
							});

							$(".btn-host-delete").click((e) => {
								let host = this.find().find(
									(host) => host.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.manager.get("WWSUutil").confirmDialog(
									`Are you sure you want to remove the host "${host.friendlyname}"?
							<ul>
							<li>This host will no longer have access to WWSU.</li>
							<li>If the host is connected to WWSU, they will be disconnected.</li>
							<li>All options for this host will be removed.</li>
							<li>This host will be removed from the list. However, should this host try to connect to WWSU again, it will re-appear in the list but with all settings erased (as if it was a new host without authorization to connect).</li>
                            </ul>`,
									null,
									() => {
										this.remove({ ID: host.ID });
									}
								);
							});
						},
					});

					this.table
						.buttons()
						.container()
						.appendTo(`#section-hosts-table_wrapper .col-md-6:eq(0)`);

					// Update with information
					this.updateTable();
				});
		});
	}

	/**
	 * Update the host management table if it exists
	 */
	updateTable() {
		this.manager.get("WWSUanimations").add("hosts-update-table", () => {
			if (this.table) {
				this.table.clear();
				this.find().forEach((host) => {
					let dj;
					if (host) {
						dj = this.manager.get("WWSUdjs").find({ ID: host.belongsTo }, true);
					}
					this.table.row.add([
						host.friendlyname,
						dj ? `${dj.name} (${dj.realName})` : ``,
						`${
							host.authorized
								? `<span class="badge badge-success" title="${host.friendlyname} is authorized to connect to WWSU."><i class="fas fa-check-circle p-1"></i>Yes</span>`
								: `<span class="badge badge-danger" title="${host.friendlyname} is NOT authorized to connect to WWSU; they will be denied access on any attempts."><i class="far fa-times-circle p-1"></i>No</span>`
						}`,
						`${
							host.admin
								? `<span class="badge badge-success" title="${host.friendlyname} has access to the DJ Controls administration menu."><i class="fas fa-check-circle p-1"></i>Yes</span>`
								: `<span class="badge badge-danger" title="${host.friendlyname} does not have access to the DJ Controls administration menu."><i class="far fa-times-circle p-1"></i>No</span>`
						}`,
						`<ul>${
							host.lockDown === "onair"
								? `<li><span class="badge bg-teal" title="${host.friendlyname} is marked as an on-air computer and will be locked down to bookings and scheduled broadcasts."><i class="fas fa-lock p-1"></i>Lockdown: On-Air</span></li>`
								: ``
						}${
							host.lockDown === "prod"
								? `<li><span class="badge bg-teal" title="${host.friendlyname} is marked as a production computer and will be locked down to bookings."><i class="fas fa-lock p-1"></i>Lockdown: Production</span></li>`
								: ``
						}${
							host.lockDown === "director"
								? `<li><span class="badge bg-teal" title="${host.friendlyname} is marked as a director computer and only directors may use it."><i class="fas fa-lock p-1"></i>Lockdown: Director</span></li>`
								: ``
						}${
							host.answerCalls
								? `<li><span class="badge bg-indigo" title="${host.friendlyname} can play audio for remote broadcasts over the airwaves via its configured Output Device."><i class="fas fa-volume-up p-1"></i>Can Broadcast Remotes</span></li>`
								: ``
						}${
							host.silenceDetection
								? `<li><span class="badge bg-orange" title="${host.friendlyname} is monitoring for, and reporting, silence on the air, via its configured Silence Detection Input Devices."><i class="fas fa-volume-mute p-1"></i>Silence Detection</span></li>`
								: ``
						}${
							host.recordAudio
								? `<li><span class="badge bg-blue" title="${host.friendlyname} is recording on-air audio from its configured Record Audio Input Devices and saving them to its configured file path."><i class="fas fa-circle p-1"></i>Record On-Air Audio</span></li>`
								: ``
						}${
							host.delaySystem
								? `<li><span class="badge bg-fuchsia" title="${host.friendlyname} is monitoring the status of the delay system via its configured Serial Port and will trigger it when someone clicks the dump button in DJ Controls."><i class="fas fa-hourglass-start p-1"></i>Monitor / Operate Delay System</span></li>`
								: ``
						}${
							host.EAS
								? `<li><span class="badge badge-danger" title="${host.friendlyname} is monitoring the Emergency Alert System (EAS) via its configured Serial Port."><i class="fas fa-bolt p-1"></i>Monitor / Operate EAS</span></li>`
								: ``
						}</ul>`,
						`<div class="btn-group"><button class="btn btn-sm bg-black btn-host-lockdown-logs" data-id="${host.ID}" title="View host lockdown logs"><i class="fas fa-user-lock"></i></button><button class="btn btn-sm btn-warning btn-host-edit" data-id="${host.ID}" title="Edit Host"><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-host-delete" data-id="${host.ID}" title="Remove Host"><i class="fas fa-trash"></i></button></div>`,
					]);
				});
				this.table.draw(false);
			}
		});
	}
}
