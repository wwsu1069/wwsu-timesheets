// This class manages RSS data from different sources.

// REQUIRES: WWSUdb, noReq (WWSUreq), WWSUMeta
class WWSUrss extends WWSUdb {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The module manager that initiated the class.
	 * @param {object} options Options to pass
	 * @param {Array} options.sources Array of sources to grab and subscribe to
	 */
	constructor(manager, options) {
		super(); // Create the db

		this.manager = manager;
		this.options = options;

		this.endpoints = {
			get: "GET /api/rss",
		};
		this.data = {
			get: { sources: this.options.sources },
		};

		this.assignSocketEvent("rss", this.manager.socket);
	}

    /**
     * Initialize data. Call on socket connect.
     */
    init() {
		this.replaceData(
			this.manager.get("noReq"),
			this.endpoints.get,
			this.data.get
		);
	}
}
