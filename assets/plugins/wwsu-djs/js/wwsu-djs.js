"use strict";

/* global WWSUdb */

// This class manages DJs from WWSU.

// REQUIRES these WWSUmodules: noReq (WWSUreq), adminDirectorReq (WWSUreq), directorReq (WWSUreq), hostReq (WWSUreq), WWSUlogs, WWSUMeta, WWSUutil, WWSUanimations, WWSUlockdown (for viewing lockdown logs), WWSUanalytics (for viewing member analytic tables)
class WWSUdjs extends WWSUdb {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super("WWSUdjs"); // Create the db

		this.manager = manager;

		this.endpoints = {
			get: "GET /api/djs/host/:ID?",
			getDoorCode: "GET /api/djs/host/:ID/door-code",
			add: "POST /api/djs/host",
			edit: "PUT /api/djs/host/:ID",
			remove: "DELETE /api/djs/host/:ID",
			active: "PUT /api/djs/host/:ID/activate",
			inactive: "PUT /api/djs/host/:ID/deactivate",
		};
		this.data = {
			get: {},
		};

		this.assignSocketEvent("djs", this.manager.socket);

		this.table;

		this.djsModal = new WWSUmodal(`Manage Members`, null, ``, true, {
			overlayClose: false,
			zindex: 1100,
		});

		this.djModal = new WWSUmodal(`Member`, null, ``, true, {
			overlayClose: false,
			zindex: 1100,
		});

		this.djInfoModal = new WWSUmodal(`Member Analytics`, null, ``, true, {
			width: 800,
			zindex: 1100,
		});

		this.newDjModal = new WWSUmodal(`New Member`, null, ``, true, {
			overlayClose: false,
			zindex: 1110,
		});

		this.on("change", "WWSUdjs", () => {
			this.updateTable();
		});
	}

	// Initialize connection. Call this on socket connect event.
	init() {
		this.replaceData(
			this.manager.get("noReq"),
			this.endpoints.get,
			this.data.get
		);
	}

	/**
	 * Get a specific DJ (and their stats) from the system.
	 *
	 * @param {string} dom the DOM query string of the element to block while retrieving data
	 * @param {object} data Data to be passed to the WWSU API
	 * @param {function} cb Function to be called with the API info returned as a parameter.
	 */
	getDJ(dom, data, cb) {
		this.manager.get("hostReq").request(
			this.endpoints.get,
			{
				data: data,
			},
			{ dom },
			(body, resp) => {
				if (resp.statusCode < 400 && typeof body === "object" && body.DJ) {
					cb(body);
				} else {
					cb(false);
				}
			}
		);
	}

	/**
	 * Generate a simple DataTables.js table of the members in the system
	 *
	 */
	showDJs() {
		this.djsModal.iziModal("open");
		this.djsModal.body = `<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
			this.manager.get("WWSUMeta")
				? this.manager.get("WWSUMeta").meta.timezone
				: moment.tz.guess()
		}</p><table id="modal-${
			this.djsModal.id
		}-table" class="table table-striped" style="min-width: 100%;"></table><button type="button" class="btn btn-outline-success" id="modal-${
			this.djsModal.id
		}-new">New Member</button>`;
		// Generate new member button

		this.this.manager
			.get("WWSUutil")
			.waitForElement(`#modal-${this.djsModal.id}-new`, () => {
				$(`#modal-${this.djsModal.id}-new`).unbind("click");
				$(`#modal-${this.djsModal.id}-new`).click(() => {
					this.showDJForm();
				});
			});

		$(this.djsModal.body).block({
			message: "<h1>Loading...</h1>",
			css: { border: "3px solid #a00" },
			timeout: 30000,
			onBlock: () => {
				let table = $(`#modal-${this.djsModal.id}-table`).DataTable({
					scrollCollapse: true,
					paging: true,
					data: [],
					columns: [
						{ title: "Nickname" },
						{ title: "Full Name" },
						{ title: "Active?" },
					],
					order: [[0, "asc"]],
					pageLength: 100,
				});
				this.find().forEach((dj) => {
					table.rows.add([
						[
							dj.name || "Unknown",
							dj.fullName || "Unknown",
							dj.active ? `Yes` : `No`,
						],
					]);
				});
				table.draw(false);
				$(this.djsModal.body).unblock();
			},
		});
	}

	/**
	 * Make a "New member" Alpaca form in a modal.
	 */
	showDJForm(data) {
		// reset login to null when filling out default values
		if (data) {
			data.login = null;
			data.doorCode = null;
		}

		// Properly format avatars
		if (data && data.avatar) {
			data.avatar = {
				id: data.avatar,
				name: "Member Headshot",
				size: "N/A",
				url: `https://server.wwsu1069.org/api/uploads/${data.avatar}`,
				thumbnailUrl: `https://server.wwsu1069.org/api/uploads/${data.avatar}`,
				deleteUrl: `https://server.wwsu1069.org/api/uploads/${
					data.avatar
				}&host=${
					this.manager.has("WWSUhosts")
						? this.manager.get("WWSUhosts").client.host
						: "null"
				}`,
				deleteType: "DELETE",
			};
		}

		this.newDjModal.iziModal("open");
		this.newDjModal.body = ``;

		let _djs = this.find();
		let _directors;
		if (this.manager.has("WWSUdirectors"))
			_directors = this.manager.get("WWSUdirectors").find();

		$(this.newDjModal.body).alpaca({
			schema: {
				title: data ? "Edit Member" : "New Member",
				type: "object",
				properties: {
					ID: {
						type: "number",
					},
					name: {
						type: "string",
						required: true,
						title: "Nickname",
						maxLength: 255,
					},
					realName: {
						type: "string",
						title: "Real / Full Name",
						maxLength: 255,
					},
					permissions: {
						title: "Permissions",
						type: "array",
						items: {
							type: "string",
						},
						enum: [
							"org",
							"sportsbroadcaster",
							"book",
							"live",
							"remote",
							"sports",
						],
					},
					login: {
						type: "string",
						format: "password",
						title: "Login Password",
						maxLength: 255,
						required: data ? false : true,
					},
					doorCode: {
						type: "string",
						format: "password",
						title: "Door Code",
						maxLength: 32,
						required: false,
					},
					doorCodeAccess: {
						title: "Door Code Access",
						type: "array",
						items: {
							type: "string",
						},
						enum: ["018", "018A", "018B", "018C", "018D"],
					},
					email: {
						type: "string",
						format: "email",
						title: "Email address",
						maxLength: 255,
						required: data ? false : true,
					},
					avatar: {
						type: "array",
						title: "Head Shot",
					},
					profile: {
						type: "string",
						title: "Profile / Bio",
					},
				},
			},
			view: {
				wizard: {
					bindings: {
						ID: 1,
						name: 1,
						realName: 1,
						permissions: 2,
						login: 2,
						doorCode: 2,
						doorCodeAccess: 2,
						email: 1,
						avatar: 1,
						profile: 1,
					},
					steps: [
						{
							title: "Information",
						},
						{
							title: "Permissions",
						},
					],
				},
			},
			options: {
				fields: {
					ID: {
						type: "hidden",
					},
					name: {
						helper:
							"This is the name that appears publicly on shows, the website, etc, especially if this member hosts their own show. The member will also use their nickname when authenticating for certain things such as the web panel and booking the studios. You may not use the same nickname twice. It is acceptable to use one's real name as the nickname.",
						validator: function (callback) {
							let value = this.getValue();
							let _dj = _djs.find((dj) => dj.name === value);
							if (value.includes(" -") || value.includes(";")) {
								callback({
									status: false,
									message: `Nicknames may not contain " - " or semicolons. These are used by the system as separators. If you are adding multiple members, please add each one by one.`,
								});
								return;
							}
							if ((!data || data.name !== value) && _dj) {
								if (_dj.active) {
									callback({
										status: false,
										message: `An active member with the specified nickname already exists. Please choose another nickname.`,
									});
								} else {
									callback({
										status: false,
										message: `An inactive member with the specified nickname already exists. Please choose another nickname.`,
									});
								}
								return;
							}
							callback({
								status: true,
							});
						},
					},
					realName: {
						helper:
							"Full names are not public; they only help the director identify who is who since nicknames may be non-identifiable.",
						validator: function (callback) {
							let value = this.getValue();
							if (!_directors) {
								callback({
									status: true,
								});
								return;
							}

							if (data) {
								let _director2 = _directors.find(
									(director) => director.name === data.realName
								);
								if (_director2) {
									callback({
										message:
											"A director with the same (original) full name exists. If you change this, the name of the director will also be changed automatically.",
										status: true,
									});
									return;
								}
							}

							let _director = _directors.find(
								(director) => director.name === value
							);
							if (_director) {
								callback({
									message:
										"CAUTION! A director exists with this name. If you continue, this org member will be linked to that director.",
									status: true,
								});
								return;
							}

							callback({
								status: true,
							});
						},
					},
					permissions: {
						helpers: [
							"Select all of the permissions this org member should have.",
							"Be sure to set the belongs to option in Administration -> Hosts to this org member for their computers when granting remote or sports remote broadcasting permissions.",
						],
						optionLabels: [
							"Is an official / full WWSU org member",
							"Is a Sports Broadcaster",
							"Can use / book the studios",
							"Can air live broadcasts from the WWSU onair studio",
							"Can air remote broadcasts from their own hosts / computers",
							"Can use their own hosts / computers to air remote sports broadcasts",
						],
					},
					login: {
						helpers: [
							`The member will use this password to log in to locked-down On-Air or Production studio computers and to access the member web panel.`,
							"If this is not applicable for the member yet, specify a password here but do not give it to the member until applicable.",
						],
					},
					doorCode: {
						helpers: [
							"If this member has door code access, specify / change their door code here (it will be encrypted and can only be accessed by admin directors after they authorize).",
							"If this member does not yet have a door code, or to keep the member's current door code in the system, leave this blank.",
						],
					},
					doorCodeAccess: {
						helper:
							"Select the doors which the member has access to via their door code.",
						optionLabels: [
							"Front Door (018)",
							"On-Air Studio (018A)",
							"Production Studio (018B)",
							"GM Office (018C)",
							"Engineering (018D)",
						],
					},
					email: {
						helper:
							"The email address of the member; highly recommended to use their wright.edu address. Members will be auto-emailed by the system for things like analytics and show/booking changes.",
					},
					avatar: {
						type: "upload",
						maxNumberOfFiles: 1,
						fileTypes: "image/*",
						maxFileSize: 1024 * 1024, // 1 MB file size limit
						upload: {
							autoUpload: false,
							formData: {
								type: "djs",
								host: this.manager.get("WWSUhosts").client.host,
							},
							url: "https://server.wwsu1069.org/api/uploads", // TODO: Programmatic config
						},
						helper:
							"Maximum file size is 1 MB. Will only use first image. Member head shots should be semi-professional and high quality.",
					},
					profile: {
						type: "tinymce",
						options: {
							toolbar:
								"undo redo restoredraft | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | fullscreen preview | image link | ltr rtl",
							plugins:
								"autoresize autosave preview paste importcss searchreplace autolink save directionality visualblocks visualchars fullscreen image link table hr pagebreak nonbreaking toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help quickbars",
							menubar: "file edit view insert format tools table help",
							a11y_advanced_options: true,
							autosave_ask_before_unload: false,
							autosave_retention: "180m",
						},
						helpers: [
							"Optionally specify a profile / biography for this member. Include relevant details about this member's interests and involvement with WWSU as well as any relevant college involvement / credentials. This will be showcased publicly on the website and display signs.",
							"You should ideally add a profile for org members hosting a radio show or authoring blog posts.",
						],
					},
				},
				form: {
					buttons: {
						submit: {
							title: data ? "Edit Member" : "Add Member",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								// Avatars must be formatted to the first id
								value.avatar =
									value.avatar && value.avatar[0] ? value.avatar[0].id : null;

								if (data) {
									this.editDJ(value, (success) => {
										if (success) {
											this.djsModal.iziModal("close");
											this.newDjModal.iziModal("close");
										}
									});
								} else {
									this.addDJ(value, (success) => {
										if (success) {
											this.djsModal.iziModal("close");
											this.newDjModal.iziModal("close");
										}
									});
								}
							},
						},
					},
				},
			},
			data: data ? data : [],
		});
	}

	/**
	 * Add a new member to the system via the API
	 *
	 * @param {Object} data The data to send in the request to the API to add a member
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	addDJ(data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.add,
			{
				data: data,
			},
			{ domModal: this.newDjModal.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Member Added",
						autohide: true,
						delay: 10000,
						body: `member has been created`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Edit a member in the system
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	editDJ(data, cb) {
		this.manager.get("adminDirectorReq").request(
			this.endpoints.edit,
			{
				data: data,
			},
			{ domModal: this.newDjModal.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "member Edited",
						autohide: true,
						delay: 10000,
						body: `member has been edited`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Mark a member as inactive in the system.
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	inactiveDJ(data, cb) {
		this.manager.get("adminDirectorReq").request(
			this.endpoints.inactive,
			{
				data: data,
			},
			{ domModal: this.newDjModal.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "member marked inactive",
						autohide: true,
						delay: 30000,
						body: `member was marked inactive.<br /><br /><strong>Please permanently remove this member when all of the following is true:</strong> They are no longer a WWSU member, their door code was removed from all doors, and you no longer need their analytics.`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Mark a member as active in the system.
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	activeDJ(data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.active,
			{
				data: data,
			},
			{ domModal: this.newDjModal.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "member marked active",
						autohide: true,
						delay: 5000,
						body: `member was marked active.`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Permanently remove a member from the system.
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	removeDJ(data, cb) {
		this.manager.get("adminDirectorReq").request(
			this.endpoints.remove,
			{
				data: data,
			},
			{ domModal: this.newDjModal.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "member Removed",
						autohide: true,
						delay: 10000,
						body: `member was removed.`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Get the door code of a member and send in an event (verifyJWT).
	 * There should be an event handler in the renderer which calls the main process to verify the JWT and display the decrypted door code.
	 *
	 * @param {object} data Data to be passed to the WWSU API.
	 */
	getDoorCode(data) {
		this.manager.get("adminDirectorReq").request(
			this.endpoints.getDoorCode,
			{
				data: data,
			},
			{},
			(body, resp) => {
				if (resp.statusCode < 400)
					this.emitEvent("verifyJWT", [
						body,
						this.manager.get("adminDirectorReq").token,
					]);
			}
		);
	}

	/**
	 * Initialize the table for managing members.
	 *
	 * @param {string} table The DOM query string for the div container to place the table.
	 */
	initTable(table) {
		this.manager.get("WWSUanimations").add("djs-init-table", () => {
			// Init html
			$(table).html(
				`<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
					this.manager.get("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				}.</p><p><button type="button" class="btn btn-block btn-success btn-dj-new">New member</button></p><table id="section-members-table" class="table table-striped display responsive" style="width: 100%;"></table>`
			);

			this.manager
				.get("WWSUutil")
				.waitForElement(`#section-members-table`, () => {
					// Generate table
					this.table = $(`#section-members-table`).DataTable({
						paging: true,
						data: [],
						columns: [
							{ title: "Nickname" },
							{ title: "Real Name" },
							{ title: "Active?" },
							{ title: "Actions" },
						],
						columnDefs: [{ responsivePriority: 1, targets: 3 }],
						order: [[0, "asc"]],
						pageLength: 100,
						buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
						drawCallback: () => {
							// Action button click events
							$(".btn-dj-logs").unbind("click");
							$(".btn-dj-lockdown-logs").unbind("click");
							$(".btn-dj-analytics").unbind("click");
							$(".btn-dj-edit").unbind("click");
							$(".btn-dj-inactive").unbind("click");
							$(".btn-dj-active").unbind("click");
							$(".btn-dj-delete").unbind("click");
							$(".btn-dj-notes").unbind("click");
							$(".btn-dj-doorcode").unbind("click");

							$(".btn-dj-analytics").click((e) => {
								this.manager
									.get("WWSUanalytics")
									.showAnalytics(
										"member",
										parseInt($(e.currentTarget).data("id"))
									);
							});

							$(".btn-dj-logs").click((e) => {
								let dj = this.find().find(
									(dj) => dj.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.showDJLogs(dj);
							});

							$(".btn-dj-lockdown-logs").click((e) => {
								this.manager.get("WWSUlockdown").showLog({
									type: "DJ",
									typeID: parseInt($(e.currentTarget).data("id")),
								});
							});

							$(".btn-dj-notes").click((e) => {
								let dj = this.find().find(
									(dj) => dj.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.manager.get("WWSUdjnotes").showDJNotes(dj);
							});

							$(".btn-dj-edit").click((e) => {
								let dj = this.find().find(
									(dj) => dj.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.showDJForm(dj);
							});

							$(".btn-dj-doorcode").click((e) => {
								console.log(`Clicked doorcode`);
								this.getDoorCode({
									ID: parseInt($(e.currentTarget).data("id")),
								});
							});

							$(".btn-dj-inactive").click((e) => {
								let dj = this.find().find(
									(dj) => dj.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.manager.get("WWSUutil").confirmDialog(
									`Are you sure you want to <strong>mark the member "${dj.name}" as inactive</strong>?
                            <ul>
                            <li><strong>Do NOT mark a member as inactive unless they are either an inactive member of WWSU or no longer a member of WWSU.</strong> Do not mark a member inactive simply when they are no longer hosting radio shows (mark the events inactive instead via the Calendar tab).</li>
							<li>The member will be removed from all calendar events they are listed on.</li>
							<li>Any events which this member was the primary host of will also be marked inactive and its schedules removed (If you don't want this, please assign a new host to these events before marking this member inactive.). Subscribers of those shows will be notified it has been discontinued.</li>
							<li>If a director exists with the same full name, <strong>the director will be deleted permanently</strong> along with their office hour schedules (but not timesheet records) and tasks.</li>
							<li>Any hosts set as belonging to this org member will no longer be authorized and can no longer connect to WWSU.</li>
							<li>This member can no longer be assigned as a host or co-host of any events as long as they are marked inactive.</li>						
							<li>The member can no longer log in or authorize, such as to access the DJ Panel or book/use the studios, as long as they are set as inactive.</li>
							<li>The member will no longer receive any emails sent via the DJ Controls email tab so long as they are set as inactive.</li>
							<li>Analytics, logs, and notes/credits/warnings of inactive members can still be accessed until/unless they are permanently deleted.</li>	
                            </ul>`,
									dj.name,
									() => {
										this.inactiveDJ({ ID: dj.ID });
									}
								);
							});

							$(".btn-dj-active").click((e) => {
								let dj = this.find().find(
									(dj) => dj.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.manager.get("WWSUutil").confirmDialog(
									`Are you sure you want to <strong>mark the member "${dj.name}" as active</strong>?
                            <ul>
                            <li>The member can log in or authorize again, such as to access the DJ Panel or use/book the studios.</li>
							<li>The member can now be set as a host / co-host for events again.</li>
							<li>The member will start to receive emails sent via the DJ Controls -> Email tab.</li>
							<li><strong>If this member is also a director, you will need to manually re-add them as a director back in the system;</strong> this is not automatically done.</li>
							<li><strong>You must manually re-authorize hosts belonging to this org member</strong>; they were marked as no longer authorized when this member was marked inactive.</li>
							<li><strong>Events which had this member as a host or co-host will need to be manually edited or re-created</strong>; This member was removed as co-host from all events when marked inactive. And events which this member was the primary host were marked inactive.</li>
                            </ul>`,
									null,
									() => {
										this.activeDJ({ ID: dj.ID });
									}
								);
							});

							$(".btn-dj-delete").click((e) => {
								let dj = this.find().find(
									(dj) => dj.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.manager.get("WWSUutil").confirmDialog(
									`Are you sure you want to <strong>permanently remove the member "${dj.name}"</strong>?
                            <ul>
                            <li><strong>Do NOT permanently remove this member if any of these conditions are true:</strong> the member is still active with WWSU or may become active at any time in the future, their door codes or other access were not removed yet, or you still need their analytics or DJ notes / credits / warnings records.</li>
							<li>The member will be removed from all calendar events they are listed on.</li>
                            <li>Any events which this member was the primary host of will also be marked inactive and their schedules removed (If you don't want this, please assign a new host to these events before deleting this member.). Subscribers of those shows will be notified it has been discontinued.</li>
							<li>If a director exists with the same full name, <strong>the director will be deleted permanently</strong> along with their office hour schedules (but not timesheet records) and tasks.</li>
							<li>All notes, remote credits, and warnings records for the member will be removed.</li>
							<li>Any hosts set as belonging to this org member will no longer be authorized and can no longer connect to WWSU.</li>
							<li>This member's analytics will no longer be available (but show logs will still be available in the Logs tab).</li>
                            <li>The member can no longer log in or authorize, such as to access the DJ Panel or use the studio computers.</li>
                            </ul>`,
									dj.name,
									() => {
										this.removeDJ({ ID: dj.ID });
									}
								);
							});
						},
					});

					this.table
						.buttons()
						.container()
						.appendTo(`#section-members-table_wrapper .col-md-6:eq(0)`);

					// Add click event for new member button
					$(".btn-dj-new").unbind("click");
					$(".btn-dj-new").click(() => {
						this.showDJForm();
					});

					// Update with information
					this.updateTable();
				});
		});
	}

	/**
	 * Update the member management table if it exists
	 */
	updateTable() {
		this.manager.get("WWSUanimations").add("djs-update-table", () => {
			if (this.table) {
				this.table.clear();
				this.find().forEach((dj) => {
					let daysAgo = "unknown";
					if (dj.lastSeen) {
						daysAgo = moment(
							this.manager.get("WWSUMeta")
								? this.manager.get("WWSUMeta").meta.time
								: undefined
						).diff(dj.lastSeen, "days");
					}
					let icon = `<span class="badge badge-danger" title="INACTIVE: This member is marked inactive."><i class="far fa-times-circle p-1"></i>No</span>`;
					if (!dj.active) {
						icon = `<span class="badge badge-danger" title="INACTIVE: This member is marked inactive."><i class="far fa-times-circle p-1"></i>No</span>`;
					} else if (
						!dj.lastSeen ||
						moment(dj.lastSeen)
							.add(120, "days")
							.isBefore(
								moment(
									this.manager.get("WWSUMeta")
										? this.manager.get("WWSUMeta").meta.time
										: undefined
								)
							)
					) {
						icon = `<span class="badge bg-orange" title="ACTIVE, but this member did not air a broadcast nor authorize anything for 4 or more months."><i class="far fa-question-circle p-1"></i>Yes??? (${daysAgo} days ago)</span>`;
					} else if (
						!dj.lastSeen ||
						moment(dj.lastSeen)
							.add(30, "days")
							.isBefore(
								moment(
									this.manager.get("WWSUMeta")
										? this.manager.get("WWSUMeta").meta.time
										: undefined
								)
							)
					) {
						icon = `<span class="badge badge-warning" title="ACTIVE, but this member did not air a broadcast nor authorize anything for over 30 days but less than 1 year."><i class="far fa-question-circle p-1"></i>Yes? (${daysAgo} days ago)</span>`;
					} else if (
						moment(dj.lastSeen)
							.add(7, "days")
							.isBefore(
								moment(
									this.manager.get("WWSUMeta")
										? this.manager.get("WWSUMeta").meta.time
										: undefined
								)
							)
					) {
						icon = `<span class="badge badge-info" title="ACTIVE, but did not air a broadcast nor authorize anything for 7 - 30 days."><i class="far fa-check-circle p-1"></i>Yes (${daysAgo} days ago)</span>`;
					} else {
						icon = `<span class="badge badge-success" title="ACTIVE"><i class="fas fa-check-circle p-1"></i>Yes (${daysAgo} days ago)</span>`;
					}
					this.table.row.add([
						dj.name || "",
						dj.realName || "",
						`${icon}`,
						dj.active
							? `<div class="btn-group"><button class="btn btn-sm bg-blue btn-dj-analytics" data-id="${
									dj.ID
							  }" title="View member and Show Analytics"><i class="fas fa-chart-line"></i></button><button class="btn btn-sm btn-secondary btn-dj-logs" data-id="${
									dj.ID
							  }" title="View Broadcast Logs"><i class="fas fa-clipboard-list"></i></button><button class="btn btn-sm bg-black btn-dj-lockdown-logs" data-id="${
									dj.ID
							  }" title="View Lockdown Logs"><i class="fas fa-user-lock"></i></button><button class="btn btn-sm bg-indigo btn-dj-notes" data-id="${
									dj.ID
							  }" title="View/Edit Notes, Remote Credits, and Warnings"><i class="fas fa-sticky-note"></i></button><button class="btn btn-sm bg-fuchsia btn-dj-doorcode" data-id="${
									dj.ID
							  }" title="Access member's door code"><i class="fas fa-door-open"></i></button><button class="btn btn-sm btn-warning btn-dj-edit" data-id="${
									dj.ID
							  }" title="Edit member"><i class="fas fa-edit"></i></button><button class="btn btn-sm bg-orange btn-dj-inactive${
									dj.ID === 1 ? ` disabled` : ``
							  }" data-id="${
									dj.ID
							  }" title="Mark member Inactive and Remove as Host/Co-Host from All Events"><i class="fas fa-times-circle"></i></button></div>`
							: `<div class="btn-group"><button class="btn btn-sm bg-blue btn-dj-analytics" data-id="${
									dj.ID
							  }" title="View member and Show Analytics"><i class="fas fa-chart-line"></i></button><button class="btn btn-sm btn-secondary btn-dj-logs" data-id="${
									dj.ID
							  }" title="View Broadcast Logs"><i class="fas fa-clipboard-list"></i></button><button class="btn btn-sm bg-black btn-dj-lockdown-logs" data-id="${
									dj.ID
							  }" title="View Lockdown Logs"><i class="fas fa-user-lock"></i></button><button class="btn btn-sm bg-indigo btn-dj-notes" data-id="${
									dj.ID
							  }" title="View/Edit Notes, Remote Credits, and Warnings"><i class="fas fa-sticky-note"></i></button><button class="btn btn-sm bg-fuchsia btn-dj-doorcode" data-id="${
									dj.ID
							  }" title="Access member's door code"><i class="fas fa-door-open"></i></button><button class="btn btn-sm btn-success btn-dj-active" data-id="${
									dj.ID
							  }" title="Mark member as active"><i class="fas fa-check-circle"></i></button><button class="btn btn-sm btn-danger btn-dj-delete${
									dj.ID === 1 ? ` disabled` : ``
							  }" data-id="${
									dj.ID
							  }" title="Permanently remove this member"><i class="fas fa-trash"></i></button></div>`,
					]);
				});
				this.table.draw(false);
			}
		});
	}

	/**
	 * Show attendance records for the provided member in a modal
	 *
	 * @param {object} dj The member record to view attendance records
	 */
	showDJLogs(dj) {
		this.djInfoModal.iziModal("open");
		this.djInfoModal.title = `Attendance Logs for ${dj.name} (${
			dj.realName || `Unknown Person`
		})`;
		this.djInfoModal.body = `<div class="callout callout-info">
		<h5>Logs Lifespan</h5>
		<p>
			Operation logs are deleted automatically after 2 years. This is
			to keep the system running fast and smooth.
		</p>
	</div>
	<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
		this.manager.get("WWSUMeta")
			? this.manager.get("WWSUMeta").meta.timezone
			: moment.tz.guess()
	}.</p><table id="section-members-table-logs" class="table table-striped display responsive" style="width: 100%;"></table>
								<h5>Actions Key:</h5>
								<div class="container-fluid">
									<div class="row">
										<div class="col">
											<span class="badge bg-blue"
												><i class="fas fa-eye"></i></span
											>View Log and Online Listener Graph
										</div>
									</div>
								</div>`;

		this.manager
			.get("WWSUutil")
			.waitForElement(`#section-members-table-logs`, () => {
				this.manager
					.get("WWSUlogs")
					.getAttendance(
						`#modal-${this.djInfoModal.id}`,
						{ dj: dj.ID },
						(logs) => {
							let table = $(`#section-members-table-logs`).DataTable({
								paging: true,
								data:
									!logs || typeof logs.map !== "function"
										? []
										: logs.map((record) => {
												let theClass = "secondary";
												let theType = `Unknown`;
												if (record.event.toLowerCase().startsWith("show: ")) {
													theClass = "primary";
													theType = "Show";
												} else if (
													record.event.toLowerCase().startsWith("prerecord: ")
												) {
													theClass = "pink";
													theType = "Prerecord";
												} else if (
													record.event.toLowerCase().startsWith("sports: ")
												) {
													theClass = "success";
													theType = "Sports";
												} else if (
													record.event.toLowerCase().startsWith("remote: ")
												) {
													theClass = "indigo";
													theType = "Remote";
												} else if (
													record.event.toLowerCase().startsWith("genre: ")
												) {
													theClass = "info";
													theType = "Genre";
												} else if (
													record.event.toLowerCase().startsWith("playlist: ")
												) {
													theClass = "blue";
													theType = "Playlist";
												}
												if (
													record.actualStart !== null &&
													record.actualEnd !== null &&
													record.happened === 1
												) {
													return [
														record.ID,
														moment
															.tz(
																record.actualStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("L"),
														`<span class="badge bg-${theClass}">${theType}</span>`,
														record.event,
														moment
															.tz(
																record.actualStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("h:mm A"),
														moment
															.tz(
																record.actualEnd,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("h:mm A"),
														`<button class="btn btn-sm bg-blue btn-logs-view" data-id="${record.ID}" title="View this log"><i class="fas fa-eye"></i></button>`,
													];
												} else if (
													record.actualStart !== null &&
													record.actualEnd === null &&
													record.happened === 1
												) {
													return [
														record.ID,
														moment
															.tz(
																record.actualStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("L"),
														`<span class="badge bg-${theClass}">${theType}</span>`,
														record.event,
														moment
															.tz(
																record.actualStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("h:mm A"),
														`ONGOING`,
														`<button class="btn btn-sm bg-blue btn-logs-view" data-id="${record.ID}" title="View this log"><i class="fas fa-eye"></i></button>`,
													];
												} else if (
													record.actualStart === null &&
													record.actualEnd === null &&
													record.happened === -1
												) {
													return [
														record.ID,
														moment
															.tz(
																record.scheduledStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("L"),
														`<span class="badge bg-${theClass}">${theType}</span>`,
														record.event,
														`CANCELED (${moment
															.tz(
																record.scheduledStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("h:mm A")})`,
														`CANCELED (${moment
															.tz(
																record.scheduledEnd,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("h:mm A")})`,
														`<button class="btn btn-sm bg-blue btn-logs-view" data-id="${record.ID}" title="View this log"><i class="fas fa-eye"></i></button>`,
													];
												} else if (
													record.actualStart === null &&
													record.actualEnd === null &&
													record.happened === 0
												) {
													return [
														record.ID,
														moment
															.tz(
																record.scheduledStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("L"),
														`<span class="badge bg-${theClass}">${theType}</span>`,
														record.event,
														`ABSENT (${moment
															.tz(
																record.scheduledStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("h:mm A")})`,
														`ABSENT (${moment
															.tz(
																record.scheduledEnd,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("h:mm A")})`,
														`<button class="btn btn-sm bg-blue btn-logs-view" data-id="${record.ID}" title="View this log"><i class="fas fa-eye"></i></button>`,
													];
												} else if (
													record.actualStart !== null &&
													record.actualEnd !== null
												) {
													return [
														record.ID,
														moment
															.tz(
																record.actualStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("L"),
														`<span class="badge bg-${theClass}">${theType}</span>`,
														record.event,
														moment
															.tz(
																record.actualStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("h:mm A"),
														record.actualEnd !== null
															? moment
																	.tz(
																		record.actualEnd,
																		this.manager.get("WWSUMeta")
																			? this.manager.get("WWSUMeta").meta
																					.timezone
																			: moment.tz.guess()
																	)
																	.format("h:mm A")
															: `ONGOING`,
														`<button class="btn btn-sm bg-blue btn-logs-view" data-id="${record.ID}" title="View this log"><i class="fas fa-eye"></i></button>`,
													];
												} else {
													return [
														record.ID,
														moment
															.tz(
																record.scheduledStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("L"),
														`<span class="badge bg-${theClass}">${theType}</span>`,
														record.event,
														`SCHEDULED (${moment
															.tz(
																record.scheduledStart,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("h:mm A")})`,
														`SCHEDULED (${moment
															.tz(
																record.scheduledEnd,
																this.manager.get("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("h:mm A")})`,
														`<button class="btn btn-sm bg-blue btn-logs-view" data-id="${record.ID}" title="View this log"><i class="fas fa-eye"></i></button>`,
													];
												}
										  }),
								columns: [
									{ title: "ID" },
									{ title: "Date" },
									{ title: "Icon" },
									{ title: "Event" },
									{ title: "Start" },
									{ title: "End" },
									{ title: "Actions" },
								],
								columnDefs: [{ responsivePriority: 1, targets: 6 }],
								order: [[0, "desc"]],
								pageLength: 50,
								buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
								drawCallback: () => {
									// Add log buttons click event
									$(".btn-logs-view").unbind("click");
									$(".btn-logs-view").click((e) => {
										let id = parseInt($(e.currentTarget).data("id"));
										this.manager.get("WWSUlogs").viewLog(id);
									});
								},
							});

							table
								.buttons()
								.container()
								.appendTo(
									`#section-members-table-logs_wrapper .col-md-6:eq(0)`
								);
						}
					);
			});
	}
}
