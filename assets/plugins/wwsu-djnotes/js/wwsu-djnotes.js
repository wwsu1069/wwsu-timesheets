"use strict";

/* global WWSUdb */

// This class manages Member notes from WWSU.

class WWSUdjnotes extends WWSUevents {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options = {}) {
		super(); // Create the db

		this.manager = manager;
		this.options = options;

		this.endpoints = {
			get: "GET /api/djnotes/:dj?",
			add: "POST /api/djnotes",
			edit: "PUT /api/djnotes/:ID",
			remove: "DELETE /api/djnotes/:ID",
		};
		this.data = {
			get: {},
		};

		this.modals = {
			notes: new WWSUmodal(`Member Notes`, null, ``, true, {
				zindex: 1100,
			}),
			newNote: new WWSUmodal(`New Member Note`, null, ``, true, {
				zindex: 1110,
				overlayClose: false,
			}),
		};

		this.notes = [];
	}

	/**
	 * Get the timezone we should use.
	 */
	get timezone() {
		return this.manager.has("WWSUMeta")
			? this.manager.get("WWSUMeta").timezone
			: moment.tz.guess();
	}

	/**
	 * Get records of Member notes from the WWSU API.
	 *
	 * @param {Object} data Data to be passed to the API.
	 * @param {function} cb Function called after request is made; parameter is the data returned from the server.
	 */
	get(data, cb) {
		this.manager
			.get("hostReq")
			.request(this.endpoints.get, { data }, {}, (body, resp) => {
				if (resp.statusCode <= 400 && body.constructor === Array) {
					this.notes = body;
				}
				cb(body);
			});
	}

	/**
	 * Permanently remove a Member Note from the system.
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	remove(data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.remove,
			{
				data: data,
			},
			{ domModal: this.modals.notes.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Member Note Removed",
						autohide: true,
						delay: 10000,
						body: `Member note was removed!`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Add a Member Note into the system.
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	add(data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.add,
			{
				data: data,
			},
			{ domModal: this.modals.newNote.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Member Note Added",
						autohide: true,
						delay: 10000,
						body: `Member note was added!`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Edit a Member Note in the system.
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	edit(data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.edit,
			{
				data: data,
			},
			{ domModal: this.modals.newNote.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Member Note Edited",
						autohide: true,
						delay: 10000,
						body: `Member note was edited!`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Generate a DataTables.js table and action buttons for Member notes of a specific member
	 *
	 * @param {Object} dj The dj from WWSUdjs to get notes
	 */
	showDJNotes(dj) {
		// Initialize the table class
		this.modals.notes.iziModal("open");
		this.modals.notes.title = `Member Notes for ${dj.name}`;
		this.modals.notes.body = `<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
			this.manager.get("WWSUMeta")
				? this.manager.get("WWSUMeta").meta.timezone
				: moment.tz.guess()
		}.</p><table id="modal-${
			this.modals.notes.id
		}-table" class="table table-striped" style="min-width: 100%;"></table>
								<h5>Actions Key:</h5>
								<div class="container-fluid">
									<div class="row">
										<div class="col">
											<span class="badge badge-warning"
												><i class="fas fa-edit"></i></span
											>Edit
										</div>
										<div class="col">
											<span class="badge badge-danger"
												><i class="fas fa-trash"></i></span
											>Delete
										</div>
									</div>
								</div>
								<button type="button" class="btn btn-outline-success" id="modal-${
									this.modals.notes.id
								}-new">New Member Note</button>`;

		// Block the modal while we generate the table
		$(`#modal-${this.modals.notes.id} .modal-content`)
			.prepend(`<div class="overlay">
  <i class="fas fa-2x fa-sync fa-spin"></i>
  <h1 class="text-white">Loading table...</h1>
</div>`);
		// Get the data
		this.get({ dj: dj.ID }, (djnotes) => {
			// Extra information
			const format = (d) => {
				return `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
					<tr>
						<td>Details:</td>
						<td>${d.description}</td>
					</tr>
					<tr>
						<td>Amount (Remote Credits / Warning Points):</td>
						<td>${d.amount}</td>
					</tr>
					</table>`;
			};

			// Generate the data table
			let table = $(`#modal-${this.modals.notes.id}-table`).DataTable({
				paging: true,
				data: [],
				columns: [
					{
						className: "details-control",
						orderable: false,
						data: null,
						defaultContent: "",
					},
					{ title: "ID", data: "ID" },
					{ title: "Type", data: "type" },
					{ title: "Date/Time", data: "date" },
					{ title: "Actions", data: "actions" },
				],
				columnDefs: [{ responsivePriority: 1, targets: 4 }],
				order: [[1, "asc"]],
				pageLength: 25,
				buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
				drawCallback: () => {
					// Action button click events
					$(".btn-djnote-edit").unbind("click");
					$(".btn-djnote-delete").unbind("click");

					// Edit event
					$(".btn-djnote-edit").click((e) => {
						let djnote = this.notes.find(
							(note) =>
								note.ID === parseInt($(e.currentTarget).data("djnoteid"))
						);
						this.showDJNoteForm(djnote, dj);
					});

					// Confirm before deleting when someone wants to delete an event
					$(".btn-djnote-delete").click((e) => {
						let djnote = this.notes.find(
							(note) =>
								note.ID === parseInt($(e.currentTarget).data("djnoteid"))
						);
						this.manager.get("WWSUutil").confirmDialog(
							`<p>Are you sure you want to <b>permanently</b> remove Member note ${
								djnote.ID
							}?</p>
                            <ul>
							<li><strong>This will permanently remove the Member Note from the member's records!</strong> It is not recommended to do this unless this record is no longer relevant.</li>
							${
								djnote.type.startsWith("remote-")
									? `<li>This will remove ${djnote.amount} remote credits from the member.</li>`
									: ``
							}
							${
								djnote.type.startsWith("warning-")
									? `<li>This will remove ${djnote.amount} warning points from the member.</li>`
									: ``
							}
                            </ul>`,
							djnote.ID,
							() => {
								this.remove(
									{ ID: parseInt($(e.currentTarget).data("djnoteid")) },
									(success) => {
										// Reload the modal
										this.showDJNotes(dj);
									}
								);
							}
						);
					});
				},
			});

			table
				.buttons()
				.container()
				.appendTo(
					$(`#modal-${this.modals.notes.id}-table_wrapper .col-md-6:eq(0)`)
				);

			// Additional info rows
			$(`#modal-${this.modals.notes.id}-table tbody`).on(
				"click",
				"td.details-control",
				(e) => {
					let tr = $(e.target).closest("tr");
					let row = table.row(tr);

					if (row.child.isShown()) {
						// This row is already open - close it
						row.child.hide();
						tr.removeClass("shown");
					} else {
						// Open this row
						row.child(format(row.data())).show();
						tr.addClass("shown");
					}
				}
			);

			// Populate the data table with data.
			let drawRows = () => {
				djnotes.forEach((note) => {
					let badgeClass = `badge-secondary`;
					if (note.type.startsWith("remote-")) badgeClass = `badge-success`;
					if (note.type.startsWith("warning-")) badgeClass = `badge-warning`;
					if (note.type.startsWith("public-")) badgeClass = `badge-info`;
					table.rows.add([
						{
							ID: note.ID,
							amount: note.amount,
							description: note.description,
							type: `<span class="badge ${badgeClass}">${note.type}</span>`,
							date: moment
								.tz(
									note.date,
									this.manager.get("WWSUMeta")
										? this.manager.get("WWSUMeta").meta.timezone
										: moment.tz.guess()
								)
								.format("LLLL"),
							actions: `<div class="btn-group"><button class="btn btn-sm btn-warning btn-djnote-edit" data-djnoteid="${note.ID}" title="View / Edit Member Note"><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-djnote-delete" data-djnoteid="${note.ID}" title="Delete this Member Note"><i class="fas fa-trash"></i></button></div>`,
						},
					]);
				});
				table.draw(false);

				$(`#modal-${this.modals.notes.id} .overlay`).remove();
			};

			drawRows();
		});

		$(`#modal-${this.modals.notes.id}-new`).unbind("click");
		$(`#modal-${this.modals.notes.id}-new`).click(() => {
			this.showDJNoteForm(null, dj);
		});
	}

	/**
	 * Make a "New Member Note" Alpaca form in a modal.
	 *
	 * @param {?Object} data When editing an entry, the initial data.
	 * @param {?Object} defaultDj When adding or editing a note from a DJ screen, the WWSUdjs dj from which we were viewing notes.
	 */
	showDJNoteForm(data, defaultDj) {
		this.modals.newNote.body = ``;

		this.modals.newNote.iziModal("open");

		let _djs = this.manager.get("WWSUdjs").find();

		$(this.modals.newNote.body).alpaca({
			schema: {
				title: data ? "Edit Member Note" : "New Member Note",
				type: "object",
				properties: {
					ID: {
						type: "number",
					},
					dj: data
						? {
								type: "number",
								required: true,
								title: "Applicable Member",
								enum: _djs.map((dj) => dj.ID),
						  }
						: undefined,
					djs: !data
						? {
								type: "array",
								items: {
									type: "number",
								},
								required: true,
								title: "Applicable Members",
								enum: _djs.map((dj) => dj.ID),
								default: defaultDj ? [defaultDj.ID] : [],
						  }
						: undefined,
					date: {
						title: "Date/time of Occurrance",
						format: "datetime",
						required: true,
					},
					type: {
						type: "string",
						required: true,
						title: "Type of Note",
						enum: [
							"public-general",
							"public-reminder",
							"public-praise",
							"private-general",
							"private-watchlist",
							"remote-general",
							"remote-event",
							"remote-production",
							"remote-cdreview",
							"remote-feedback",
							"remote-sportsbroadcast",
							"warning-general",
							"warning-fccviolation",
							"warning-absence",
							"warning-handbookviolation",
							"warning-membership",
							"warning-deadlineviolation",
						],
					},
					description: {
						type: "string",
						title: "Note",
						required: true,
					},
					amount: {
						type: "number",
						default: 0,
						title: "Remote Credits / Warning Points",
						required: true,
					},
				},
			},
			options: {
				fields: {
					ID: {
						type: "hidden",
					},
					dj: {
						type: data ? "select" : "hidden",
						helper: [
							"Change the member this note applies to.",
							"WARNING! Changing the member will alter the applicable remote credits / warning points. Also, do note that notes that were added to multiple members cannot be edited en masse; they must be edited one at a time from each member.",
						],
						optionLabels: _djs.map((dj) => `${dj.name} (${dj.realName})`),
					},
					djs: {
						type: data ? "hidden" : "select",
						helpers: [
							"Choose all of the members that you want this note to be saved.",
							"WARNING! After adding the note, you cannot edit it for all selected members en masse; you must edit the note from each member individually in their respective notes screen.",
						],
						optionLabels: _djs.map((dj) => `${dj.name} (${dj.realName})`),
						multiple: true,
					},
					date: {
						dateFormat: `YYYY-MM-DD hh:mm A`,
						picker: {
							inline: true,
							sideBySide: true,
						},
						helper: `This should generally be the date/time the note was applicable; use the station timezone of ${this.timezone}. For examples: date/time the remote credit was earned; date/time the incident being disciplined occurred, etc.`,
					},
					type: {
						type: "select",
						helper:
							"public- are notes that will be visible to the member. private- are notes that will not be visible to the member. remote- are remote credits earned by the member. warning- are warning points / discipline issued against the member.",
					},
					description: {
						type: "tinymce",
						options: {
							toolbar:
								"undo redo restoredraft | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | fullscreen preview | image link | ltr rtl",
							plugins:
								"autoresize autosave preview paste importcss searchreplace autolink save directionality visualblocks visualchars fullscreen image link table hr pagebreak nonbreaking toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help quickbars",
							menubar: "file edit view insert format tools table help",
							a11y_advanced_options: true,
							autosave_ask_before_unload: false,
							autosave_retention: "180m",
						},
						helper:
							"Be sure to include all relevant information especially for warning- types",
					},
				},
				form: {
					buttons: {
						submit: {
							title: data ? "Edit Member Note" : "Add Member Note",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								// Add timezone to date and convert to ISO String
								if (value.date) {
									value.date = `${moment
										.utc(value.date, "YYYY-MM-DD hh:mm A")
										.format("YYYY-MM-DD[T]HH:mm:ss")}${moment
										.tz(value.date, this.timezone)
										.format("ZZ")}`;
								}

								if (value.djs) value.djs = value.djs.map((item) => item.value);

								if (data) {
									this.edit(value, (success) => {
										if (success) {
											this.modals.newNote.iziModal("close");
											if (defaultDj) this.showDJNotes(defaultDj);
										}
									});
								} else {
									this.add(value, (success) => {
										if (success) {
											this.modals.newNote.iziModal("close");
											if (defaultDj) this.showDJNotes(defaultDj);
										}
									});
								}
							},
						},
					},
				},
			},
			data: data ? data : [],
		});
	}
}
