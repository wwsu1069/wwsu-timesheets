class Bokeh {
	constructor(
		bulbs = 100,
		bulbSize = 100,
		blurLevel = 10,
		baseId = `bokeh`,
		colors = ["#ff0000", "#ff7800", "#ffff00", "#00ff00", "#0000ff", "#ff00ff"]
	) {
		this.bulbs = bulbs;
		this.bulbSize = bulbSize;
		this.blurLevel = blurLevel;
		this.baseId = baseId;
		this.colors = colors;

		this.active = false;
	}

	// This function allows you to turn on and off the lights
	toggle(activate = true) {
		if (activate) {
			if (!this.active) {
				this._spawnCSS();
				this._spawnBulbs();
				$(`#${this.baseId}`).css("display", "block");
				this.active = true;
			}
		} else {
			if (this.active) {
				this._destroyBulbs();
				$(`#${this.baseId}`).css("display", "none");
				this.active = false;
			}
		}
	}

	random_int(value = 100) {
		return Math.floor(Math.random() * value) + 1;
	}

	random_range(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	// Destroy bulbs
	_destroyBulbs() {
		$(`.${this.baseId}-bulb`).remove();
		$(`.${this.baseId}-bulb-css`).remove();
	}

	// Create style for bulb
	_spawnCSS(bulbs = this.bulbs) {
		let bulbName = `${this.baseId}-bulb`;
		let rule = ``;

		for (let i = 1; i < bulbs; i++) {
			let bulbSize = this.bulbSize + this.random_int(25);
			rule += `.${bulbName}:nth-child(${i}) {
                        width: ${bulbSize}px;
                        height: ${bulbSize}px;
                        top: ${this.random_int(100)}%;
                        left: ${this.random_int(100)}%;
                        background: ${
													this.colors[this.random_range(0, this.colors.length)]
												};
                        filter: blur(${this.random_range(
													this.blurLevel,
													this.blurLevel * 2
												)}px);
                        animation: ${this.random_range(
													4,
													16
												)}s bulb${i} linear infinite;
                        pointer-events: none;
                        position: fixed;
                        z-index: 99999;
                    }

                    @keyframes bulb${i} {
                        25% {
                            opacity: ${this.random_range(25, 75) * 0.01};
                        }
                        50% {
                            opacity: 0;
                        }
                        75% {
                            opacity: ${this.random_range(25, 75) * 0.01};
                        }
                        100% {
                            opacity: 0;
                        }
                    }
        `;
		}

		this._addCSS(rule);
	}

	// Create bulbs
	_spawnBulbs(bulbs = this.bulbs) {
		bulbs -= 1;

		for (let x = 0; x < bulbs; x++) {
			let board = document.createElement("div");
			board.className = `${this.baseId}-bulb`;

			document.getElementById(this.baseId).appendChild(board);
		}
	}

	// Append style for each snowflake to the head
	_addCSS(rule) {
		let css = document.createElement("style");
		css.className = `${this.baseId}-bulb-css`;
		css.type = "text/css";
		css.appendChild(document.createTextNode(rule)); // Support for the rest
		document.getElementsByTagName("head")[0].appendChild(css);
	}
}
