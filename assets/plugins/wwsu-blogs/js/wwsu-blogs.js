// Class to manage blog posts on an admin level

// REQUIRES these WWSU modules: WWSUdb, hostReq (WWSUreq), directorReq (WWSUreq), WWSUmodal, WWSUanimations, WWSUutil, WWSUMeta

class WWSUblogs extends WWSUdb {
	/**
	 * The class constructor.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options = {}) {
		super("WWSUblogs");

		this.manager = manager;
		this.options = options;

		this.endpoints = {
			add: "POST /api/blogs/host",
			edit: "PUT /api/blogs/host/:ID",
			getOne: "GET /api/blogs/host/:ID",
			get: "GET /api/blogs/host",
			remove: "DELETE /api/blogs/host/:ID",
		};
		this.data = {
			get: {},
		};

		this.table;

		this.postModal = new WWSUmodal(`New Blog Post`, null, ``, true, {
			overlayClose: false,
			zindex: 1110,
		});

		this.assignSocketEvent("blogs", this.manager.socket);

		this.on("change", "WWSUblogs", () => {
			this.updateTable();
		});
	}

	/**
	 * Call this upon socket connection.
	 */
	init() {
		this.replaceData(
			this.manager.get("hostReq"),
			this.endpoints.get,
			this.data.get
		);
	}

	/**
	 * Add a new blog entry via the API.
	 *
	 * @param {object} data Data to be passed to the API.
	 * @param {?function} cb Callback after API call is made. Parameter is true on success.
	 */
	add(data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.add,
			{
				data: data,
			},
			{
				domModal: this.postModal.id,
			},
			(body, res) => {
				if (res.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Blog post added",
						autohide: true,
						delay: 10000,
						body: `Blog post has been added.`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Edit a blog post in the system
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	edit(data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.edit,
			{
				data: data,
			},
			{
				domModal: this.postModal.id,
			},
			(body, res) => {
				if (res.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Blog post edited",
						autohide: true,
						delay: 10000,
						body: `Blog post has been edited`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Remove a blog post from the system.
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	remove(data, cb) {
		this.manager.get("directorReq").request(
			this.endpoints.remove,
			{
				data: data,
			},
			{},
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Blog post Removed",
						autohide: true,
						delay: 30000,
						body: `Blog post has been removed`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Get full details of a blog post in the system.
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter if successful will be blog data, else false.
	 */
	getOne(data, cb) {
		this.manager.get("hostReq").request(
			this.endpoints.getOne,
			{
				data: data,
			},
			{},
			(body, res) => {
				if (res.statusCode >= 400 || typeof body !== "object" || !body.ID) {
					cb(false);
				} else {
					cb(body);
				}
			}
		);
	}

	/**
	 * Initialize blog management table.
	 *
	 * @param {string} table The DOM query string of the div container that should house the table.
	 */
	initTable(table) {
		this.manager.get("WWSUanimations").add("blogs-init-table", () => {
			// Init html
			$(table).html(
				`<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
					this.manager.get("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				}.</p><p><button type="button" class="btn btn-block btn-success btn-blog-new">New Blog Post</button></p><table id="section-blogs-table" class="table table-striped display responsive" style="width: 100%;"></table>`
			);

			this.manager
				.get("WWSUutil")
				.waitForElement(`#section-blogs-table`, () => {
					// Generate table
					this.table = $(`#section-blogs-table`).DataTable({
						paging: true,
						data: [],
						columns: [
							{ title: "ID" },
							{ title: "Categories" },
							{ title: "Title" },
							{ title: "Visible?" },
							{ title: "Actions" },
						],
						columnDefs: [{ responsivePriority: 1, targets: 4 }],
						order: [[0, "desc"]],
						buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
						pageLength: 100,
						drawCallback: () => {
							// Action button click events
							$(".btn-blog-edit").unbind("click");
							$(".btn-blog-delete").unbind("click");

							$(".btn-blog-edit").click((e) => {
								let blog = this.find().find(
									(blog) => blog.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.postModal.iziModal("open");
								this.postModal.body = ``;
								$(`#modal-${this.postModal.id} .modal-content`)
									.prepend(`<div class="overlay">
									<i class="fas fa-2x fa-sync fa-spin"></i>
									<h1 class="text-white">Getting blog info...</h1>
								</div>`);
								this.getOne({ ID: blog.ID }, (cb) => {
									$(`#modal-${this.postModal.id} .overlay`).remove();
									if (!cb) {
										this.postModal.iziModal("close");
										return;
									}
									this.showBlogForm(cb);
								});
							});

							$(".btn-blog-delete").click((e) => {
								let blog = this.find().find(
									(blog) => blog.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.manager.get("WWSUutil").confirmDialog(
									`Are you sure you want to <strong>permanently</strong> remove the blog post "${blog.title}" (ID: ${blog.ID})?
                                <ul>
                                <li><strong>This action cannot be undone!</strong> The blog post will be removed forever and will no longer show up on the website.</li>
                                </ul>`,
									blog.ID,
									() => {
										this.remove({ ID: blog.ID });
									}
								);
							});
						},
					});

					this.table
						.buttons()
						.container()
						.appendTo(`#section-blogs-table_wrapper .col-md-6:eq(0)`);

					// Add click event for new DJ button
					$(".btn-blog-new").unbind("click");
					$(".btn-blog-new").click(() => {
						this.showBlogForm();
					});

					// Update with information
					this.updateTable();
				});
		});
	}

	/**
	 * Update the Blogs management table if it exists
	 */
	updateTable() {
		this.manager.get("WWSUanimations").add("blogs-update-table", () => {
			if (this.table) {
				this.table.clear();
				this.find().forEach((blog) => {
					// Get categories
					let categoryBadges = ``;
					if (blog.categories) {
						categoryBadges = blog.categories
							.map((category) => {
								switch (category) {
									case "sports":
										return `<span class="badge badge-success p-1">${category}</span>`;
									case "music":
										return `<span class="badge bg-blue p-1">${category}</span>`;
									case "show":
										return `<span class="badge badge-danger p-1">${category}</span>`;
									case "station":
										return `<span class="badge badge-primary p-1">${category}</span>`;
									default:
										return `<span class="badge badge-secondary p-1">${category}</span>`;
								}
							})
							.join("");
					}
					let activeBadge = `<span class="badge badge-success" title="This blog post is visible on the website."><i class="fas fa-check-circle p-1"></i>Yes</span>`;
					if (!blog.active) {
						activeBadge = `<span class="badge badge-danger" title="This blog post is not visible on the website: It is marked inactive."><i class="far fa-times-circle p-1"></i>No</span>`;
					} else if (
						blog.expires &&
						moment(this.manager.get("WWSUMeta").meta.time).isAfter(
							moment(blog.expires)
						)
					) {
						activeBadge = `<span class="badge bg-orange" title="This blog post is not visible on the website: It is expired."><i class="far fa-times-circle p-1"></i>Expired</span>`;
					} else if (blog.needsApproved) {
						activeBadge = `<span class="badge badge-warning" title="This blog post is not visible on the website: It needs approval from a director."><i class="far fa-times-circle p-1"></i>Needs Approved</span>`;
					} else if (
						blog.starts &&
						moment(this.manager.get("WWSUMeta").meta.time).isBefore(
							moment(blog.starts)
						)
					) {
						activeBadge = `<span class="badge badge-info" title="This blog post is not visible on the website: The start date has not been reached yet."><i class="far fa-times-circle p-1"></i>Too Soon</span>`;
					}
					this.table.row.add([
						blog.ID,
						categoryBadges,
						blog.title,
						activeBadge,
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-blog-edit" data-id="${blog.ID}" title="Edit blog post"><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-blog-delete" data-id="${blog.ID}" title="Remove blog post"><i class="fas fa-trash"></i></button></div>`,
					]);
				});
				this.table.draw(false);
			}
		});
	}

	/**
	 * Show an Alpaca form for blog entry.
	 */
	showBlogForm(data) {
		// Properly format featured image
		if (data && data.featuredImage) {
			data.featuredImage = {
				id: data.featuredImage,
				name: "Featured Image",
				size: "N/A",
				url: `https://server.wwsu1069.org/api/uploads/${data.featuredImage}`,
				thumbnailUrl: `https://server.wwsu1069.org/api/uploads/${data.featuredImage}`,
				deleteUrl: `https://server.wwsu1069.org/api/uploads/${
					data.featuredImage
				}?host=${
					this.manager.has("WWSUhosts")
						? this.manager.get("WWSUhosts").client.host
						: "null"
				}`,
				deleteType: "DELETE",
			};
		}

		// Inactive DJs might still be set as blog authors, so fetch inactive ones too.
		let _djs = this.manager.get("WWSUdjs").find();
		let blogCategories = ["sports", "music", "show", "station", "general"];

		this.postModal.iziModal("open");
		this.postModal.body = ``;

		$(this.postModal.body).alpaca({
			schema: {
				title: data ? "Edit Blog Post" : "New Blog Post",
				type: "object",
				properties: {
					ID: {
						type: "number",
					},
					active: {
						type: "boolean",
						title: "Active Post?",
						default: data ? data.active : true,
					},
					approved: {
						type: "boolean",
						title: "Approved Post?",
						default: data ? !data.needsApproved : true,
					},
					categories: {
						type: "array",
						items: {
							type: "number",
						},
						required: true,
						title: "Blog Categories",
						enum: blogCategories,
						default: data && data.categories ? data.categories : [],
					},
					author: {
						type: "number",
						title: "Author (Org Member)",
						required: true,
						enum: _djs.map((dj) => dj.ID),
					},
					title: {
						type: "string",
						required: true,
						title: "Title / Headline",
						maxLength: 128,
					},
					contents: {
						type: "string",
						required: true,
						title: "Contents",
					},
					summary: {
						type: "string",
						required: true,
						title: "Excerpt / Summary",
						maxLength: 255,
					},
					tags: {
						type: "array",
						items: {
							type: "string",
							maxLength: 64,
						},
						title: "Tags / Keywords",
					},
					featuredImage: {
						type: "array",
						title: "Featured Image",
					},
					starts: {
						title: "Starts",
						format: "datetime",
					},
					expires: {
						title: "Expires",
						format: "datetime",
					},
				},
			},
			view: {
				wizard: {
					bindings: {
						ID: 1,
						active: 1,
						approved: 1,
						categories: 1,
						author: 1,
						title: 2,
						contents: 2,
						summary: 2,
						tags: 2,
						featuredImage: 2,
						starts: 3,
						expires: 3,
					},
					steps: [
						{
							title: "Settings",
						},
						{
							title: "Content",
						},
						{
							title: "Dates",
						},
					],
				},
			},
			options: {
				fields: {
					ID: {
						type: "hidden",
					},
					active: {
						rightLabel: "Yes",
						helper:
							"If unchecked, this post will NOT be visible on the website regardless of the Approved or starts/expires settings.",
					},
					approved: {
						rightLabel: "Yes",
						helper:
							"If unchecked, this post will NOT be visible on the website regardless of the Active or starts/expires settings. Posts needing approved will be prompted in Administration -> To-Do.",
					},
					categories: {
						type: "select",
						helper:
							"Choose which category or categories this post belongs to; they will be categorizes on the relevant blog pages on the website.",
						multiple: true,
					},
					author: {
						type: "select",
						optionLabels: _djs.map((dj) => `${dj.name} (${dj.realName})`),
						helpers: [
							"The org member who authored this blog post.",
							"If the specified org member is deleted, this blog post will be marked anonymous; you will have to set a new author when editing the post.",
						],
					},
					title: {
						helpers: [
							"Please use Associated Press styling, and begin titles of critical blog posts with BREAKING: .",
							"Limit is 128 characters.",
						],
					},
					contents: {
						type: "tinymce",
						options: {
							toolbar:
								"undo redo restoredraft | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | fullscreen preview | image link | ltr rtl",
							plugins:
								"autoresize autosave preview paste importcss searchreplace autolink save directionality visualblocks visualchars fullscreen image link table hr pagebreak nonbreaking toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help quickbars",
							menubar: "file edit view insert format tools table help",
							a11y_advanced_options: true,
							autosave_ask_before_unload: false,
							autosave_retention: "180m",
						},
						helper:
							"Provide the full blog post contents here. Please use Associated Press style guidelines.",
					},
					summary: {
						type: "textarea",
						helpers: [
							"Provide a short, concise summary for this blog post. Please follow Associated Press guidelines.",
							"Limit 255 characters.",
						],
					},
					tags: {
						helper:
							"Optionally provide tags or keywords people can use to easily search or find blogs similar to this one. Each tag has a limit of 64 characters.",
						actionbar: {
							showLabels: true,
							actions: [
								{
									label: "Add",
									action: "add",
								},
								{
									label: "Remove",
									action: "remove",
								},
							],
						},
					},
					featuredImage: {
						type: "upload",
						maxNumberOfFiles: 1,
						fileTypes: "image/*",
						maxFileSize: 1024 * 1024, // 1 MB file size limit
						upload: {
							autoUpload: false,
							formData: {
								type: "blogs",
								host: this.manager.get("WWSUhosts").client.host,
							},
							url: "https://server.wwsu1069.org/api/uploads", // TODO: Programmatic config
						},
						helpers: [
							"Optionally, provide an image to be used as the featured image for this post. Do NOT use images from other sources without their permission.",
							"Max file size is 1 MB. Will only use the first image in the list.",
						],
					},
					starts: {
						dateFormat: `YYYY-MM-DD hh:mm A`,
						picker: {
							inline: true,
							sideBySide: true,
						},
						helpers: [
							"Optionally, specify when this blog post should be published (blank = immediately, providing active and approved are checked)",
							`Specify the date/time in the station timezone of ${this.timezone}`,
						],
					},
					expires: {
						dateFormat: `YYYY-MM-DD hh:mm A`,
						picker: {
							inline: true,
							sideBySide: true,
						},
						helpers: [
							"Optionally, specify when this blog post should expire and no longer appear on the website (blank = never, though it will still be deleted according to the daily cleanup)",
							`Specify the date/time in the station timezone of ${this.timezone}`,
						],
					},
				},
				form: {
					buttons: {
						submit: {
							title: data ? "Edit Blog Post" : "Add Blog Post",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								value.needsApproved = !value.approved;

								value.featuredImage =
									value.featuredImage && value.featuredImage[0]
										? value.featuredImage[0].id
										: null;

								value.categories = value.categories.map((item) => item.value);

								if (data) {
									this.edit(value, (success) => {
										if (success) {
											this.postModal.iziModal("close");
										}
									});
								} else {
									this.add(value, (success) => {
										if (success) {
											this.postModal.iziModal("close");
										}
									});
								}
							},
						},
					},
				},
			},
			data: data ? data : [],
		});
	}
}
