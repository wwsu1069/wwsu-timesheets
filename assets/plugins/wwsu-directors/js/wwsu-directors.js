"use strict";

// This class manages directors from WWSU.

// REQUIRED WWSUmodules: noReq (WWSUreq), adminDirectorReq (WWSUreq), masterDirectorReq (WWSUreq), WWSUMeta, WWSUutil, WWSUanimations, WWSUlockdown (for viewing lockdown logs), WWSUdjs
class WWSUdirectors extends WWSUdb {
	/**
	 * Construct the directors.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super("WWSUdirectors"); // Create the db

		this.manager = manager;

		this.endpoints = {
			get: "GET /api/directors/:name?",
			add: "POST /api/directors",
			edit: "PUT /api/directors/:ID",
			remove: "DELETE /api/directors/:ID",
		};
		this.data = {
			get: {},
		};

		this.table;

		this.assignSocketEvent("directors", this.manager.socket);

		this.newDirectorModal = new WWSUmodal(`New Director`, null, ``, true, {
			overlayClose: false,
			zindex: 1110,
		});

		this.on("change", "WWSUdirectors", () => {
			this.updateTable();

			// Update which directors are allowed to send emails
			let directorsCanSendEmails = this.db()
				.get()
				.filter((director) => director.ID === 1 || director.canSendEmails)
				.map((director) => director.name)
				.join(", ");
			$(".section-email-allowed-directors").html(directorsCanSendEmails);
		});
	}

	// Initialize the directors class. Call this on socket connect event.
	init() {
		this.replaceData(
			this.manager.get("noReq"),
			this.endpoints.get,
			this.data.get
		);
	}

	/**
	 * Add a new Director to the system via the API
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	addDirector(data, cb) {
		this.manager.get("adminDirectorReq").request(
			this.endpoints.add,
			{
				data: data,
			},
			{ domModal: this.newDirectorModal.id },
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Director Added",
						autohide: true,
						delay: 10000,
						body: `Director has been created`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Edit a Director in the system
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	editDirector(data, cb) {
		this.manager
			.get(data.ID === 1 ? "masterDirectorReq" : "adminDirectorReq")
			.request(
				this.endpoints.edit,
				{
					data: data,
				},
				{ domModal: this.newDirectorModal.id },
				(body, resp) => {
					if (resp.statusCode >= 400) {
						if (typeof cb === "function") cb(false);
					} else {
						$(document).Toasts("create", {
							class: "bg-success",
							title: "Director Edited",
							autohide: true,
							delay: 10000,
							body: `Director has been edited.`,
						});
						if (typeof cb === "function") cb(true);
					}
				}
			);
	}

	/**
	 * Remove a Director from the system.
	 *
	 * @param {Object} data The data to send in the request to the API
	 * @param {function} cb Callback called after the request is complete. Parameter false if unsuccessful or true if it was.
	 */
	removeDirector(data, cb) {
		this.manager.get("adminDirectorReq").request(
			this.endpoints.remove,
			{
				data: data,
			},
			{},
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(false);
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Director Removed",
						autohide: true,
						delay: 10000,
						body: `Director has been removed.`,
					});
					if (typeof cb === "function") cb(true);
				}
			}
		);
	}

	/**
	 * Initialize director management table.
	 *
	 * @param {string} table The DOM query string of the div container that should house the table.
	 */
	initTable(table) {
		this.manager.get("WWSUanimations").add("directors-init-table", () => {
			// Init html
			$(table).html(
				`<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
					this.manager.get("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				}.</p><p><button type="button" class="btn btn-block btn-success btn-director-new">New Director</button></p><table id="section-directors-table" class="table table-striped display responsive" style="width: 100%;"></table>`
			);

			this.manager
				.get("WWSUutil")
				.waitForElement(`#section-directors-table`, () => {
					// Generate table
					this.table = $(`#section-directors-table`).DataTable({
						paging: true,
						data: [],
						columns: [
							{ title: "Name" },
							{ title: "Position" },
							{ title: "Clocked In?" },
							{ title: "Admin?" },
							{ title: "Assistant?" },
							{ title: "Emails" },
							{ title: "Actions" },
						],
						columnDefs: [{ responsivePriority: 1, targets: 6 }],
						order: [[0, "asc"]],
						buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
						pageLength: 100,
						drawCallback: () => {
							// Action button click events
							$(".btn-director-lockdown-logs").unbind("click");
							$(".btn-director-edit").unbind("click");
							$(".btn-director-delete").unbind("click");

							$(".btn-director-edit").click((e) => {
								let director = this.find().find(
									(director) =>
										director.ID === parseInt($(e.currentTarget).data("id"))
								);
								if (director.ID === 1) {
									this.manager.get("WWSUutil").confirmDialog(
										`<p>Are you sure you want to edit the <strong>Master Director</strong>?</p>
									<p>Please be aware that <strong>only the master director may edit the master director</strong>.</p>
									<p>If the master director forgot their password, it must be reset. Please contact Patrick Schmalstig at xanaftp@gmail.com.</p>`,
										null,
										() => {
											this.showDirectorForm(director);
										}
									);
								} else {
									this.showDirectorForm(director);
								}
							});

							$(".btn-director-delete").click((e) => {
								let director = this.find().find(
									(director) =>
										director.ID === parseInt($(e.currentTarget).data("id"))
								);
								this.manager.get("WWSUutil").confirmDialog(
									`Are you sure you want to <strong>permanently</strong> remove the director "${director.name}"?
                                <ul>
                                <li><strong>Do NOT permanently remove a director unless they are no longer working for WWSU.</strong></li>
                                <li>This removes the director from the system, including WWSU timesheets application and anywhere office hours and directors are posted.</li>
                                <li>The director can no longer perform actions that require director authorization.</li>
                                <li>This removes the director's office hours from the calendar.</li>
                                <li>This will disassociate the director from any tasks to which they are assigned.</li>
                                <li>This will delete tasks created by this director.</li>
								<li>Any hosts set as belonging to this director / org member will have their admin setting set to false and therefore can no longer access the DJ Controls administration menu.</li>
                                <li>The timesheet records for the director will remain in the system and can still be viewed. But the director cannot clock in/out anymore.</li>
								<li>The associated org member for this director will remain in active status. <strong>If this director is no longer with WWSU as a member, you should instead mark the org member as inactive via Administration -> Org Members.</strong>. Doing so will also auto-delete this director.</li>
                                </ul>`,
									director.name,
									() => {
										this.removeDirector({ ID: director.ID });
									}
								);
							});

							$(".btn-director-lockdown-logs").click((e) => {
								this.manager.get("WWSUlockdown").showLog({
									type: "director",
									typeID: parseInt($(e.currentTarget).data("id")),
								});
							});
						},
					});

					this.table
						.buttons()
						.container()
						.appendTo(`#section-directors-table_wrapper .col-md-6:eq(0)`);

					// Add click event for new DJ button
					$(".btn-director-new").unbind("click");
					$(".btn-director-new").click(() => {
						this.showDirectorForm();
					});

					// Update with information
					this.updateTable();
				});
		});
	}

	/**
	 * Update the Director management table if it exists
	 */
	updateTable() {
		this.manager.get("WWSUanimations").add("directors-update-table", () => {
			if (this.table) {
				this.table.clear();
				this.find().forEach((director) => {
					this.table.row.add([
						director.name || "",
						director.position || "Unknown Position",
						director.present
							? `<span class="badge ${
									director.present === 2 ? `bg-indigo` : `badge-success`
							  }" title="${director.name} is currently clocked in${
									director.present === 2 ? ` for remote hours` : ``
							  }"><i class="fas fa-check-circle p-1"></i>${
									director.present === 2 ? `Remote` : `Yes`
							  }</span>`
							: `<span class="badge badge-danger" title="${director.name} is not currently clocked in."><i class="far fa-times-circle p-1"></i>No`,
						director.admin
							? `<span class="badge badge-success" title="${director.name} is an administrator director"><i class="fas fa-check-circle p-1"></i>Yes</span>`
							: `<span class="badge badge-danger" title="${director.name} is not an administrator director"><i class="far fa-times-circle p-1"></i>No</span>`,
						director.assistant
							? `<span class="badge badge-success" title="${director.name} is an assistant director"><i class="fas fa-check-circle p-1"></i>Yes</span>`
							: `<span class="badge badge-danger" title="${director.name} is a primary director"><i class="far fa-times-circle p-1"></i>No</span>`,
						`<ul>
						${
							director.canSendEmails
								? `<li><span class="badge badge-success" title="${director.name} can send emails through the email tab of DJ Controls."><i class="fas fa-paper-plane p-1"></i>Can Send Emails</span></li>`
								: ``
						}
						${
							director.emailEmergencies
								? `<li><span class="badge badge-danger" title="${director.name} will receive emails of critical system issues."><i class="fas fa-exclamation-triangle p-1"></i>Emergencies</span></li>`
								: ``
						}${
							director.emailCalendar
								? `<li><span class="badge badge-info" title="${director.name} will receive emails regarding calendar events and shows/broadcasts."><i class="fas fa-calendar p-1"></i>Events</span></li>`
								: ``
						}${
							director.emailWeeklyAnalytics
								? `<li><span class="badge bg-blue" title="${director.name} will receive emails every Sunday 12AM containing weekly analytics."><i class="fas fa-chart-line p-1"></i>Analytics</span></li>`
								: ``
						}${
							director.emailFlags
								? `<li><span class="badge badge-warning" title="${director.name} will receive emails whenever a track or broadcast is reported by someone via the website."><i class="fas fa-flag p-1"></i>Flags</span></li>`
								: ``
						}</ul>`,
						`<div class="btn-group"><button class="btn btn-sm bg-black btn-director-lockdown-logs" data-id="${
							director.ID
						}" title="View lockdown logs for this director"><i class="fas fa-user-lock"></i></button><button class="btn btn-sm btn-warning btn-director-edit" data-id="${
							director.ID
						}" title="Edit director"><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-director-delete" data-id="${
							director.ID
						}" ${
							director.ID === 1
								? `title="Cannot remove the master director" disabled`
								: `title="Remove Director"`
						}><i class="fas fa-trash"></i></button></div>`,
					]);
				});
				this.table.draw(false);
			}
		});
	}

	/**
	 * Make a "New Director" Alpaca form in a modal.
	 */
	showDirectorForm(data) {
		// reset login to null when filling out default values
		if (data && data.login) data.login = null;

		// Force admin and canSendEmails to true for master director
		if (data && data.ID === 1) {
			data.admin = true;
			data.canSendEmails = true;
		}

		// Properly format avatars
		if (data && data.avatar) {
			data.avatar = {
				id: data.avatar,
				name: "Director Headshot",
				size: "N/A",
				url: `https://server.wwsu1069.org/api/uploads/${data.avatar}`,
				thumbnailUrl: `https://server.wwsu1069.org/api/uploads/${data.avatar}`,
				deleteUrl: `https://server.wwsu1069.org/api/uploads/${
					data.avatar
				}?host=${
					this.manager.has("WWSUhosts")
						? this.manager.get("WWSUhosts").client.host
						: "null"
				}`,
				deleteType: "DELETE",
			};
		}

		let _djs = this.manager.get("WWSUdjs").find({ active: true });
		let _directors = this.find();

		this.newDirectorModal.iziModal("open");
		this.newDirectorModal.body = ``;

		$(this.newDirectorModal.body).alpaca({
			schema: {
				title: data ? "Edit Director" : "New Director",
				type: "object",
				properties: {
					ID: {
						type: "number",
					},
					name: {
						type: "string",
						required: true,
						title: "Full Name",
						maxLength: 64,
					},
					position: {
						type: "string",
						title: "Position",
						maxLength: 32,
					},
					admin: {
						type: "boolean",
						title: "Is Admin Director?",
						readonly: data && data.ID === 1,
					},
					assistant: {
						type: "boolean",
						title: "Is Assistant Director?",
					},
					email: {
						type: "string",
						format: "email",
						title: "Change Email Address",
						maxLength: 255,
					},
					login: {
						type: "string",
						format: "password",
						required: data ? false : true,
						title: "Change Password",
						maxLength: 255,
					},
					avatar: {
						type: "array",
						title: "Head Shot",
					},
					profile: {
						type: "string",
						title: "Profile / Bio",
					},
					canSendEmails: {
						type: "boolean",
						title: "Can send emails via DJ Controls?",
						readonly: data && data.ID === 1,
					},
					emailEmergencies: {
						type: "boolean",
						title: "Receive emails of critical issues?",
					},
					emailCalendar: {
						type: "boolean",
						title: "Receive event/calendar related emails?",
					},
					emailWeeklyAnalytics: {
						type: "boolean",
						title: "Receive weekly analytic emails?",
					},
					emailFlags: {
						type: "boolean",
						title: "Receive reported track / broadcast emails?",
					},
				},
			},
			view: {
				wizard: {
					bindings: {
						ID: 1,
						name: 1,
						position: 1,
						admin: 2,
						assistant: 2,
						email: 1,
						login: 2,
						avatar: 1,
						profile: 1,
						canSendEmails: 3,
						emailEmergencies: 3,
						emailCalendar: 3,
						emailWeeklyAnalytics: 3,
						emailFlags: 3,
					},
					steps: [
						{
							title: "Information",
						},
						{
							title: "Permissions",
						},
						{
							title: "Emails",
						},
					],
				},
			},
			options: {
				fields: {
					ID: {
						type: "hidden",
					},
					name: {
						validator: function (callback) {
							let value = this.getValue();

							if (!data || data.name !== value) {
								let _director = _directors.find(
									(director) => director.name === value
								);
								if (_director) {
									callback({
										status: false,
										message: `This director was already added to the system.`,
									});
									return;
								}
							}

							if (data) {
								let _dj2 = _djs.find((dj) => dj.realName === data.name);
								if (_dj2) {
									callback({
										status: true,
										message: `Changing the director's name will also change the full name of their org member entry.`,
									});
									return;
								}
							}

							let _dj = _djs.find((dj) => dj.realName === value);

							if (!_dj) {
								callback({
									status: false,
									message: `An org member with the specified full name was not found. Please first add the director as an org member via Administration -> Org Members (in the DJ Controls menu), and make sure you correctly type the member's full name.`,
								});
								return;
							}

							callback({
								status: true,
							});
						},
					},
					position: {
						helper: "What is the director's official title at WWSU?",
					},
					admin: {
						rightLabel: "Yes",
						helpers: [
							"Check this box if this director is an admin. Admin directors have the power to manage other directors and their timesheets in the system. They can also access door codes and certain passwords. And they can edit or delete org members from the system.",
							"This option cannot be changed for the master director; it is always enabled for them.",
							"<strong>This is a dangerous permission to set!!!</strong> It is recommended never to make a director admin except for the president and maybe vice president.",
						],
					},
					assistant: {
						rightLabel: "Yes",
						helper:
							"Check this box if this director is an assistant, defined by WWSU as an unpaid director.",
					},
					email: {
						helper:
							"Change the email address of the director; the system will send emails to them according to email settings. Type remove@example.com to remove their email address. <strong>WWSU work email is highly recommended, or campus email if the director does not have a WWSU work email.</strong>",
					},
					login: {
						helper:
							"Change the password for the director. Director will use this password to clock in/out and to perform tasks requiring director authorization. <strong>CAUTION!</strong> If the password is forgotten, it must be changed by an/another admin director.",
					},
					avatar: {
						type: "upload",
						maxNumberOfFiles: 1,
						fileTypes: "image/*",
						maxFileSize: 1024 * 1024, // 1 MB file size limit
						upload: {
							autoUpload: false,
							formData: {
								type: "directors",
								host: this.manager.get("WWSUhosts").client.host,
							},
							url: "https://server.wwsu1069.org/api/uploads", // TODO: Programmatic config
						},
						helper:
							"Maximum file size is 1 MB. Will only use first image. Director head shots should be professional and high quality.",
					},
					profile: {
						type: "tinymce",
						options: {
							toolbar:
								"undo redo restoredraft | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | fullscreen preview | image link | ltr rtl",
							plugins:
								"autoresize autosave preview paste importcss searchreplace autolink save directionality visualblocks visualchars fullscreen image link table hr pagebreak nonbreaking toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help quickbars",
							menubar: "file edit view insert format tools table help",
							a11y_advanced_options: true,
							autosave_ask_before_unload: false,
							autosave_retention: "180m",
						},
						helper:
							"Optionally specify a profile / biography for this director. Include relevant details about this member's interests and involvement with WWSU <strong>as a director</strong> (differs from the org member profile). This will be showcased publicly on the website and display signs.",
					},
					canSendEmails: {
						rightLabel: "Yes",
						helpers: [
							"Is this director allowed to send emails via the Emails tab in DJ Controls on behalf of WWSU?",
							"This option cannot be changed for the master director; they are always allowed.",
						],
					},
					emailEmergencies: {
						rightLabel: "Yes",
						helper:
							"Check this box if this director should be emailed when a critical issue occurs with WWSU (Emergencies).",
					},
					emailCalendar: {
						rightLabel: "Yes",
						helper:
							"Check this box if this director should be emailed for calendar/broadcast related matters, such as show absences/cancellations/changes (Events).",
					},
					emailWeeklyAnalytics: {
						rightLabel: "Yes",
						helper:
							"Check this box if this director should be emailed every Sunday 12AM with the broadcast analytics for the week (Analytics).",
					},
					emailFlags: {
						rightLabel: "Yes",
						helper:
							"Check this box if this director should be emailed whenever someone reports a track or a broadcast via the website for inappropriate content (Flags).",
					},
				},
				form: {
					buttons: {
						submit: {
							title: data ? "Edit Director" : "Add Director",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								// Avatars must be formatted to the first id
								value.avatar =
									value.avatar && value.avatar[0] ? value.avatar[0].id : null;

								if (data) {
									this.editDirector(value, (success) => {
										if (success) {
											this.newDirectorModal.iziModal("close");
										}
									});
								} else {
									this.addDirector(value, (success) => {
										if (success) {
											this.newDirectorModal.iziModal("close");
										}
									});
								}
							},
						},
					},
				},
			},
			data: data ? data : [],
		});
	}
}
