"use strict";

// Socket connection
io.sails.url = "https://server.wwsu1069.org";
io.sails.reconnection = true;
io.sails.reconnectionAttempts = Infinity;
let socket = io.sails.connect();

// Create WWSU modules
let wwsumodules = new WWSUmodules(socket);
wwsumodules.add("noReq", WWSUreq, { host: null }).add("WWSUsports", WWSUsports);
let sports = wwsumodules.get("WWSUsports");

// Add listener for changes in sports values
const changeData = (data) => {
  console.dir(data);
  switch (data.name) {
    case `wsuScore`:
      document.querySelector("#wsu-score").value = data.value;
      break;
    case `oppScore`:
      document.querySelector("#opp-score").value = data.value;
      break;
    case `wsuNum`:
      document.querySelector("#wsu-num").value = data.value;
      break;
    case `oppNum`:
      document.querySelector("#opp-num").value = data.value;
      break;
    case `wsuText`:
      document.querySelector("#wsu-text").value = data.value;
      break;
    case `oppText`:
      document.querySelector("#opp-text").value = data.value;
      break;
    case `wsuTimeouts`:
      document.querySelector("#wsu-timeouts").value = data.value;
      break;
    case `oppTimeouts`:
      document.querySelector("#opp-timeouts").value = data.value;
      break;
    case `wsuFouls`:
      document.querySelector("#wsu-fouls").value = data.value;
      break;
    case `oppFouls`:
      document.querySelector("#opp-fouls").value = data.value;
      break;
  }
}
sports.on("insert", "renderer", (data) => {
  changeData(data);
});
sports.on("update", "renderer", (data) => {
  changeData(data);
});
sports.on("replace", "renderer", (db) => {
	db.each((data) => {
    changeData(data);
	});
});

socket.on("connect", () => {
	sports.init();
});

socket.on("disconnect", () => {
	console.log("Lost connection");
	try {
		socket._raw.io._reconnection = true;
		socket._raw.io._reconnectionAttempts = Infinity;
	} catch (e) {
		console.error(e);
	}
});

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function wsuScore(amount) {
	sports.updateValue(
		`wsuScore`,
		parseInt(document.querySelector("#wsu-score").value) + amount
	);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function oppScore(amount) {
	sports.updateValue(
		`oppScore`,
		parseInt(document.querySelector("#opp-score").value) + amount
	);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function clearAlt() {
	sports.updateValue(`wsuText`, ``);
	sports.updateValue(`oppText`, ``);
	sports.updateValue(`wsuNum`, ``);
	sports.updateValue(`oppNum`, ``);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function showAlt() {
	sports.updateValue(`wsuText`, document.querySelector("#wsu-text").value);
	sports.updateValue(`oppText`, document.querySelector("#opp-text").value);
	sports.updateValue(`wsuNum`, document.querySelector("#wsu-num").value);
	sports.updateValue(`oppNum`, document.querySelector("#opp-num").value);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function wsuTimeout() {
	document.querySelector("#wsu-timeouts").value =
		parseInt(document.querySelector("#wsu-timeouts").value) - 1;
	sports.updateValue(
		`wsuTimeouts`,
		document.querySelector("#wsu-timeouts").value
	);
	sports.updateValue(`wsuText`, `TIMEOUT TAKEN`);
	sports.updateValue(`oppText`, ``);
	sports.updateValue(`wsuNum`, document.querySelector("#wsu-timeouts").value);
	sports.updateValue(`oppNum`, document.querySelector("#opp-timeouts").value);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function oppTimeout() {
	document.querySelector("#opp-timeouts").value =
		parseInt(document.querySelector("#opp-timeouts").value) - 1;
	sports.updateValue(
		`oppTimeouts`,
		document.querySelector("#opp-timeouts").value
	);
	sports.updateValue(`wsuText`, ``);
	sports.updateValue(`oppText`, `TIMEOUT TAKEN`);
	sports.updateValue(`wsuNum`, document.querySelector("#wsu-timeouts").value);
	sports.updateValue(`oppNum`, document.querySelector("#opp-timeouts").value);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function mediaTimeout() {
	sports.updateValue(`wsuText`, `MEDIA TIMEOUT`);
	sports.updateValue(`oppText`, `MEDIA TIMEOUT`);
	sports.updateValue(`wsuNum`, ``);
	sports.updateValue(`oppNum`, ``);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function refReview() {
	sports.updateValue(`wsuText`, `REFEREE REVIEW`);
	sports.updateValue(`oppText`, `REFEREE REVIEW`);
	sports.updateValue(`wsuNum`, ``);
	sports.updateValue(`oppNum`, ``);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function wsuInjury() {
	sports.updateValue(`wsuText`, `INJURY TIMEOUT`);
	sports.updateValue(`oppText`, ``);
	sports.updateValue(`wsuNum`, ``);
	sports.updateValue(`oppNum`, ``);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function oppInjury() {
	sports.updateValue(`wsuText`, ``);
	sports.updateValue(`oppText`, `INJURY TIMEOUT`);
	sports.updateValue(`wsuNum`, ``);
	sports.updateValue(`oppNum`, ``);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function halftime() {
	sports.updateValue(`wsuText`, `HALFTIME`);
	sports.updateValue(`oppText`, `HALFTIME`);
	sports.updateValue(`wsuNum`, ``);
	sports.updateValue(`oppNum`, ``);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function final() {
	sports.updateValue(`wsuText`, `FINAL`);
	sports.updateValue(`oppText`, `FINAL`);
	sports.updateValue(`wsuNum`, ``);
	sports.updateValue(`oppNum`, ``);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function coachChallenge() {
	sports.updateValue(`wsuText`, `COACH'S CHALLENGE`);
	sports.updateValue(`oppText`, `COACH'S CHALLENGE`);
	sports.updateValue(`wsuNum`, ``);
	sports.updateValue(`oppNum`, ``);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function wsuFoul() {
	document.querySelector("#wsu-fouls").value =
		parseInt(document.querySelector("#wsu-fouls").value) + 1;
	sports.updateValue(`wsuFouls`, document.querySelector("#wsu-fouls").value);
	sports.updateValue(`wsuText`, `FOUL`);
	sports.updateValue(`oppText`, ``);
	sports.updateValue(`wsuNum`, document.querySelector("#wsu-fouls").value);
	sports.updateValue(`oppNum`, document.querySelector("#opp-fouls").value);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function oppFoul() {
	document.querySelector("#opp-fouls").value =
		parseInt(document.querySelector("#opp-fouls").value) + 1;
	sports.updateValue(`oppFouls`, document.querySelector("#opp-fouls").value);
	sports.updateValue(`wsuText`, ``);
	sports.updateValue(`oppText`, `FOUL`);
	sports.updateValue(`wsuNum`, document.querySelector("#wsu-fouls").value);
	sports.updateValue(`oppNum`, document.querySelector("#opp-fouls").value);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function flagDown() {
	sports.updateValue(`wsuText`, `FLAG`);
	sports.updateValue(`oppText`, `FLAG`);
}

// USED IN HTML
// eslint-disable-next-line no-unused-vars
function displayDown() {
	var temp = document.querySelector("#down-number");
	var temp2 = document.querySelector("#down-yards");
	var temp3 = document.querySelector("#down-possession");

	if (temp !== null && temp2 !== null && temp3 !== null) {
		var selection = temp.options[temp.selectedIndex].value;
		var text = `${selection} AND ${temp2.value > 0 ? temp2.value : "GOAL"}`;
		if (temp3.checked) {
			sports.updateValue(`wsuText`, text);
			sports.updateValue(`oppText`, ``);
		} else {
			sports.updateValue(`wsuText`, ``);
			sports.updateValue(`oppText`, text);
		}
	}
}
