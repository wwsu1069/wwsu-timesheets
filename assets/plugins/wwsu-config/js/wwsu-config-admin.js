"use strict";

/**
 * This class extends WWSUconfig by providing a database of WWSU config and functions to edit it.
 *
 * @requires WWSUconfig
 * @requires WWSUdb
 * @requires WWSUutil via WWSUmodules
 * @requires WWSUreq noReq via WWSUmodules
 * @requires WWSUreq hostReq via WWSUmodules
 * @requires WWSUreq directorReq via WWSUmodules
 * @requires WWSUreq adminDirectorReq via WWSUmodules
 * @requires $.alpaca
 * @requires $ jQuery
 * @requires WWSUmodal
 * @Requires WWSUMeta via WWSUmodules
 */
class WWSUconfigadmin extends WWSUconfig {
	constructor(manager, options = {}) {
		super(manager, options);

		this.endpoints = {
			get: "GET /api/config",
			startOfSemester: {
				edit: "PUT /api/config/start-of-semester",
			},
			secrets: {
				edit: "PUT /api/config/secrets",
			},
			skyway: {
				edit: "PUT /api/config/skyway",
			},
			requests: {
				edit: "PUT /api/config/requests",
			},
			maintenance: {
				edit: "PUT /api/config/maintenance",
			},
			radiodjs: {
				remove: "DELETE /api/config/radiodjs/:ID",
				add: "POST /api/config/radiodjs",
				edit: "PUT /api/config/radiodjs/:ID",
			},
			songlikes: {
				edit: "PUT /api/config/songlikes",
			},
			songs: {
				long: {
					get: "GET /api/songs/long",
					disable: "PUT /api/songs/long/:ID?",
				},
			},
			categories: {
				get: "GET /api/categories",
				remove: "DELETE /api/config/categories/:ID",
				add: "POST /api/config/categories",
				edit: "PUT /api/config/categories/:ID",
			},
			breaks: {
				remove: "DELETE /api/config/breaks/:ID",
				add: "POST /api/config/breaks",
				edit: "PUT /api/config/breaks/:ID",
			},
			maxQueue: {
				edit: "PUT /api/config/max-queue",
			},
			filterProfanity: {
				edit: "PUT /api/config/filter-profanity",
			},
			sanitize: {
				edit: "PUT /api/config/sanitize",
			},
			discord: {
				edit: "PUT /api/config/discord",
			},
			websites: {
				edit: "PUT /api/config/websites",
			},
			shoutcast: {
				edit: "PUT /api/config/shoutcast",
			},
			owncast: {
				edit: "PUT /api/config/owncast",
			},
			status: {
				edit: "PUT /api/config/status",
			},
			state: {
				ChangeRadioDj: "POST /api/state/radiodj/change",
			},
			server: {
				rebootSails: "POST /api/server/sails/reboot",
			},
			nws: {
				remove: "DELETE /api/config/nws/:ID",
				add: "POST /api/config/nws",
				edit: "PUT /api/config/nws/:ID",
			},
			sports: {
				remove: "DELETE /api/config/sports/:ID",
				add: "POST /api/config/sports",
				edit: "PUT /api/config/sports/:ID",
			},
			breakTimes: {
				edit: "PUT /api/config/break-times",
			},
			tomorrowio: {
				edit: "PUT /api/config/tomorrowio",
			},
			campusAlerts: {
				edit: "PUT /api/config/campus-alerts",
			},
			bookings: {
				edit: "PUT /api/config/bookings",
			},
			scores: {
				edit: "PUT /api/config/scores",
			},
			onesignal: {
				edit: "PUT /api/config/onesignal",
			},
			reputation: {
				edit: "PUT /api/config/reputation",
			},
		};

		// Modal for config editing
		this.modal = new WWSUmodal(`Change Settings`, null, ``, true, {
			zindex: 1100,
		});

		this.modal2 = new WWSUmodal(`Change Setting`, null, ``, true, {
			zindex: 1110,
		});

		this.table;

		// Function to call whenever config is updated to update the active table.
		this.configUpdateCb = () => {};
		this.on("change", "WWSuconfigadmin", (system, db, query) => {
			try {
				this.configUpdateCb(system, db, query);
			} catch (e) {
				console.error(e);
			}
		});
	}

	/**
	 * Generic request function for making changes to configuration.
	 *
	 * @param {string} endpoint REST-like URL to request (use this.endpoints)
	 * @param {object} data Data to send to the API
	 * @param {string} domModal DOM modal query string to block while processing
	 * @param {function} cb Callback when executed. Contains response body on success, false on failure.
	 */
	change(endpoint, data, domModal, cb) {
		this.manager
			.get("directorReq")
			.request(endpoint, { data }, { domModal }, (body, resp) => {
				if (resp.statusCode < 400) {
					$(document).Toasts("create", {
						class: "bg-success",
						title: `Success: ${endpoint}`,
						autohide: true,
						delay: 10000,
						body: `Configuration has been changed.`,
					});

					cb(body);
				} else {
					cb(false);
				}
			});
	}

	/**
	 * Toggle maintenance mode.
	 */
	toggleMaintenance() {
		let data = this.basic;
		this.manager
			.get("WWSUutil")
			.confirmDialog(
				`Are you sure you want to <strong>${
					data.maintenance ? `DISABLE` : `ENABLE`
				}</strong> maintenance mode (it is currently ${
					data.maintenance ? `enabled` : `disabled`
				})?`,
				null,
				() => {
					this.change(
						this.endpoints.maintenance.edit,
						{ maintenance: !data.maintenance },
						undefined,
						(body) => {}
					);
				}
			);
	}

	/**
	 * Change which RadioDJ is active.
	 */
	changeRadioDJ() {
		// Error if we are not using RadioDJ.
		if (!this.usingRadioDJ) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "RadioDJs not being used",
				body: "You cannot change the active RadioDJ because there are no RadioDJs configured in DJ Controls -> System Settings -> RadioDJs. If using another automation system, manually switch instances instead.",
				autohide: true,
				delay: 15000,
				icon: "fas fa-skull-crossbones",
			});
			return;
		}

		this.manager.get("WWSUutil").confirmDialog(
			`Are you sure you want to change which RadioDJ is active?<br /><br />
			
Proceeding will randomly choose another configured RadioDJ (with good status) to take over programming. If the current RadioDJ is the only one with good status, then the current RadioDJ will be refreshed instead of changing RadioDJs.`,
			null,
			() => {
				this.manager
					.get("directorReq")
					.request(
						this.endpoints.state.ChangeRadioDj,
						{},
						{},
						(body, res) => {}
					);
			}
		);
	}

	/**
	 * Request a reboot of the sailsJS server.
	 */
	reboot() {
		// Disallow reboot when not in automation unless maintenance mode is active
		if (
			!this.basic.maintenance &&
			[
				"automation_on",
				"automation_break",
				"automation_playlist",
				"automation_genre",
				"unknown",
				"",
			].indexOf(this.manager.get("WWSUMeta").meta.state) === -1
		) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "Broadcast in progress",
				body: "You cannot reboot sailsJS at this time; a broadcast is in progress.<br /><br />If you ABSOLUTELY MUST reboot right now, activate maintenance mode and then try again.",
				autohide: true,
				delay: 20000,
				icon: "fas fa-skull-crossbones",
			});
			return;
		}

		this.manager.get("WWSUutil").confirmDialog(
			`Are you sure you want to reboot the sailsJS app?<br /><br />
			
Remember, this assumes the sailsJS app is running on a process manager. If not, this will terminate SailsJS instead of rebooting it.`,
			null,
			() => {
				this.manager
					.get("adminDirectorReq")
					.request(
						this.endpoints.server.rebootSails,
						{},
						{},
						(body, res) => {}
					);
			}
		);
	}

	/**
	 * Display a list of RadioDJ music tracks detected as too long, and manage (disable) ones as appropriate.
	 */
	longTracks() {
		// Error if we are not using RadioDJ.
		if (!this.usingRadioDJ) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "RadioDJs not being used",
				body: "Management of long music tracks is only for RadioDJ, but RadioDJ has not been configured for use. To configure, go to DJ Controls -> System Settings -> RadioDJs. Otherwise, break and tracks management should take place in your automation system.",
				autohide: true,
				delay: 30000,
				icon: "fas fa-skull-crossbones",
			});
			return;
		}

		// Prep private function called to load / re-load table data.
		const loadLongTracks = () => {
			this.manager
				.get("hostReq")
				.request(this.endpoints.songs.long.get, {}, {}, (body, resp) => {
					if (resp.statusCode < 400) {
						if (this.table) {
							this.table.clear();
							body.forEach((item) => {
								this.table.row.add([
									`${item.artist} - ${item.title}`,
									item.category,
									`<button class="btn btn-sm bg-secondary btn-longtracks-disable" data-id="${item.ID}" title="Disable this track in RadioDJ"><i class="fas fa-ban"></i></button>`,
								]);
							});
							this.table.draw(false);
						}
					}
				});
		};

		// Do nothing on config updates
		this.configUpdateCb = () => {};

		this.modal.iziModal("open");

		this.modal.title = `Music Tracks Marked Too Long`;
		this.modal.body = ``;

		$(this.modal.body).html(
			`<table id="section-maintenance-longtracks-table" class="table table-striped display responsive" style="width: 100%;"></table>
			<h5>Actions Key:</h5>
			<div class="container-fluid">
				<div class="row">
					<div class="col">
						<span class="badge badge-secondary"
							><i class="fas fa-ban"></i></span
						>Disable Track
					</div>
				</div>
			</div>`
		);

		this.manager
			.get("WWSUutil")
			.waitForElement(`#section-maintenance-longtracks-table`, () => {
				this.table = $(`#section-maintenance-longtracks-table`).DataTable({
					paging: true,
					data: [],
					columns: [
						{ title: "Track" },
						{ title: "Category" },
						{ title: "Disable" },
					],
					columnDefs: [{ responsivePriority: 1, targets: 2 }],
					order: [[0, "asc"]],
					pageLength: 100,
					drawCallback: () => {
						// Action button click events
						$(".btn-longtracks-disable").unbind("click");
						$(".btn-longtracks-disable").click((e) => {
							let ID = parseInt($(e.currentTarget).data("id"));

							this.change(
								this.endpoints.songs.long.disable,
								{ ID },
								this.modal.id,
								(body) => {
									loadLongTracks(); // Reload table
								}
							);
						});
					},
				});
				loadLongTracks();
			});
	}

	/**
	 * Change the start date of the current semester.
	 */
	startOfSemester() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Start of Semester`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Change Start of Semester",
				type: "object",
				properties: {
					startOfSemester: {
						format: "date",
						title: "Start Date of Current Semester",
						required: true,
					},
				},
			},
			options: {
				fields: {
					startOfSemester: {
						dateFormat: `YYYY-MM-DD`,
						helper: `Choose the first date of the current semester. All semesterly analytics will account for broadcasts on or after this date.`,
						picker: {
							inline: true,
							sideBySide: true,
						},
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Start of Semester`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.startOfSemester.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				startOfSemester: this.analytics.startOfSemester
					? moment(this.analytics.startOfSemester).format("YYYY-MM-DD")
					: undefined,
			},
		});
	}

	/**
	 * Change secret keys in the system.
	 */
	secrets() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Secrets`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Change Secrets",
				type: "object",
				properties: {
					hostSecret: {
						type: "string",
						format: "password",
						title: "Change Host Secret",
						maxLength: 32,
					},
					metaSecret: {
						type: "string",
						format: "password",
						title: "Change Meta Secret",
						maxLength: 32,
					},
				},
			},
			options: {
				fields: {
					hostSecret: {
						helpers: [
							`Specify a new unique / random string (max 32 characters) to change the secret used to conceal IP addresses / host hashes for security.`,
							`WARNING: Changing this value will invalidate all active discipline!`,
						],
					},
					metaSecret: {
						helper:
							"This is a random string that should be sent when calling GET /api/meta/update/:metaSecret or PUT /api/meta/:metaSecret by the automation system to report what it is playing. For example, if you set this to hamburger, you would call PUT /api/meta/hamburger or GET /api/meta/update/hamburger.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Secrets`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.secrets.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
		});
	}

	/**
	 * Change skyway.js settings.
	 */
	skyway() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Skyway.js Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Change Skyway.js Settings",
				type: "object",
				properties: {
					skywayAPI: {
						type: "string",
						title: "Skyway.js API Key",
					},
					skywaySecret: {
						type: "string",
						format: "password",
						title: "Change Skyway.js Secret",
					},
				},
			},
			options: {
				fields: {
					skywayAPI: {
						helper: `If set to blank, skyway.js use will be disabled, and remote broadcasts through DJ Controls will not be possible.`,
					},
					skywaySecret: {
						helper: `Provide a new secret to change this setting; the current secret is not displayed for security.`,
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Skyway.js Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.skyway.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				skywayAPI: this.basic.skywayAPI,
			},
		});

		$(this.modal.body).prepend(`<div class="callout callout-info">
		<h5>Skyway Website</h5>
		<p>
			The Skyway.js website where you can manage your API key is <a href="https://webrtc.ecl.ntt.com/" target="_blank">https://webrtc.ecl.ntt.com/</a>
		</p>
	</div>`);
	}

	/**
	 * Change track requests system settings.
	 */
	requests() {
		// Error if we are not using RadioDJ.
		if (!this.usingRadioDJ) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "RadioDJs not being used",
				body: "The track requests system inside DJ Controls is for RadioDJ, but no RadioDJs are configured. To configure, go to DJ Controls -> System Settings -> RadioDJs. Otherwise, if using another automation system, use the track request system that came with your automation system.",
				autohide: true,
				delay: 30000,
				icon: "fas fa-skull-crossbones",
			});
			return;
		}

		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Track Request Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Change Track Request Settings",
				type: "object",
				properties: {
					requestsDailyLimit: {
						type: "number",
						required: true,
						minimum: 0,
						title: "Daily Request Limit per IP",
					},
					requestsPriorityChange: {
						type: "number",
						required: true,
						minimum: -100,
						maximum: 100,
						title: "Adjust track priority when requested",
					},
				},
			},
			options: {
				fields: {
					requestsDailyLimit: {
						helper:
							"The number of track requests that can be placed per day per IP address, reset at midnight station/server time.",
					},
					requestsPriorityChange: {
						helper:
							"When a track is requested, its priority in RadioDJ will be adjusted by this much (negative number decreases, positive number increases).",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Track Request Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.requests.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				requestsDailyLimit: this.basic.requestsDailyLimit,
				requestsPriorityChange: this.basic.requestsPriorityChange,
			},
		});

		$(this.modal.body).prepend(`<div class="callout callout-info">
		<h5>Other settings</h5>
		<p>
			The track request system will also restrict tracks based on rotation rules set in RadioDJ, and individual track settings like start/end date and number of allowed spins.
		</p>
	</div>`);
	}

	/**
	 * Load table of RadioDJs to manage.
	 */
	manageRadioDJs() {
		// Update the function called on config changes so this table is updated when radiodjs are updated.
		this.configUpdateCb = (system, db, query) => {
			if (system !== `config-radiodjs`) return;
			if (this.table) {
				this.table.clear();
				this.radiodjs.forEach((radiodj) => {
					this.table.row.add([
						radiodj.name,
						radiodj.label,
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-settings-radiodjs-edit" data-id="${radiodj.ID}" title="Edit this RadioDJ instance."><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-settings-radiodjs-delete" data-id="${radiodj.ID}" title="Delete this RadioDJ instance."><i class="fas fa-trash"></i></button></div>`,
					]);
				});
				this.table.draw(false);
			}
		};

		// Private function for RadioDJ form
		const editRadioDJ = (data) => {
			this.modal2.iziModal("open");
			this.modal2.title = `${data ? "Edit " : "Add "} RadioDJ`;
			this.modal2.body = ``;

			$(this.modal2.body).alpaca({
				schema: {
					title: `${data ? "Edit " : "Add "} RadioDJ instance`,
					type: "object",
					properties: {
						ID: {
							type: "number",
						},
						name: {
							type: "string",
							title: "Alphanumeric name",
							required: true,
							pattern: /^[a-zA-Z0-9-_]+$/,
						},

						label: {
							type: "string",
							title: "Human Friendly Name / Label",
							required: true,
						},

						restURL: {
							type: "string",
							title: "RadioDJ REST Server URL",
							required: true,
						},

						restPassword: {
							type: "string",
							format: "password",
							title: "RadioDJ REST Server password",
							required: data ? false : true,
						},

						errorLevel: {
							type: "number",
							minimum: 1,
							maximum: 5,
							title: "Error level to trigger",
						},
					},
				},
				options: {
					fields: {
						ID: {
							hidden: true,
						},
						name: {
							helper:
								"Alphanumeric (+ dashes) name to give this RadioDJ. It must be unique from other RadioDJs configured.",
						},
						restURL: {
							helper:
								"REST server can be configured in RadioDJ -> Options -> Plugins -> Plugin REST Server.",
						},
						errorLevel: {
							helper:
								"Error level to trigger in the status system when this RadioDJ malfunctions: 1 = critical (also sends an emergency email), 2 = major, 3 = minor, 4 = info, 5 = none / good",
						},
					},
					form: {
						buttons: {
							submit: {
								title: `${data ? "Edit " : "Add "} RadioDJ`,
								click: (form, e) => {
									form.refreshValidationState(true);
									if (!form.isValid(true)) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										form.focus();
										return;
									}
									let value = form.getValue();

									this.change(
										data
											? this.endpoints.radiodjs.edit
											: this.endpoints.radiodjs.add,
										value,
										this.modal2.id,
										(body) => {
											if (body) this.modal2.iziModal("close");
										}
									);
								},
							},
						},
					},
				},
				data: data
					? {
							ID: data.ID,
							name: data.name,
							label: data.label,
							restURL: data.restURL,
							// No restPassword!
							errorLevel: data.errorLevel,
					  }
					: {},
			});
		};

		this.modal.iziModal("open");

		this.modal.title = `Configure RadioDJs`;
		this.modal.body = ``;

		$(this.modal.body).html(
			`<div class="callout callout-info">
			<h4>Disabling RadioDJ</h4>
			<p>If you configure no RadioDJs for the system, the system will enter "automation assist mode" which allows you to continue using DJ Controls with any automation system of your choosing.</p>
			<p>However, you will lose out on some functionality, such as remote controlling of the automation system, status monitoring of the automation system, breaks and underwritings management, and automatic triggering of genre rotations / playlists / prerecords.</p>
			<p>Furthermore, without RadioDJ, broadcast hosts will have to control both DJ Controls <strong>and</strong> control the automation system you are using.</p>
			<p>If you decide you want the system to integrate with your non-RadioDJ automation system and would like help, please contact Patrick Schmalstig at xanaftp@gmail.com .</p>
			<p>Your automation system should also regularly make URL calls to the station website PUT /api/meta/:metaSecret (or GET /api/meta/update/:metaSecret) [metaSecret is configured in DJ Controls -> System Settings -> Secrets] with these query / data parameters:</p>
			<ul>
				<li>track (string; Change the title of the track currently playing; send the value null when no longer playing anything in automation)</li>
				<li>artist (string; Change the artist of the track currently playing)</li>
				<li>album (string; Change the album of the track currently playing)</li>
				<li>label (string; Change the record label of the track currently playing)</li>
				<li>eventHosts (string; when starting a new event and you do not want to use the default hosts from the calendar system for the event, specify the nickname of the hosts as used in the Org Members database separated by a semicolon... maximum of 4)</li>
				<li>eventName (string; when starting a new event, specify the name of the event starting as used in the calendar system; system will switch to the relevant state according to the event type. NOTE: You should never start remote broadcasts this way. Instead, start them from DJ Controls.)
			</ul>
			</div>
			<p><button type="button" class="btn btn-block btn-success btn-settings-radiodjs-new">New RadioDJ Instance</button></p>
			<table id="section-settings-radiodjs-table" class="table table-striped display responsive" style="width: 100%;"></table>
			<h5>Actions Key:</h5>
			<div class="container-fluid">
				<div class="row">
					<div class="col">
						<span class="badge badge-warning"
							><i class="fas fa-edit"></i></span
						>Edit
					</div>
					<div class="col">
						<span class="badge badge-danger"
							><i class="fas fa-trash"></i></span
						>Delete
					</div>
				</div>
			</div>`
		);

		$(".btn-settings-radiodjs-new").unbind("click");
		$(".btn-settings-radiodjs-new").click((e) => {
			editRadioDJ();
		});

		this.manager
			.get("WWSUutil")
			.waitForElement(`#section-settings-radiodjs-table`, () => {
				this.table = $(`#section-settings-radiodjs-table`).DataTable({
					paging: true,
					data: this.radiodjs.map((radiodj) => [
						radiodj.name,
						radiodj.label,
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-settings-radiodjs-edit" data-id="${radiodj.ID}" title="Edit this RadioDJ instance."><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-settings-radiodjs-delete" data-id="${radiodj.ID}" title="Delete this RadioDJ instance."><i class="fas fa-trash"></i></button></div>`,
					]),
					columns: [
						{ title: "Name" },
						{ title: "Label" },
						{ title: "Actions" },
					],
					columnDefs: [{ responsivePriority: 1, targets: 2 }],
					order: [[0, "asc"]],
					pageLength: 100,
					drawCallback: () => {
						// Action button click events
						$(".btn-settings-radiodjs-edit").unbind("click");
						$(".btn-settings-radiodjs-delete").unbind("click");

						$(".btn-settings-radiodjs-edit").click((e) => {
							let radioDJ = this.radiodjs.find(
								(radiodj) =>
									radiodj.ID === parseInt($(e.currentTarget).data("id"))
							);
							if (!radioDJ) {
								throw new Error(
									"Internal error: tried to load radiodj configuration but it was not found."
								);
							}

							editRadioDJ(radioDJ);
						});
						$(".btn-settings-radiodjs-delete").click((e) => {
							let radioDJ = this.radiodjs.find(
								(radiodj) =>
									radiodj.ID === parseInt($(e.currentTarget).data("id"))
							);
							if (!radioDJ) {
								throw new Error(
									"Internal error: tried to load radiodj configuration but it was not found."
								);
							}

							this.manager
								.get("WWSUutil")
								.confirmDialog(
									`Are you sure you want to remove RadioDJ ${radioDJ.name}?`,
									radioDJ.name,
									() => {
										this.change(
											this.endpoints.radiodjs.remove,
											{ ID: radioDJ.ID },
											this.modal.id,
											(body) => {}
										);
									}
								);
						});
					},
				});
			});
	}

	/**
	 * Edit categories / groups used in the system.
	 */
	manageCategories() {
		// Error if we are not using RadioDJ.
		if (!this.usingRadioDJ) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "RadioDJs not being used",
				body: "The categories configuration is only used for RadioDJ, but RadioDJ is not configured. To configure, go to DJ Controls -> System Settings -> RadioDJs.",
				autohide: true,
				delay: 30000,
				icon: "fas fa-skull-crossbones",
			});
			return;
		}

		// Map of system category names as key to their description as value
		const systemCategories = {
			music:
				"All RadioDJ categories and subcategories containing music used in programming / automation. All tracks in these defined RadioDJ categories / subcategories can be requested in the track request system.",
			adds: "RadioDJ categories / subcategories containing recently-added music for high rotation; the Play Top Add button randomly plays a track from here.",
			IDs: "RadioDJ cats/subcats containing required FCC ID at the top of every hour that includes call sign, frequency, and market area. System checks to see if one airs between :55 and :05 past the hour and logs an error if not.",
			PSAs: "RadioDJ cats/subcats containing Public Service Announcements that promote a cause, organization, or social issue.",
			sweepers:
				"RadioDJ cats/subcats with fun, non-formal audio clips identifying the radio station.",
			underwritings:
				"RadioDJ cats/subcats containing sponsorships / promotional audio clips. Only tracks in these RadioDJ categories / subcategories can be selected in DJ Controls -> Underwritings.",
			liners:
				"RadioDJ cats/subcats with very short (< 7 seconds) audio clips identifying the station which are played between music tracks in automation.",
			requestLiners:
				"RadioDJ cats/subcats containing audio clips played just before track requests begin airing.",
			promos:
				"RadioDJ cats/subcats containing audio clips promoting on-air programming / broadcasts / events.",
			halftime:
				"RadioDJ cats/subcats containing upbeat energetic instrumentals played during extended breaks for sports broadcasts.",
			technicalIssues:
				"RadioDJ cats/subcats containing short audio clips played when the system was automatically sent to break due to an issue (usually used for remote broadcasts dropping out).",
			easNewAlertIntro:
				"RadioDJ cats/subcats containing intros to be played when the system is about to broadcast an Emergency Alert via the Internal EAS (NOT the FCC EAS).",
			easNewAlertStream:
				"RadioDJ cats/subcats containing an internet stream track pointing to /eas/speak-new which returns text-to-speech of new alerts in effect from the internal EAS (NOT the FCC EAS) (there should only be 1 track in these).",
			easAlertUpdateStream:
				"RadioDJ cats/subcats containing an internet stream track pointing to /eas/speak which returns text-to-speech audio of the active EAS alerts in effect from the Internal EAS (NOT the FCC EAS) (there should only be 1 track in these).",
			noClearGeneral:
				"When the system changes to a new playlist, prerecord, or genre, all tracks will be removed from the current queue EXCEPT tracks that are in these defined RadioDJ categories / subcategories.",
			noClearShow:
				"When someone starts a show/broadcast, all tracks in the queue will be removed EXCEPT tracks in these RadioDJ categories / subcategories.",
			clearBreak:
				"When a DJ or producer requests to resume a broadcast from a break, all tracks in these defined RadioDJ categories and subcategories will be removed from the queue",
			noMeta:
				"Whenever a track from any of these RadioDJ categories / subcategories play, the metadata will show configured alt(state) text from meta settings instead of the actual track info. NOTE: This also determines when the system determines when someone has gone on the air; the first track not existing in here is deemed when someone has started the broadcast.",
			noFade:
				"Every hour, the system will erase all fading from tracks in these RadioDJ categories / subcategories which should never have any crossfading (eg. we expect these tracks to usually be dictation / speech).",
		};

		// Update the function called on config changes so this table is updated when categories are updated.
		this.configUpdateCb = (system, db, query) => {
			if (system !== `config-categories`) return;
			if (this.table) {
				this.table.clear();
				this.categories.forEach((cat) => {
					this.table.row.add([
						cat.name,
						systemCategories[cat.name]
							? systemCategories[cat.name]
							: `Custom Category`,
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-settings-categories-edit" data-id="${
							cat.ID
						}" title="Edit this category."><i class="fas fa-edit"></i></button>${
							systemCategories[cat.name]
								? `<button class="btn btn-sm bg-orange btn-settings-categories-delete" data-id="${cat.ID}" title="Reset this system category to defaults."><i class="fas fa-undo"></i></button>`
								: `<button class="btn btn-sm btn-danger btn-settings-categories-delete" data-id="${cat.ID}" title="Delete this category."><i class="fas fa-trash"></i></button>`
						}</div>`,
					]);
				});
				this.table.draw(false);
			}
		};

		// Convert server categories object to Alpaca array
		const categoriesToAlpaca = (data) => {
			let returnData = [];

			for (let key in data) {
				if (!Object.prototype.hasOwnProperty.call(data, key)) continue;

				if (!data[key].length) {
					returnData.push(`${key} >> >ALL SUBCATEGORIES<`);
				} else {
					data[key].forEach((subcat) => {
						returnData.push(`${key} >> ${subcat}`);
					});
				}
			}

			console.dir(returnData);
			return returnData;
		};

		// Private function for category form
		const editCategory = (data) => {
			// Get available RadioDJ categories and subcategories before opening the form.
			this.manager
				.get("hostReq")
				.request(this.endpoints.categories.get, {}, {}, (body, resp) => {
					if (resp.statusCode >= 400 || typeof body !== "object") return;

					let choices = [];
					for (let key in body) {
						if (!Object.prototype.hasOwnProperty.call(body, key)) continue;

						choices.push(`${key} >> >ALL SUBCATEGORIES<`);
						body[key].forEach((subcat) => {
							choices.push(`${key} >> ${subcat}`);
						});
					}

					this.modal2.iziModal("open");
					this.modal2.title = `${data ? "Edit " : "Add "} Category`;
					this.modal2.body = ``;

					$(this.modal2.body).alpaca({
						schema: {
							title: `${data ? "Edit " : "Add "} Category`,
							type: "object",
							properties: {
								ID: {
									type: "number",
								},
								name: {
									type: "string",
									title: "Alphanumeric Name",
									required: true,
									readonly: data && systemCategories[data.name],
									pattern: /^[a-zA-Z0-9-_]+$/,
								},
								categories: {
									title: "RadioDJ Main Category >> Subcategory",
									type: "array",
									minItems: 1,
									required: true,
									enum: choices,
									items: {
										type: "string",
									},
								},
							},
						},
						options: {
							fields: {
								ID: {
									hidden: true,
								},
								name: {
									helper:
										"Alphanumeric (+ dashes) name to give this category. It must be unique from other categories configured.",
								},
								categories: {
									type: "select",
									multiple: true,
								},
							},
							form: {
								buttons: {
									submit: {
										title: `${data ? "Edit " : "Add "} Category`,
										click: (form, e) => {
											form.refreshValidationState(true);
											if (!form.isValid(true)) {
												if (this.manager.has("WWSUehhh"))
													this.manager.get("WWSUehhh").play();
												form.focus();
												return;
											}
											let value = form.getValue();

											// Map categories appropriately
											let categories = {};
											console.dir(value.categories);

											// Convert categories / subcategories object
											value.categories.forEach((category) => {
												let splitter = category.split(" >> ");
												if (splitter[1] !== `>ALL SUBCATEGORIES<`) {
													if (typeof categories[splitter[0]] === "undefined")
														categories[splitter[0]] = [];

													if (typeof categories[splitter[0]] !== "string") {
														categories[splitter[0]].push(splitter[1]);
													}
												} else {
													categories[splitter[0]] = "ALL";
												}
											});

											// Reset all subcategories to empty array
											for (let key in categories) {
												if (
													!Object.prototype.hasOwnProperty.call(
														categories,
														key
													) ||
													typeof categories[key] !== "string"
												)
													continue;
												categories[key] = [];
											}

											console.dir(categories);

											this.change(
												data
													? this.endpoints.categories.edit
													: this.endpoints.categories.add,
												{
													ID: data ? data.ID : undefined,
													name: value.name,
													categories: categories,
												},
												this.modal2.id,
												(body) => {
													if (body) this.modal2.iziModal("close");
												}
											);
										},
									},
								},
							},
						},
						data: data
							? {
									ID: data.ID,
									name: data.name,
									categories: categoriesToAlpaca(data.categories),
							  }
							: {},
					});
				});
		};

		this.modal.iziModal("open");

		this.modal.title = `Configure Categories`;
		this.modal.body = ``;

		$(this.modal.body).html(
			`<div class="callout callout-info">
			<h4>What are categories?</h4>
			<p>Categories (in DJ Controls) group together RadioDJ track categories and subcategories for various operations. They are most notably used to configure breaks but are also used elsewhere.</p>
			<p>See the table below for explanations of each system category. You can also add your own custom non-system categories to program in breaks.</p>
			</div>
			<p><button type="button" class="btn btn-block btn-success btn-settings-categories-new">New Custom Category</button></p>
			<table id="section-settings-categories-table" class="table table-striped display responsive" style="width: 100%;"></table>
			<h5>Actions Key:</h5>
			<div class="container-fluid">
				<div class="row">
					<div class="col">
						<span class="badge badge-warning"
							><i class="fas fa-edit"></i></span
						>Edit
					</div>
					<div class="col">
					<span class="badge bg-orange"
						><i class="fas fa-undo"></i></span
					>Reset to Default
				</div>
					<div class="col">
						<span class="badge badge-danger"
							><i class="fas fa-trash"></i></span
						>Delete
					</div>
				</div>
			</div>`
		);

		$(".btn-settings-categories-new").unbind("click");
		$(".btn-settings-categories-new").click((e) => {
			editCategory();
		});

		this.manager
			.get("WWSUutil")
			.waitForElement(`#section-settings-categories-table`, () => {
				this.table = $(`#section-settings-categories-table`).DataTable({
					paging: true,
					data: this.categories.map((cat) => [
						cat.name,
						systemCategories[cat.name]
							? systemCategories[cat.name]
							: `Custom Category`,
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-settings-categories-edit" data-id="${
							cat.ID
						}" title="Edit this category."><i class="fas fa-edit"></i></button>${
							systemCategories[cat.name]
								? `<button class="btn btn-sm bg-orange btn-settings-categories-delete" data-id="${cat.ID}" title="Reset this system category to defaults."><i class="fas fa-undo"></i></button>`
								: `<button class="btn btn-sm btn-danger btn-settings-categories-delete" data-id="${cat.ID}" title="Delete this category."><i class="fas fa-trash"></i></button>`
						}</div>`,
					]),
					columns: [
						{ title: "Name" },
						{ title: "Description" },
						{ title: "Actions" },
					],
					columnDefs: [{ responsivePriority: 1, targets: 2 }],
					order: [[0, "asc"]],
					pageLength: 100,
					drawCallback: () => {
						// Action button click events
						$(".btn-settings-categories-edit").unbind("click");
						$(".btn-settings-categories-delete").unbind("click");

						$(".btn-settings-categories-edit").click((e) => {
							let category = this.categories.find(
								(category) =>
									category.ID === parseInt($(e.currentTarget).data("id"))
							);
							if (!category) {
								throw new Error(
									"Internal error: tried to load category configuration but it was not found."
								);
							}

							editCategory(category);
						});
						$(".btn-settings-categories-delete").click((e) => {
							let category = this.categories.find(
								(category) =>
									category.ID === parseInt($(e.currentTarget).data("id"))
							);
							if (!category) {
								throw new Error(
									"Internal error: tried to load category configuration but it was not found."
								);
							}

							this.manager
								.get("WWSUutil")
								.confirmDialog(
									`Are you sure you want to ${
										systemCategories[category.name]
											? `reset to default value`
											: `remove`
									} the category ${category.name}?`,
									category.name,
									() => {
										this.change(
											this.endpoints.categories.remove,
											{ ID: category.ID },
											this.modal.id,
											(body) => {}
										);
									}
								);
						});
					},
				});
			});
	}

	/**
	 * Edit Song Liking System properties.
	 */
	songlikes() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Track Liking Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Change Track Liking Settings",
				type: "object",
				properties: {
					songsLikedCooldown: {
						title: "Track Like Cooldown (days)",
						type: "number",
						minimum: 0,
					},

					songsLikedPriorityChange: {
						title: "Track Priority Change",
						type: "number",
						minimum: -100,
						maximum: 100,
					},
				},
			},
			options: {
				fields: {
					songsLikedCooldown: {
						helper:
							"When someone likes a track on the website, the same track cannot be liked again by the same IP address for this many days. 0 means a track can only ever be liked one time per IP address.",
					},
					songsLikedPriorityChange: {
						helpers: [
							"When someone likes a track, its priority in RadioDJ will be adjusted by this amount (positive = increase, negative = decrease)",
							"NOTE: This setting is ignored when RadioDJ is not being used.",
						],
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Track Liking Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.songlikes.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				songsLikedCooldown: this.basic.songsLikedCooldown,
				songsLikedPriorityChange: this.basic.songsLikedPriorityChange,
			},
		});
	}

	/**
	 * Edit breaks configuration.
	 */
	manageBreaks() {
		// Error if we are not using RadioDJ.
		if (!this.usingRadioDJ) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "RadioDJs not being used",
				body: "The breaks configuration is only used for RadioDJ, but RadioDJ is not configured. To configure, go to DJ Controls -> System Settings -> RadioDJs. Or, if using another automation system, configure breaks in your automation system instead.",
				autohide: true,
				delay: 30000,
				icon: "fas fa-skull-crossbones",
			});
			return;
		}

		// Update the function called on config changes so this table is updated when radiodjs are updated.
		this.configUpdateCb = (system, db, query) => {
			if (system !== `config-breaks`) return;
			if (this.table) {
				this.table.clear();
				this.breaks.forEach((record) => {
					this.table.row.add([
						record.ID,
						record.type,
						record.subtype,
						record.order,
						taskToString(record),
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-settings-breaks-edit" data-id="${record.ID}" title="Edit this break task."><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-settings-breaks-delete" data-id="${record.ID}" title="Delete this break task."><i class="fas fa-trash"></i></button></div>`,
					]);
				});
				this.table.draw(false);
			}
		};

		/**
		 * Convert a break task to a string.
		 *
		 * @param {object} record The break task record.
		 * @returns {string} Description of task.
		 */
		const taskToString = (record) => {
			let task = `Unknown`;
			switch (record.task) {
				case "log":
					task = "Add a log entry";
					break;
				case "queueRequests":
					task = `Queue up to ${record.quantity} track requests`;
					break;
				case "queue":
					task = `Queue ${record.quantity} tracks from the ${record.category} category with ${record.rules}`;
					break;
				case "queueDuplicates":
					task = `Queue duplicate tracks that were removed`;
					break;
				case "queueUnderwritings":
					task = `Queue up to ${record.quantity} underwritings`;
					break;
				case "queueAlerts":
					task = `Announce active EAS Alerts`;
					break;
			}

			if (record.doWhen && record.doWhen.length) {
				task += `, but only if active event is of one of these types: ${record.doWhen.join(
					", "
				)}.`;
			}

			return task;
		};

		// Private function for break form
		const editBreak = (data) => {
			this.modal2.iziModal("open");
			this.modal2.title = `${data ? "Edit " : "Add "} Break Task`;
			this.modal2.body = ``;

			$(this.modal2.body).alpaca({
				schema: {
					title: `${data ? "Edit " : "Add "} Break Task`,
					type: "object",
					properties: {
						ID: {
							type: "number",
						},
						type: {
							type: "string",
							required: true,
							enum: ["clockwheel", "automation", "live", "remote", "sports"],
							title: "Break Type",
						},
						subtype: {
							type: "string",
							required: true,
							title: "Break Sub-Type",
						},
						order: {
							type: "number",
							required: true,
							title: "Order of Execution",
						},
						task: {
							type: "string",
							required: true,
							title: "The task to execute",
							enum: [
								"log",
								"queue",
								"queueDuplicates",
								"queueUnderwritings",
								"queueRequests",
								"queueAlerts",
							],
						},
						event: {
							type: "string",
							title: "Log text / event",
						},
						quantity: {
							type: "number",
							title: "Number of Tracks to Queue",
							minimum: 1,
						},
						category: {
							type: "string",
							title: "Category",
							enum: this.categories.map((cat) => cat.name),
						},
						rules: {
							type: "string",
							title: "RadioDJ Track Rotation Rules",
							enum: ["noRules", "lenientRules", "strictRules"],
						},
						doWhen: {
							title: "Limit to Event Types",
							type: "array",
							enum: [
								"show",
								"remote",
								"sports",
								"prerecord",
								"genre",
								"playlist",
								"default",
							],
							items: {
								type: "string"
							}
						},
					},
					dependencies: {
						subtype: ["type"],
						order: ["type"],
						task: ["type"],
						event: ["task"],
						category: ["task"],
						quantity: ["task"],
						rules: ["task"]
					},
				},
				options: {
					fields: {
						ID: {
							hidden: true,
						},
						type: {
							type: "select",
							helper:
								"Clockwheel = hourly breaks; automation / live / remote / sports = special break tasks for those types of broadcasts.",
							events: {
								change: function () {
									// Set subtype stuff depending on type selected
									let value = this.getValue();
									let subtype =
										this.getParent().childrenByPropertyId["subtype"];

									if (value === "clockwheel") {
										subtype.schema.enum = undefined;
										subtype.schema.type = "number";
										subtype.options.type = undefined;
										subtype.options.helper =
											"The approximate minute of the hour this break task will execute";
										subtype.schema.maximum = 59;
										subtype.schema.minimum = 0;
									} else if (value === "sports") {
										subtype.schema.enum = [
											"start",
											"before",
											"during",
											"duringHalftime",
											"after",
											"end",
										];
										subtype.schema.type = "string";
										subtype.options.type = "select";
										subtype.schema.maximum = undefined;
										subtype.schema.minimum = undefined;
										subtype.options.optionLabels = [
											"start (when broadcast starts)",
											"before (when break starts)",
											"during (repeatedly when queue runs low during break)",
											"duringHalftime (repeatedly when queue runs low during extended break)",
											"after (when break ends)",
											"end (when broadcast ends)",
										];
										subtype.options.helper = undefined;
									} else {
										subtype.schema.enum = [
											"start",
											"before",
											"during",
											"after",
											"end",
										];
										subtype.schema.type = "string";
										subtype.options.type = "select";
										subtype.schema.maximum = undefined;
										subtype.schema.minimum = undefined;
										subtype.options.optionLabels = [
											"start (when broadcast starts)",
											"before (when break starts)",
											"during (repeatedly when queue runs low during break)",
											"after (when break ends)",
											"end (when broadcast ends)",
										];
										subtype.options.helper = undefined;
									}
								},
							},
						},
						order: {
							helper:
								"Break tasks for a type/sub-type execute in order from lowest number to highest (same order number = lowest to highest ID)",
						},
						task: {
							type: "select",
							optionLabels: [
								"log (add an entry to the operation log)",
								"queue (queue tracks from a configured category)",
								"queueDuplicates (re-load tracks removed for being duplicates in queue)",
								"queueUnderwritings (Add scheduled underwritings)",
								"queueRequests (Queue requested tracks)",
								"queueAlerts (Queue from easAlertUpdateStream category if any Internal EAS alerts active)",
							],
						},
						event: {
							dependencies: {
								task: "log",
							},
						},
						quantity: {
							dependencies: {
								task: ["queue", "queueUnderwritings", "queueRequests"],
							},
						},
						category: {
							type: "select",
							dependencies: {
								task: "queue",
							},
							helper:
								"The category (DJ Controls -> System Settings -> Categories) from which to queue tracks.",
						},
						rules: {
							dependencies: {
								task: "queue",
							},
							optionLabels: [
								"Do not follow rules",
								"Follow rules if we can but always queue requested quantity",
								"Follow rules strictly even if that means we cannot queue requested quantity",
							],
						},
						doWhen: {
							type: "select",
							multiple: true,
							helper:
								"An event/broadcast of any of the selected types must be on the air for this task to execute (blank = any event).",
						},
					},
					form: {
						buttons: {
							submit: {
								title: `${data ? "Edit " : "Add "} Break Task`,
								click: (form, e) => {
									form.refreshValidationState(true);
									if (!form.isValid(true)) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										form.focus();
										return;
									}
									let value = form.getValue();
									console.dir(value);

									this.change(
										data
											? this.endpoints.breaks.edit
											: this.endpoints.breaks.add,
										value,
										this.modal2.id,
										(body) => {
											if (body) this.modal2.iziModal("close");
										}
									);
								},
							},
						},
					},
				},
				data: data,
			});
		};

		this.modal.iziModal("open");

		this.modal.title = `Configure Breaks`;
		this.modal.body = ``;

		$(this.modal.body).html(
			`<div class="callout callout-info">
			<h4>How Breaks Work</h4>
			<p>The table below lists active "break tasks". Every break task defines something to be done for a break (such as queuing a task) and the order which it is executed. A break can have multiple break tasks.</p>
			<p><strong>Types:</strong></p>
			<ul>
				<li><strong>clockwheel</strong>: Breaks that execute hourly (the subtype is the approximate minute of the hour which the break executes)</li>
				<li><strong>automation</strong>: Special tasks that occur during automation / genres / playlists.</li>
				<li><strong>live</strong>: Special tasks that occur during live in-studio broadcasts.</li>
				<li><strong>remote</strong>: Special tasks that occur during remote broadcasts.</li>
				<li><strong>sports</strong>: Special tasks that occur during sports broadcasts.</li>
			</ul>
			<p><strong>Sub-types</strong> (they depend on the main type):</p>
			<ul>
				<li><strong>(number)</strong>: For clockwheel break tasks, the approximate minute of the hour it will execute.</li>
				<li><strong>start</strong>: Break tasks that execute at the start of a broadcast.</li>
				<li><strong>before</strong>: Break tasks that execute at the start of a break.</li>
				<li><strong>during</strong>: Break tasks that repeatedly execute during the break when the queue runs low to keep it filled until the host ends the break (note... also includes automation type: breaks between broadcasts!).</li>
				<li><strong>duringHalftime</strong>: Same as during, but for extended breaks (during sports broadcasts only).</li>
				<li><strong>after</strong>: Break tasks that execute when a host ends the break / requests to resume the broadcast.</li>
				<li><strong>end</strong>: Break tasks that execute when the broadcast ends.</li>
			</ul>
			</div>

			<p><button type="button" class="btn btn-block btn-success btn-settings-breaks-new">New Break Task</button></p>
			<table id="section-settings-breaks-table" class="table table-striped display responsive" style="width: 100%;"></table>
			<h5>Actions Key:</h5>
			<div class="container-fluid">
				<div class="row">
					<div class="col">
						<span class="badge badge-warning"
							><i class="fas fa-edit"></i></span
						>Edit
					</div>
					<div class="col">
						<span class="badge badge-danger"
							><i class="fas fa-trash"></i></span
						>Delete
					</div>
				</div>
			</div>`
		);

		$(".btn-settings-breaks-new").unbind("click");
		$(".btn-settings-breaks-new").click((e) => {
			editBreak();
		});

		this.manager
			.get("WWSUutil")
			.waitForElement(`#section-settings-breaks-table`, () => {
				this.table = $(`#section-settings-breaks-table`).DataTable({
					paging: true,
					data: this.breaks.map((record) => [
						record.ID,
						record.type,
						record.subtype,
						record.order,
						taskToString(record),
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-settings-breaks-edit" data-id="${record.ID}" title="Edit this break task."><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-settings-breaks-delete" data-id="${record.ID}" title="Delete this break task."><i class="fas fa-trash"></i></button></div>`,
					]),
					columns: [
						{ title: "ID" },
						{ title: "Type" },
						{ title: "Subtype" },
						{ title: "Order" },
						{ title: "Task" },
						{ title: "Actions" },
					],
					columnDefs: [{ responsivePriority: 1, targets: 5 }],
					order: [
						[1, "asc"],
						[2, "asc"],
						[3, "asc"],
						[0, "asc"],
					],
					pageLength: 100,
					drawCallback: () => {
						// Action button click events
						$(".btn-settings-breaks-edit").unbind("click");
						$(".btn-settings-breaks-delete").unbind("click");

						$(".btn-settings-breaks-edit").click((e) => {
							let record = this.breaks.find(
								(rec) => rec.ID === parseInt($(e.currentTarget).data("id"))
							);
							if (!record) {
								throw new Error(
									"Internal error: tried to load break configuration but it was not found."
								);
							}

							editBreak(record);
						});
						$(".btn-settings-breaks-delete").click((e) => {
							let record = this.breaks.find(
								(rec) => rec.ID === parseInt($(e.currentTarget).data("id"))
							);
							if (!record) {
								throw new Error(
									"Internal error: tried to load break configuration but it was not found."
								);
							}

							this.manager.get("WWSUutil").confirmDialog(
								`Are you sure you want to remove that break task?<br /><br />
									
								${record.type} | ${record.subtype} | ${taskToString(record)}`,
								record.ID,
								() => {
									this.change(
										this.endpoints.breaks.remove,
										{ ID: record.ID },
										this.modal.id,
										(body) => {}
									);
								}
							);
						});
					},
				});
			});
	}

	/**
	 * Configure max allowed queues.
	 */
	maxQueues() {
		// Error if we are not using RadioDJ.
		if (!this.usingRadioDJ) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "RadioDJs not being used",
				body: "Max queue / queue length correction is only available for RadioDJ, but no RadioDJs are configured. To configure, go to DJ Controls -> System Settings -> RadioDJs. Otherwise, you should manually adjust the queue in your automation system as necessary.",
				autohide: true,
				delay: 30000,
				icon: "fas fa-skull-crossbones",
			});
			return;
		}

		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Max Queue Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Change Max Queue Settings",
				type: "object",
				properties: {
					maxQueueLive: {
						type: "number",
						title: "Max Queue for Live Shows (seconds)",
						minimum: 0,
					},

					maxQueuePrerecord: {
						type: "number",
						title: "Max Queue for Prerecords (seconds)",
						minimum: 0,
					},

					maxQueueSports: {
						type: "number",
						title: "Max Queue for Sports Broadcasts (seconds)",
						minimum: 0,
					},

					maxQueueSportsReturn: {
						type: "number",
						title: "Max Queue for Returning from Break During Sports (seconds)",
						minimum: 0,
					},

					maxQueueRemote: {
						type: "number",
						title: "Max Queue for Remote Broadcasts (seconds)",
						minimum: 0,
					},
				},
			},
			options: {
				fields: {
					maxQueueLive: {
						helper:
							"When starting a live show, If the RadioDJ queue exceeds this length in seconds, the system will trim it and skip the currently playing track to get hosts on the air sooner.",
					},

					maxQueuePrerecord: {
						helper:
							"When starting a prerecord, If the RadioDJ queue exceeds this length in seconds (before the prerecord playlist starts), the system will trim it and skip the currently playing track to get the prerecord on the air sooner.",
					},

					maxQueueSports: {
						helper:
							"When starting a sports broadcast, If the RadioDJ queue exceeds this length in seconds, the system will trim it and skip the currently playing track to get hosts on the air sooner. This should generally be a low value as sports are timely.",
					},

					maxQueueSportsReturn: {
						helper:
							"When coming back from a break during a sports broadcast, If the RadioDJ queue exceeds this length in seconds, the system will trim it and skip the currently playing track to get hosts on the air sooner. This should generally be a low value as sports are timely.",
					},

					maxQueueRemote: {
						helper:
							"When starting a remote broadcast, If the RadioDJ queue exceeds this length in seconds, the system will trim it and skip the currently playing track to get hosts on the air sooner.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Max Queue Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.maxQueue.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				maxQueueLive: this.basic.maxQueueLive,
				maxQueuePrerecord: this.basic.maxQueuePrerecord,
				maxQueueSports: this.basic.maxQueueSports,
				maxQueueSportsReturn: this.basic.maxQueueSportsReturn,
				maxQueueRemote: this.basic.maxQueueRemote,
			},
		});
	}

	/**
	 * Define words to filter.
	 */
	filterProfanity() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Profanity Filter Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Words to Filter",
				type: "array",
				items: {
					word: {
						type: "string",
						required: true,
					},
				},
			},
			options: {
				form: {
					buttons: {
						submit: {
							title: `Change Profanity Filter Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.filterProfanity.edit,
									{ filterProfanity: value },
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: this.basic.filterProfanity,
		});
	}

	/**
	 * Edit sanitize configuration
	 */
	sanitize() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Sanitize Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Sanitize Settings",
				type: "object",
				properties: {
					sanitize: {
						type: "string",
						required: true,
						title: "Settings",
					},
				},
			},
			options: {
				fields: {
					sanitize: {
						type: "json",
						helper:
							"See https://github.com/apostrophecms/sanitize-html (Node) for a list of available settings.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Sanitize Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.sanitize.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				sanitize: this.basic.sanitize,
			},
		});
	}

	/**
	 * Manage Discord settings.
	 */
	manageDiscord() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Discord Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Discord Settings",
				type: "object",
				properties: {
					token: {
						type: "string",
						title: "Change Discord Bot Token",
						format: "password",
					},

					clientOptions: {
						type: "string",
						title: "Discord.js Client Options",
					},

					guildWWSU: {
						type: "string",
						title: "Snowflake ID of WWSU Server",
					},

					guildWNF: {
						type: "string",
						title: "Snowflake ID of The Wright News Feeds Server",
					},

					channelLive: {
						type: "string",
						title:
							"Snowflake ID of the text channel to post broadcasts that started",
					},

					channelScheduleChanges: {
						type: "string",
						title: "Snowflake ID of the text channel to post schedule changes",
					},

					channelRadio: {
						type: "string",
						title:
							"Snowflake ID of the voice channel where the bot should stream WWSU Radio",
					},

					channelSports: {
						type: "string",
						title: "Snowflake ID of the text channel for sports discussion",
					},

					channelGeneral: {
						type: "string",
						title: "Snowflake ID of the text channel for general discussion",
					},

					channelBlog: {
						type: "string",
						title: "Snowflake ID of the text channel to post blogs",
					},

					channelAnalytics: {
						type: "string",
						title: "Snowflake ID of the text channel to post weekly analytics",
					},

					messageAnalytics: {
						type: "string",
						title: "Snowflake ID of the weekly analytics message to edit",
					},

					channelWNF: {
						type: "string",
						title:
							"Snowflake ID of the WWSU text channel in The Wright News Feeds server",
					},

					categoryShowDefault: {
						type: "string",
						title: "Snowflake ID of the Category for Unsorted / New shows",
					},

					categoryGeneral: {
						type: "string",
						title:
							"Snowflake ID of the category for general discussion channels",
					},
				},
			},
			options: {
				fields: {
					token: {
						helpers: [
							"Change the Discord Bot Token in the system by specifying it here.",
							"<strong>You must reboot the sailsJS app after changing this value</strong> under DJ Controls -> Maintenance -> Reboot SailsJS App",
						],
					},
					clientOptions: {
						type: "json",
						helpers: [
							"Clear / set to blank to disable the Discord bot entirely. See https://discord.js.org/ and look up Client options for more information.",
							"<strong>You must reboot the sailsJS app after changing this value</strong> under DJ Controls -> Maintenance -> Reboot SailsJS App",
						],
					},
					guildWWSU: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the WWSU Server and click Copy ID to get its snowflake. Paste here.",
					},
					guildWNF: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the Wright News Feeds Server and click Copy ID to get its snowflake. Paste here.",
					},
					channelLive: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the text channel for posting broadcasts that start and click Copy ID to get its snowflake. Paste here.",
					},

					channelScheduleChanges: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the text channel for posting schedule changes and click Copy ID to get its snowflake. Paste here.",
					},

					channelRadio: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the voice channel where the bot should stream WWSU Radio and click Copy ID to get its snowflake. Paste here.",
					},

					channelSports: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the text channel where people discuss sports and click Copy ID to get its snowflake. Paste here.",
					},

					channelGeneral: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the text channel for general discussion and click Copy ID to get its snowflake. Paste here.",
					},

					channelBlog: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the text channel where published blogs should be posted and click Copy ID to get its snowflake. Paste here.",
					},

					channelAnalytics: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the text channel where weekly analytics should be posted and click Copy ID to get its snowflake. Paste here.",
					},

					messageAnalytics: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the message posted by the bot in the weekly analytics text channel that should be edited with current analytics... and click Copy ID to get its snowflake. Paste here. Set this value to 0 to trigger the bot to create a new message and auto-update this setting.",
					},

					channelWNF: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the text channel in The Wright News Feeds server for WWSU postings and click Copy ID to get its snowflake. Paste here.",
					},

					categoryShowDefault: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the channel category where unsorted radio show text channels should go, and click Copy ID to get its snowflake. Paste here.",
					},

					categoryGeneral: {
						helper:
							"In Discord settings / Advanced, activate Developer Mode. Then, right-click the channel category where channels for general discussion go, and click Copy ID to get its snowflake. Paste here.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Discord Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.discord.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				clientOptions: this.discord.clientOptions,
				guildWWSU: this.discord.guildWWSU,
				guildWNF: this.discord.guildWNF,
				channelLive: this.discord.channelLive,
				channelScheduleChanges: this.discord.channelScheduleChanges,
				channelRadio: this.discord.channelRadio,
				channelSports: this.discord.channelSports,
				channelGeneral: this.discord.channelGeneral,
				channelBlog: this.discord.channelBlog,
				channelAnalytics: this.discord.channelAnalytics,
				messageAnalytics: this.discord.messageAnalytics,
				channelWNF: this.discord.channelWNF,
				categoryShowDefault: this.discord.categoryShowDefault,
				categoryGeneral: this.discord.categoryGeneral,
			},
		});
	}

	/**
	 * Manage station websites and social media.
	 */
	websites() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Websites`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Websites",
				type: "object",
				properties: {
					website: {
						title: "Station Website URL",
						type: "string",
					},

					recordings: {
						title: "Website URL to Download Recordings",
						type: "string",
					},

					facebook: {
						title: "Facebook Page URL",
						type: "string",
					},

					twitter: {
						title: "Twitter Page URL",
						type: "string",
					},

					youtube: {
						title: "YouTube Channel URL",
						type: "string",
					},

					instagram: {
						title: "Instagram Page URL",
						type: "string",
					},

					discord: {
						title: "Permanent Discord Server Invite Link",
						type: "string",
					},
				},
			},
			options: {
				fields: {
					website: {
						helper:
							"URL to the station website; the status system will regularly check to make sure it is working.",
						type: "url",
					},

					recordings: {
						helper:
							"URL to the website where broadcast recordings can be downloaded (such as a publicly shared OneDrive link). Requests to (station base url)/recordings will be redirected to this site.",
						type: "url",
					},

					facebook: {
						type: "url",
					},

					twitter: {
						type: "url",
					},

					youtube: {
						type: "url",
					},

					instagram: {
						type: "url",
					},

					discord: {
						helper:
							"This should be an invite link to the WWSU Discord Server that does not expire.",
						type: "url",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Websites`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.websites.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				website: this.basic.website,
				recordings: this.basic.recordings,
				facebook: this.basic.facebook,
				twitter: this.basic.twitter,
				youtube: this.basic.youtube,
				instagram: this.basic.instagram,
				discord: this.basic.discord,
			},
		});
	}

	/**
	 * Change settings for the shoutcast server.
	 */
	shoutcast() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Shoutcast Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Shoutcast Settings",
				type: "object",
				properties: {
					shoutcastStream: {
						type: "string",
						title: "Shoutcast Server URL",
					},
					shoutcastStreamID: {
						type: "number",
						title: "Shoutcast Stream ID",
					},
				},
			},
			options: {
				fields: {
					shoutcastStream: {
						helper:
							"The full URL path and relevant port number to the Shoutcast v2 server. Set to blank to disable Shoutcast checking (will also disable listener analytics tracking).",
					},
					shoutcastStreamID: {
						helper:
							"The ID number of the stream on this server which is WWSU's.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Shoutcast Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.shoutcast.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				shoutcastStream: this.basic.shoutcastStream,
				shoutcastStreamID: this.basic.shoutcastStreamID,
			},
		});
	}

	/**
	 * Edit settings for the Owncast video stream server.
	 */
	owncast() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Owncast Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Owncast Settings",
				type: "object",
				properties: {
					owncastStream: {
						title: "Owncast server URL",
						type: "string",
					},
				},
			},
			options: {
				fields: {
					owncastStream: {
						helper:
							"The full URL path and relevant port number to the Owncast server. Set to blank to disable Owncast (will also disable viewer analytics tracking).",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Owncast Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.owncast.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				owncastStream: this.basic.owncastStream,
			},
		});
	}

	/**
	 * Edit status settings.
	 */
	manageStatus() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Status Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Status Settings",
				type: "object",
				properties: {
					musicLibraryVerify1: {
						type: "number",
						minimum: 1,
						title: "Critical Status Threshold - Bad Tracks",
					},

					musicLibraryVerify2: {
						type: "number",
						minimum: 1,
						title: "Major Status Threshold - Bad Tracks",
					},

					musicLibraryVerify3: {
						type: "number",
						minimum: 1,
						title: "Minor Status Threshold - Bad Tracks",
					},

					musicLibraryLong1: {
						type: "number",
						minimum: 1,
						title: "Critical Status Threshold - Tracks Too Long",
					},

					musicLibraryLong2: {
						type: "number",
						minimum: 1,
						title: "Major Status Threshold - Tracks Too Long",
					},

					musicLibraryLong3: {
						type: "number",
						minimum: 1,
						title: "Minor Status Threshold - Tracks Too Long",
					},

					server1MinLoad1: {
						type: "number",
						minimum: 0,
						title: "Critical Status Threshold - 1-minute CPU Load",
					},

					server1MinLoad2: {
						type: "number",
						minimum: 0,
						title: "Major Status Threshold - 1-minute CPU Load",
					},

					server1MinLoad3: {
						type: "number",
						minimum: 0,
						title: "Minor Status Threshold - 1-minute CPU Load",
					},

					server5MinLoad1: {
						type: "number",
						minimum: 0,
						title: "Critical Status Threshold - 5-minute CPU Load",
					},

					server5MinLoad2: {
						type: "number",
						minimum: 0,
						title: "Major Status Threshold - 5-minute CPU Load",
					},

					server5MinLoad3: {
						type: "number",
						minimum: 0,
						title: "Minor Status Threshold - 5-minute CPU Load",
					},

					server15MinLoad1: {
						type: "number",
						minimum: 0,
						title: "Critical Status Threshold - 15-minute CPU Load",
					},

					server15MinLoad2: {
						type: "number",
						minimum: 0,
						title: "Major Status Threshold - 15-minute CPU Load",
					},

					server15MinLoad3: {
						type: "number",
						minimum: 0,
						title: "Minor Status Threshold - 15-minute CPU Load",
					},

					serverMemory1: {
						type: "number",
						minimum: 0,
						title: "Critical Status Threshold - Free RAM (bytes)",
					},

					serverMemory2: {
						type: "number",
						minimum: 0,
						title: "Major Status Threshold - Free RAM (bytes)",
					},

					serverMemory3: {
						type: "number",
						minimum: 0,
						title: "Minor Status Threshold - Free RAM (bytes)",
					},
				},
			},
			options: {
				fields: {
					musicLibraryVerify1: {
						helpers: [
							"Trigger critical status when the number of bad tracks exceeds this value.",
							"Setting is ignored when not using RadioDJ / no RadioDJs configured.",
						],
					},

					musicLibraryVerify2: {
						helpers: [
							"Trigger major status when the number of bad tracks exceeds this value.",
							"Setting is ignored when not using RadioDJ / no RadioDJs configured.",
						],
					},

					musicLibraryVerify3: {
						helpers: [
							"Trigger minor status when the number of bad tracks exceeds this value.",
							"Setting is ignored when not using RadioDJ / no RadioDJs configured.",
						],
					},

					musicLibraryLong1: {
						helpers: [
							"Trigger critical status when the number of tracks too long exceeds this value.",
							"Setting is ignored when not using RadioDJ / no RadioDJs configured.",
						],
					},

					musicLibraryLong2: {
						helpers: [
							"Trigger major status when the number of tracks too long exceeds this value.",
							"Setting is ignored when not using RadioDJ / no RadioDJs configured.",
						],
					},

					musicLibraryLong3: {
						helpers: [
							"Trigger minor status when the number of tracks too long exceeds this value.",
							"Setting is ignored when not using RadioDJ / no RadioDJs configured.",
						],
					},

					server1MinLoad1: {
						helpers: [
							"Trigger critical status when the 1-minute CPU load exceeds this value.",
							"Suggested setting: Number of CPU cores * 8",
						],
					},

					server1MinLoad2: {
						helpers: [
							"Trigger major status when the 1-minute CPU load exceeds this value.",
							"Suggested setting: Number of CPU cores * 4",
						],
					},

					server1MinLoad3: {
						helpers: [
							"Trigger minor status when the 1-minute CPU load exceeds this value.",
							"Suggested setting: Number of CPU cores * 2",
						],
					},

					server5MinLoad1: {
						helpers: [
							"Trigger critical status when the 5-minute CPU load exceeds this value.",
							"Suggested setting: Number of CPU cores * 6",
						],
					},

					server5MinLoad2: {
						helpers: [
							"Trigger major status when the 5-minute CPU load exceeds this value.",
							"Suggested setting: Number of CPU cores * 3",
						],
					},

					server5MinLoad3: {
						helpers: [
							"Trigger minor status when the 5-minute CPU load exceeds this value.",
							"Suggested setting: Number of CPU cores * 1.5",
						],
					},

					server15MinLoad1: {
						helpers: [
							"Trigger critical status when the 15-minute CPU load exceeds this value.",
							"Suggested setting: Number of CPU cores * 4",
						],
					},

					server15MinLoad2: {
						helpers: [
							"Trigger major status when the 15-minute CPU load exceeds this value.",
							"Suggested setting: Number of CPU cores * 2",
						],
					},

					server15MinLoad3: {
						helpers: [
							"Trigger minor status when the 15-minute CPU load exceeds this value.",
							"Suggested setting: Number of CPU cores * 1",
						],
					},

					serverMemory1: {
						helpers: [
							"Trigger critical status when the amount of free RAM available drops below this value in bytes.",
							"Suggested setting: 5% of total RAM capacity",
						],
					},

					serverMemory2: {
						helpers: [
							"Trigger major status when the amount of free RAM available drops below this value in bytes.",
							"Suggested setting: 10% of total RAM capacity",
						],
					},

					serverMemory3: {
						helpers: [
							"Trigger minor status when the amount of free RAM available drops below this value in bytes.",
							"Suggested setting: 25% of total RAM capacity",
						],
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Status Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.status.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			view: {
				wizard: {
					bindings: {
						musicLibraryVerify1: 1,
						musicLibraryVerify2: 1,
						musicLibraryVerify3: 1,
						musicLibraryLong1: 1,
						musicLibraryLong2: 1,
						musicLibraryLong3: 1,
						server1MinLoad1: 2,
						server1MinLoad2: 2,
						server1MinLoad3: 2,
						server5MinLoad1: 2,
						server5MinLoad2: 2,
						server5MinLoad3: 2,
						server15MinLoad1: 2,
						server15MinLoad2: 2,
						server15MinLoad3: 2,
						serverMemory1: 3,
						serverMemory2: 3,
						serverMemory3: 3,
					},
					steps: [
						{
							title: "Music Library",
						},
						{
							title: "Server CPU",
						},
						{
							title: "Server Memory",
						},
					],
				},
			},
			data: {
				musicLibraryVerify1: this.status.musicLibraryVerify1,
				musicLibraryVerify2: this.status.musicLibraryVerify2,
				musicLibraryVerify3: this.status.musicLibraryVerify3,
				musicLibraryLong1: this.status.musicLibraryLong1,
				musicLibraryLong2: this.status.musicLibraryLong2,
				musicLibraryLong3: this.status.musicLibraryLong3,
				server1MinLoad1: this.status.server1MinLoad1,
				server1MinLoad2: this.status.server1MinLoad2,
				server1MinLoad3: this.status.server1MinLoad3,
				server5MinLoad1: this.status.server5MinLoad1,
				server5MinLoad2: this.status.server5MinLoad2,
				server5MinLoad3: this.status.server5MinLoad3,
				server15MinLoad1: this.status.server15MinLoad1,
				server15MinLoad2: this.status.server15MinLoad2,
				server15MinLoad3: this.status.server15MinLoad3,
				serverMemory1: this.status.serverMemory1,
				serverMemory2: this.status.serverMemory2,
				serverMemory3: this.status.serverMemory3,
			},
		});
	}

	/**
	 * Manage National Weather Service CAPS feeds for internal EAS.
	 */
	manageNWS() {
		// Update the function called on config changes so this table is updated when radiodjs are updated.
		this.configUpdateCb = (system, db, query) => {
			if (system !== `config-nws`) return;
			if (this.table) {
				this.table.clear();
				this.nws.forEach((source) => {
					this.table.row.add([
						source.code,
						source.name,
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-settings-nws-edit" data-id="${source.ID}" title="Edit this NWS Source."><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-settings-nws-delete" data-id="${source.ID}" title="Delete this NWS Source."><i class="fas fa-trash"></i></button></div>`,
					]);
				});
				this.table.draw(false);
			}
		};

		// Private function for NWS form
		const editNWS = (data) => {
			this.modal2.iziModal("open");
			this.modal2.title = `${data ? "Edit " : "Add "} NWS Source`;
			this.modal2.body = ``;

			$(this.modal2.body).alpaca({
				schema: {
					title: `${data ? "Edit " : "Add "} NWS Source`,
					type: "object",
					properties: {
						ID: {
							type: "number",
						},
						code: {
							type: "string",
							title: "County or Zone code",
							required: true,
						},
						name: {
							type: "string",
							title: "Human Friendly Label",
							required: true,
						},
					},
				},
				options: {
					fields: {
						ID: {
							hidden: true,
						},
						code: {
							helper:
								"Pull from https://alerts.weather.gov/ , click either county list or zone list next to your state, find the appropriate entry, and put in the county or zone code here.",
						},
						name: {
							helper:
								"This should be a descriptor of the location (such as the county name) that people can easily understand.",
						},
					},
					form: {
						buttons: {
							submit: {
								title: `${data ? "Edit " : "Add "} NWS Source`,
								click: (form, e) => {
									form.refreshValidationState(true);
									if (!form.isValid(true)) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										form.focus();
										return;
									}
									let value = form.getValue();

									this.change(
										data ? this.endpoints.nws.edit : this.endpoints.nws.add,
										value,
										this.modal2.id,
										(body) => {
											if (body) this.modal2.iziModal("close");
										}
									);
								},
							},
						},
					},
				},
				data: data
					? {
							ID: data.ID,
							code: data.code,
							name: data.name,
					  }
					: {},
			});
		};

		this.modal.iziModal("open");

		this.modal.title = `Configure NWS Sources`;
		this.modal.body = ``;

		$(this.modal.body).html(
			`<p><button type="button" class="btn btn-block btn-success btn-settings-nws-new">New NWS Source</button></p>
			<table id="section-settings-nws-table" class="table table-striped display responsive" style="width: 100%;"></table>
			<h5>Actions Key:</h5>
			<div class="container-fluid">
				<div class="row">
					<div class="col">
						<span class="badge badge-warning"
							><i class="fas fa-edit"></i></span
						>Edit
					</div>
					<div class="col">
						<span class="badge badge-danger"
							><i class="fas fa-trash"></i></span
						>Delete
					</div>
				</div>
			</div>`
		);

		$(".btn-settings-nws-new").unbind("click");
		$(".btn-settings-nws-new").click((e) => {
			editNWS();
		});

		this.manager
			.get("WWSUutil")
			.waitForElement(`#section-settings-nws-table`, () => {
				this.table = $(`#section-settings-nws-table`).DataTable({
					paging: true,
					data: this.nws.map((source) => [
						source.code,
						source.name,
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-settings-nws-edit" data-id="${source.ID}" title="Edit this NWS Source."><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-settings-nws-delete" data-id="${source.ID}" title="Delete this NWS Source."><i class="fas fa-trash"></i></button></div>`,
					]),
					columns: [{ title: "Code" }, { title: "Name" }, { title: "Actions" }],
					columnDefs: [{ responsivePriority: 1, targets: 2 }],
					order: [[0, "asc"]],
					pageLength: 100,
					drawCallback: () => {
						// Action button click events
						$(".btn-settings-nws-edit").unbind("click");
						$(".btn-settings-nws-delete").unbind("click");

						$(".btn-settings-nws-edit").click((e) => {
							let source = this.nws.find(
								(source) =>
									source.ID === parseInt($(e.currentTarget).data("id"))
							);
							if (!source) {
								throw new Error(
									"Internal error: tried to load NWS configuration but it was not found."
								);
							}

							editNWS(source);
						});
						$(".btn-settings-nws-delete").click((e) => {
							let source = this.nws.find(
								(source) =>
									source.ID === parseInt($(e.currentTarget).data("id"))
							);
							if (!source) {
								throw new Error(
									"Internal error: tried to load NWS configuration but it was not found."
								);
							}

							this.manager
								.get("WWSUutil")
								.confirmDialog(
									`Are you sure you want to remove NWS Source ${source.code} / ${source.name}?`,
									source.code,
									() => {
										this.change(
											this.endpoints.nws.remove,
											{ ID: source.ID },
											this.modal.id,
											(body) => {}
										);
									}
								);
						});
					},
				});
			});
	}

	/**
	 * Edit the sports used in the system.
	 */
	manageSports() {
		// Update the function called on config changes so this table is updated when radiodjs are updated.
		this.configUpdateCb = (system, db, query) => {
			if (system !== `config-sports`) return;
			if (this.table) {
				this.table.clear();
				this.sports.forEach((sport) => {
					this.table.row.add([
						sport.name,
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-settings-sports-edit" data-id="${sport.ID}" title="Edit this sport."><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-settings-sports-delete" data-id="${sport.ID}" title="Delete this sport."><i class="fas fa-trash"></i></button></div>`,
					]);
				});
				this.table.draw(false);
			}
		};

		// Private function for sport form
		const editSport = (data) => {
			this.modal2.iziModal("open");
			this.modal2.title = `${data ? "Edit " : "Add "} Sport`;
			this.modal2.body = ``;

			$(this.modal2.body).alpaca({
				schema: {
					title: `${data ? "Edit " : "Add "} Sport`,
					type: "object",
					properties: {
						ID: {
							type: "number",
						},
						name: {
							type: "string",
							title: "Sport Name",
							required: true,
						},
					},
				},
				options: {
					fields: {
						ID: {
							hidden: true,
						},
						name: {
							helper:
								"Name of the sport (such as Men's Basketball). If using RadioDJ, this should also exist as a subcategory in the RadioDJ categories of Sports Openers, Sports Liners, Sports Returns, and Sports Closers.",
						},
					},
					form: {
						buttons: {
							submit: {
								title: `${data ? "Edit " : "Add "} Sport`,
								click: (form, e) => {
									form.refreshValidationState(true);
									if (!form.isValid(true)) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										form.focus();
										return;
									}
									let value = form.getValue();

									this.change(
										data
											? this.endpoints.sports.edit
											: this.endpoints.sports.add,
										value,
										this.modal2.id,
										(body) => {
											if (body) this.modal2.iziModal("close");
										}
									);
								},
							},
						},
					},
				},
				data: data
					? {
							ID: data.ID,
							name: data.name,
					  }
					: {},
			});
		};

		this.modal.iziModal("open");

		this.modal.title = `Configure Sports`;
		this.modal.body = ``;

		$(this.modal.body).html(
			`<p><button type="button" class="btn btn-block btn-success btn-settings-sports-new">New Sport</button></p>
			<table id="section-settings-sports-table" class="table table-striped display responsive" style="width: 100%;"></table>
			<h5>Actions Key:</h5>
			<div class="container-fluid">
				<div class="row">
					<div class="col">
						<span class="badge badge-warning"
							><i class="fas fa-edit"></i></span
						>Edit
					</div>
					<div class="col">
						<span class="badge badge-danger"
							><i class="fas fa-trash"></i></span
						>Delete
					</div>
				</div>
			</div>`
		);

		$(".btn-settings-sports-new").unbind("click");
		$(".btn-settings-sports-new").click((e) => {
			editSport();
		});

		this.manager
			.get("WWSUutil")
			.waitForElement(`#section-settings-sports-table`, () => {
				this.table = $(`#section-settings-sports-table`).DataTable({
					paging: true,
					data: this.sports.map((sport) => [
						sport.name,
						`<div class="btn-group"><button class="btn btn-sm btn-warning btn-settings-sports-edit" data-id="${sport.ID}" title="Edit this sport."><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-settings-sports-delete" data-id="${sport.ID}" title="Delete this sport."><i class="fas fa-trash"></i></button></div>`,
					]),
					columns: [{ title: "Name" }, { title: "Actions" }],
					columnDefs: [{ responsivePriority: 1, targets: 1 }],
					order: [[0, "asc"]],
					pageLength: 100,
					drawCallback: () => {
						// Action button click events
						$(".btn-settings-sports-edit").unbind("click");
						$(".btn-settings-sports-delete").unbind("click");

						$(".btn-settings-sports-edit").click((e) => {
							let sport = this.sports.find(
								(sport) => sport.ID === parseInt($(e.currentTarget).data("id"))
							);
							if (!sport) {
								throw new Error(
									"Internal error: tried to load sport configuration but it was not found."
								);
							}

							editSport(sport);
						});
						$(".btn-settings-sports-delete").click((e) => {
							let sport = this.sports.find(
								(sport) => sport.ID === parseInt($(e.currentTarget).data("id"))
							);
							if (!sport) {
								throw new Error(
									"Internal error: tried to load sport configuration but it was not found."
								);
							}

							this.manager.get("WWSUutil").confirmDialog(
								`Are you sure you want to remove the sport?<br /><br />
									<strong>This will also mark inactive the sport calendar event and remove all schedules!</strong>`,
								sport.name,
								() => {
									this.change(
										this.endpoints.sports.remove,
										{ ID: sport.ID },
										this.modal.id,
										(body) => {}
									);
								}
							);
						});
					},
				});
			});
	}

	/**
	 * Manage timings for breaks
	 */
	breakTimes() {
		// Error if we are not using RadioDJ.
		if (!this.usingRadioDJ) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "RadioDJs not being used",
				body: "Break times configuration is only available for RadioDJ, but no RadioDJs are configured. To configure, go to DJ Controls -> System Settings -> RadioDJs. Otherwise, you should manually program breaks and timings in your automation system.",
				autohide: true,
				delay: 30000,
				icon: "fas fa-skull-crossbones",
			});
			return;
		}

		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Break Time Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Break Time Settings",
				type: "object",
				properties: {
					breakCheck: {
						type: "number",
						minimum: 3,
						maximum: 50,
						title: "Break Check (minutes)",
					},

					linerTime: {
						type: "number",
						minimum: 1,
						maximum: 59,
						title: "Liner Interval (minutes)",
					},
				},
			},
			options: {
				fields: {
					breakCheck: {
						helpers: [
							"If a clockwheel break (except minute 0 for top of hour) aired less than this many minutes ago, other clockwheel breaks will be delayed.",
							"Also, this defines the longest allowed duration for a music track (RadioDJ); music tracks longer than this many minutes will be flagged by the status system.",
							"Ideally, this value should not be longer than the smallest time distance between two configured clockwheel breaks.",
						],
					},

					linerTime: {
						helper:
							"Liners during automation will not play any more often than once every defined number of minutes. They will also not play if a break executed less than this many minutes ago.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Break Time Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.breakTimes.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				breakCheck: this.basic.breakCheck,
				linerTime: this.basic.linerTime,
			},
		});
	}

	/**
	 * Adjust tomorrowio weather settings
	 */
	tomorrowio() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Tomorrow.io Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Tomorrow.io Weather Settings",
				type: "object",
				properties: {
					tomorrowioAPI: {
						title: "Change Tomorrow.io API Key",
						type: "string",
						format: "password",
					},

					tomorrowioLocation: {
						title: "Tomorrow.io location parameter",
						type: "string",
					},
				},
			},
			options: {
				fields: {
					tomorrowioAPI: {
						helper:
							"Specify a new API key if changing the one in the system. You can get an API key from their website at https://tomorrow.io (Weather API).",
					},

					tomorrowioLocation: {
						helpers: [
							"You can use a lat,long string or a locationid. See https://docs.tomorrow.io/reference/api-formats#locations.",
							"If you set this empty, then tomorrow.io / weather functionality will be disabled.",
						],
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Tomorrow.io Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.tomorrowio.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				tomorrowioLocation: this.basic.tomorrowioLocation,
			},
		});
	}

	/**
	 * Adjust campus alerts feed settings.
	 */
	campusAlerts() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Campus Alerts Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Campus Alerts Settings",
				type: "object",
				properties: {
					campusAlerts: {
						type: "string",
						title: "Campus Alerts Feed URL",
					},
				},
			},
			options: {
				fields: {
					campusAlerts: {
						type: "url",
						helpers: [
							"URL to the JSON feed for campus alerts. Feed should provide JSON data which contains property alerts, which is an array of active alerts each with the properties date or updated (ISO timestamp), title (string), and description (array of strings).",
							"Set this to empty to disable receiving campus alerts.",
						],
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Campus Alerts Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.campusAlerts.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				campusAlerts: this.basic.campusAlerts,
			},
		});
	}

	/**
	 * Adjust settings pertaining to studio bookings.
	 */
	bookings() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Studio Booking Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Studio Booking Settings",
				type: "object",
				properties: {
					maxDurationPerBooking: {
						type: "number",
						minimum: 1,
						required: true,
						title: "Max Duration Per Booking (minutes)",
					},

					maxDurationPerDay: {
						type: "number",
						minimum: 1,
						required: true,
						title: "Max Total Booking Time per Day (minutes)",
					},
				},
			},
			options: {
				fields: {
					maxDurationPerBooking: {
						helpers: [
							"The maximum number of minutes a studio can be booked at one time by an org member.",
							"This setting is bypassed for bookings made by a director or through DJ Controls -> Calendar.",
						],
					},

					maxDurationPerDay: {
						helpers: [
							"The maximum number of total minutes an org member can book studios on a given day.",
							"This setting is bypassed for bookings made by a director or through DJ Controls -> Calendar.",
						],
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Booking Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.bookings.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				maxDurationPerBooking: this.basic.maxDurationPerBooking,
				maxDurationPerDay: this.basic.maxDurationPerDay,
			},
		});
	}

	/**
	 * Edit how broadcast scores for analytics are calculated.
	 */
	scores() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Broadcast Scoring Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Broadcast Scoring Settings",
				type: "object",
				properties: {
					minShowTime: {
						type: "number",
						minimum: 0,
						required: true,
						title: "Minimum Allowed Broadcast Length (minutes)",
					},

					listenerRatioPoints: {
						type: "number",
						minimum: 0,
						required: true,
						title: "Points per listener : showtime ratio",
					},

					messagePoints: {
						type: "number",
						minimum: 0,
						required: true,
						title: "Points per Message / Hour Average",
					},

					idealBreaks: {
						type: "number",
						minimum: 1, // Top of hour break is required by the FCC; do not allow values below 1.
						required: true,
						title: "Ideal Breaks Per Hour",
					},

					breakPoints: {
						type: "number",
						minimum: 0,
						required: true,
						title: "Points for Ideal Breaks Taken",
					},

					viewerRatioPoints: {
						type: "number",
						minimum: 0,
						required: true,
						title: "Points per Video Viewer : Showtime ratio",
					},

					videoStreamPoints: {
						type: "number",
						minimum: 0,
						required: true,
						title: "Points if Streamed Video for Entire Broadcast",
					},
				},
			},
			options: {
				fields: {
					minShowTime: {
						helper:
							"Broadcasts which have not been on the air for at least this many minutes get a score of 0.",
					},

					listenerRatioPoints: {
						helper:
							"Score this many points for every listener : showtime ratio (eg. every 1 average online listener)",
					},

					messagePoints: {
						helper:
							"Score this many points for every 1 average message exchanged in the chat system per hour.",
					},

					idealBreaks: {
						helper:
							"How many breaks should hosts ideally take per hour during their broadcast? Must be at least 1 for the top-of-hour break is required by law.",
					},

					breakPoints: {
						helper:
							"If the broadcast took the ideal number of breaks, award this many points. Otherwise, they will earn less points depending on how many they took. Broadcasts will never earn more points for taking more than the ideal number of breaks.",
					},

					viewerRatioPoints: {
						helper:
							"Score this many points for every video stream viewer : showtime ratio (eg. every 1 average video stream viewer)",
					},

					videoStreamPoints: {
						helper:
							"Score this many points if a broadcast streamed video the entire broadcast; otherwise, less points will be scored depending on what percent of the broadcast they streamed video.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Broadcast Scoring Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.scores.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				minShowTime: this.analytics.minShowTime,
				listenerRatioPoints: this.analytics.listenerRatioPoints,
				messagePoints: this.analytics.messagePoints,
				idealBreaks: this.analytics.idealBreaks,
				breakPoints: this.analytics.breakPoints,
				viewerRatioPoints: this.analytics.viewerRatioPoints,
				videoStreamPoints: this.analytics.videoStreamPoints,
			},
		});
	}

	/**
	 * Change OneSignal push notification settings.
	 */
	onesignal() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change OneSignal Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "OneSignal Settings",
				type: "object",
				properties: {
					oneSignalAppID: {
						type: "string",
						title: "OneSignal App ID",
					},

					oneSignalRest: {
						type: "string",
						format: "password",
						title: "OneSignal REST Key",
					},

					oneSignalCategoryMessage: {
						type: "string",
						title: "ID for Message Category 'Message'",
						pattern: /^[a-zA-Z0-9-_]+$/,
					},

					oneSignalCategoryEvent: {
						type: "string",
						title: "ID for Message Category 'Event'",
						pattern: /^[a-zA-Z0-9-_]+$/,
					},

					oneSignalCategoryAnnouncement: {
						type: "string",
						title: "ID for Message Category 'Announcement'",
						pattern: /^[a-zA-Z0-9-_]+$/,
					},

					oneSignalCategoryRequest: {
						type: "string",
						title: "ID for Message Category 'Request'",
						pattern: /^[a-zA-Z0-9-_]+$/,
					},
				},
			},
			options: {
				fields: {
					oneSignalAppID: {
						helper:
							"OneSignal App ID. You can obtain one by creating an app at https://onesignal.com . Set to blank to disable OneSignal entirely.",
					},

					oneSignalRest: {
						helper:
							"The REST secret key so the sailsJS server app can send push notifications via OneSignal.",
					},

					oneSignalCategoryMessage: {
						helper:
							"Use any alphanumeric string so long as it is different from the others in these settings.",
					},

					oneSignalCategoryEvent: {
						helper:
							"Use any alphanumeric string so long as it is different from the others in these settings.",
					},

					oneSignalCategoryAnnouncement: {
						helper:
							"Use any alphanumeric string so long as it is different from the others in these settings.",
					},

					oneSignalCategoryRequest: {
						helper:
							"Use any alphanumeric string so long as it is different from the others in these settings.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change OneSignal Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.onesignal.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				oneSignalAppID: this.basic.oneSignalAppID,
				oneSignalCategoryMessage: this.basic.oneSignalCategoryMessage,
				oneSignalCategoryEvent: this.basic.oneSignalCategoryEvent,
				oneSignalCategoryAnnouncement: this.basic.oneSignalCategoryAnnouncement,
				oneSignalCategoryRequest: this.basic.oneSignalCategoryRequest,
			},
		});
	}

	/**
	 * Configure reputation points settings
	 */
	reputation() {
		this.configUpdateCb = () => {};
		this.modal.iziModal("open");

		this.modal.title = `Change Reputation Settings`;
		this.modal.body = ``;

		$(this.modal.body).alpaca({
			schema: {
				title: "Reputation Settings",
				type: "object",
				properties: {
					warningPointsPenalty: {
						type: "number",
						title: "Penalty - Warning Points",
						minimum: 0,
					},

					cancellationPenalty: {
						type: "number",
						title: "Penalty - Cancellation",
						minimum: 0,
					},

					silenceAlarmPenalty: {
						type: "number",
						title: "Penalty - Silence Alarm",
						minimum: 0,
					},

					absencePenalty: {
						type: "number",
						title: "Penalty - Absence / No-Show",
						minimum: 0,
					},

					unauthorizedBroadcastPenalty: {
						type: "number",
						title: "Penalty - Unauthorized Broadcast",
						minimum: 0,
					},

					missedIDPenalty: {
						type: "number",
						title: "Penalty - Missed Top-of-hour ID",
						minimum: 0,
					},

					signOnEarlyPenalty: {
						type: "number",
						title: "Penalty - Early Sign-On",
						minimum: 0,
					},

					signOnLatePenalty: {
						type: "number",
						title: "Penalty - Late Sign-On",
						minimum: 0,
					},

					signOffEarlyPenalty: {
						type: "number",
						title: "Penalty - Early Sign-Off",
						minimum: 0,
					},

					signOffLatePenalty: {
						type: "number",
						title: "Penalty - Late Sign-Off",
						minimum: 0,
					},

					breakReputation: {
						type: "number",
						title: "Points - Taking Ideal Breaks Each Hour",
						minimum: 0,
					},

					showReputation: {
						type: "number",
						title: "Points - Live Broadcast",
						minimum: 0,
					},

					prerecordReputation: {
						type: "number",
						title: "Points - Prerecord",
						minimum: 0,
					},

					remoteReputation: {
						type: "number",
						title: "Points - Remote Broadcast",
						minimum: 0,
					},

					sportsReputation: {
						type: "number",
						title: "Points - Sports Broadcast",
						minimum: 0,
					},

					genreReputation: {
						type: "number",
						title: "Points - Genre Rotation",
						minimum: 0,
					},

					playlistReputation: {
						type: "number",
						title: "Points - Playlist",
						minimum: 0,
					},
				},
			},
			view: {
				wizard: {
					bindings: {
						warningPointsPenalty: 2,
						cancellationPenalty: 2,
						silenceAlarmPenalty: 2,
						absencePenalty: 2,
						unauthorizedBroadcastPenalty: 2,
						missedIDPenalty: 2,
						signOnEarlyPenalty: 2,
						signOnLatePenalty: 2,
						signOffEarlyPenalty: 2,
						signOffLatePenalty: 2,
						breakReputation: 1,
						showReputation: 1,
						prerecordReputation: 1,
						remoteReputation: 1,
						sportsReputation: 1,
						genreReputation: 1,
						playlistReputation: 1,
					},
					steps: [
						{
							title: "Rep Earned",
						},
						{
							title: "Penalties",
						},
					],
				},
			},
			options: {
				fields: {
					warningPointsPenalty: {
						helper:
							"Number of reputation points to subtract for every warning point earned (via DJ Controls -> Org Members -> member notes).",
					},

					cancellationPenalty: {
						helper:
							"Number of reputation points to subtract for every cancelled broadcast.",
					},

					silenceAlarmPenalty: {
						helper:
							"Number of reputation points to subtract for every time the silence alarm was triggered during a broadcast.",
					},

					absencePenalty: {
						helper:
							"Number of reputation points to subtract for every absent / no-show broadcast.",
					},

					unauthorizedBroadcastPenalty: {
						helper:
							"Number of reputation points to subtract for every broadcast that went on the air without being on the schedule.",
					},

					missedIDPenalty: {
						helper:
							"Number of reputation points to subtract for every time the broadcast failed to take a top-of-hour ID break.",
					},

					signOnEarlyPenalty: {
						helper:
							"Number of reputation points to subtract for every broadcast that signed on 5 or more minutes before scheduled start time.",
					},

					signOnLatePenalty: {
						helper:
							"Number of reputation points to subtract for every broadcast that signed on 5 or more minutes after scheduled start time.",
					},

					signOffEarlyPenalty: {
						helper:
							"Number of reputation points to subtract for every broadcast that signed off 5 or more minutes before scheduled end time.",
					},

					signOffLatePenalty: {
						helper:
							"Number of reputation points to subtract for every broadcast that signed off 5 or more minutes after scheduled end time.",
					},

					breakReputation: {
						helper:
							"Number of reputation points to award for taking the configured ideal number of breaks per hour.",
					},

					showReputation: {
						helper:
							"Number of reputation points to award for every live in-studio broadcast aired.",
					},

					prerecordReputation: {
						helper:
							"Number of reputation points to award for every prerecord aired.",
					},

					remoteReputation: {
						helper:
							"Number of reputation points to award for every remote broadcast aired.",
					},

					sportsReputation: {
						helper:
							"Number of reputation points to award for every sports broadcast aired.",
					},

					genreReputation: {
						helper:
							"Number of reputation points to award for every genre rotation aired.",
					},

					playlistReputation: {
						helper:
							"Number of reputation points to award for every playlist event aired.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Change Reputation Settings`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								this.change(
									this.endpoints.reputation.edit,
									value,
									this.modal.id,
									(body) => {
										if (body) this.modal.iziModal("close");
									}
								);
							},
						},
					},
				},
			},
			data: {
				warningPointsPenalty: this.analytics.warningPointsPenalty,
				cancellationPenalty: this.analytics.cancellationPenalty,
				silenceAlarmPenalty: this.analytics.silenceAlarmPenalty,
				absencePenalty: this.analytics.absencePenalty,
				unauthorizedBroadcastPenalty:
					this.analytics.unauthorizedBroadcastPenalty,
				missedIDPenalty: this.analytics.missedIDPenalty,
				signOnEarlyPenalty: this.analytics.signOnEarlyPenalty,
				signOnLatePenalty: this.analytics.signOnLatePenalty,
				signOffEarlyPenalty: this.analytics.signOffEarlyPenalty,
				signOffLatePenalty: this.analytics.signOffLatePenalty,
				breakReputation: this.analytics.breakReputation,
				showReputation: this.analytics.showReputation,
				prerecordReputation: this.analytics.prerecordReputation,
				remoteReputation: this.analytics.remoteReputation,
				sportsReputation: this.analytics.sportsReputation,
				genreReputation: this.analytics.genreReputation,
				playlistReputation: this.analytics.playlistReputation,
			},
		});

		$(this.modal.body).prepend(`<div class="callout callout-info">
		<h5>How Reputation Works</h5>
		<p>
			Every calendar event and org member has a reputationScore and reputationScoreMax. ReputationScoreMax is the maximum possible points they could have earned, and reputationScore is the number of points they earned. The final reputationScorePercent is the percent of maximum possible points that were earned.
		</p>
		<p>
			<strong>Points:</strong> Every time there was an opportunity to earn points, the reputationScoreMax will be increased by the configured number of points. If the broadcast / org member actually earned those points, their reputationScore will also be increased.
		</p>
		<p>
			<strong>Penalties:</strong> These are points directly subtracted from reputationScore, but it does not affect reputationScoreMax (maximum possible points that could have been earned). Keep in mind, for example, if an org member fails to air a live show, not only will they not earn the points they could have earned for airing their live show (reputationScoreMax was increased, but not reputationScore), but they will also incur a penalty on their reputationScore according to what you set for the absent / no-show penalty.
		</p>
		<p>
			Reputation can be viewed by org member in DJ Controls -> Org Members -> Click the analytics button next to an org member... or by broadcast in DJ Controls -> Calendar -> Manage events / schedules -> click the analytics button next to the event.
		</p>
	</div>`);
	}
}
