"use strict";

// This class manages the computer lockdown system and logging computer use

// REQUIRES these WWSUmodules: WWSUMeta, WWSUhosts, WWSUcalendar, WWSUdjs, directorReq (WWSUreq), djReq (WWSUreq), WWSUutil, WWSUanimations, WWSUdirectors, WWSUdjs, WWSUmodal
class WWSUlockdown extends WWSUdb {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super(); // Not necessary to use localStorage on logs

		this.manager = manager;

		this.endpoints = {
			get: "GET /api/lockdown",
			clockDJ: "POST /api/hosts/:ID/log-in/dj",
			clockDirector: "POST /api/hosts/:ID/log-in/director",
			clockOut: "PUT /api/hosts/:ID/log-out",
		};
		this.data = {
			get: {},
		};

		this.assignSocketEvent("lockdown", this.manager.socket);

		// DOM elements
		this.reservations;
		this.logout;

		this.lockdown = {
			type: null,
			ID: null,
		};

		this.lockdownTimeForced;

		this.prevIdleTime = 0;

		this.table;

		this.logsModal = new WWSUmodal(`Lockdown Logs`, null, ``, true, {
			zindex: 1200,
			width: 800,
		});

		// Remove completed lockdown records out of memory; we do not need them
		this.on("change", "WWSUlockdown", (db) => {
			let complete = db
				.get()
				.find((record) => record.clockIn && record.clockOut);
			if (complete) this.query({ remove: complete.ID });
		});
	}

	/**
	 * Should be called immediately on application load.
	 *
	 * @param {string} logout DOM query string of the log out button.
	 */
	init(logout) {
		this.logout = logout;

		// host event
		this.manager
			.get("WWSUhosts")
			.on("clientLockdown", "WWSUlockdown", (ldstate) => {
				if (ldstate && (!this.lockdown || !this.lockdown.type)) {
					this.emitEvent("lockDown", [true]);
					this.lockdown = { type: null, ID: null };
				} else if (!ldstate) {
					this.emitEvent("lockDown", [false]);
					this.emitEvent("lockDownEnabled", [false]);
					this.lockdown = { type: null, ID: null };
				}

				if (ldstate === "director") {
					// Automatic lock down if for some reason a member is logged in on a director only machine
					if (this.lockdown && this.lockdown.type !== "director")
						this.lockDown();
				}
			});

		// Add preNewMeta listener so we can initiate auto lock down when necessary when a broadcast ends
		this.manager
			.get("WWSUMeta")
			.on("preNewMeta", "WWSUlockdown", (updated, oldMeta) => {
				// If this host is no longer host of the broadcast, we should check for auto lock down.
				if (
					typeof updated.host !== "undefined" &&
					updated.host !== oldMeta.host &&
					oldMeta.host === this.manager.get("WWSUhosts").client.ID
				) {
					// Ignore if we are not supposed to lock down or a clock was already started
					if (
						!this.manager.get("WWSUhosts").client.lockDown ||
						!this.lockdown ||
						!this.lockdown.type ||
						this.lockdown.type === "director" ||
						this.lockdownTimeForced
					) {
						return false;
					}

					// Start a 5-minute auto lock down and emit a timed lock down event so renderer can show a message about it
					this.lockdownTimeForced = 60 * 5;
					this.emitEvent("timedLockDown", [
						moment().add(this.lockdownTimeForced, "seconds").format("LTS"),
					]);
				}
			});

		// Logout button
		$(this.logout).click(() => {
			this.promptLockDown();
		});
	}

	/**
	 * Call this instead of init() when we are only using lockdown module for lockdown data.
	 */
	initSimple() {
		this.replaceData(this.manager.get("noReq"), this.endpoints.get, {});
	}

	/**
	 * Refresh logs of who is clocked in. Call after client/host initialization and changes.
	 */
	initData() {
		this.manager.get("noReq").request(
			this.endpoints.get,
			{
				data: {},
			},
			{},
			(body, resp) => {
				if (!resp.statusCode < 400 && body.constructor === Array) {
					let record = body.find(
						(res) =>
							res.host === this.manager.get("WWSUhosts").client.ID &&
							res.clockOut === null
					);
					if (record) {
						this.lockdown = {
							type: record.type,
							ID: record.typeID,
						};
						// Automatic lock down if for some reason a member is logged in on a director only machine. Else, log in.
						if (
							this.manager.get("WWSUhosts").client.lockDown === "director" &&
							this.lockdown &&
							this.lockdown.type !== "director"
						) {
							this.lockDown();
						} else {
							this.emitEvent("lockDown", [false]);
							$(this.logout).removeClass("d-none");
							this.emitEvent("lockDownEnabled", [record.type !== "director"]);
							$(document).Toasts("create", {
								class: "bg-info",
								title: "Re-logged in",
								body: "DJ Controls was restarted when someone was logged in to this computer. <strong>Click LOG OUT when you are done using this computer.</strong>",
								autohide: true,
								delay: 15000,
							});
						}
					}
				}
			}
		);
	}

	/**
	 * Initialize the lockdown logs data table.
	 *
	 * @param {string} dom DOM query string where the table should be created in (div).
	 * @param {string} date DOM query string of the date selection
	 * @param {string} browse DOM query string of the browse button
	 */
	initTable(dom) {
		this.manager
			.get("WWSUanimations")
			.add("lockdown-init-attendance-table", () => {
				// Init html
				$(dom).html(
					`<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
						this.manager.get("WWSUMeta")
							? this.manager.get("WWSUMeta").meta.timezone
							: moment.tz.guess()
					}.</p><table id="section-logs-lockdown-table" class="table table-striped display responsive" style="width: 100%;"></table>`
				);

				this.manager
					.get("WWSUutil")
					.waitForElement(`#section-logs-lockdown-table`, () => {
						// Generate table
						this.table = $(`#section-logs-lockdown-table`).DataTable({
							paging: true,
							data: [],
							columns: [
								{ title: "ID" },
								{ title: "Host" },
								{ title: "Person" },
								{ title: "Log In" },
								{ title: "Log Out" },
							],
							pageLength: 50,
							buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
						});

						this.table
							.buttons()
							.container()
							.appendTo(
								$(`#section-logs-lockdown-table_wrapper .col-md-6:eq(0)`)
							);
					});
			});
	}

	/**
	 * Try logging in as a member.
	 *
	 * @param {string} username Name of the member
	 * @param {string} password Password of the member
	 * @param {function} cb Callback function. Parameter is true if successful, or object of {title: string, error: string} if failed.
	 */
	tryDjLogin(username, password, cb = () => {}) {
		// Forbid logging in as member if this is a director lock down
		if (this.manager.get("WWSUhosts").client.lockDown === "director") {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			cb({
				title: "Members not allowed",
				error:
					"Only directors may log in to this computer. Please contact a director if you think this is an error.",
			});
			return;
		}

		// Check for any reservations or scheduled shows
		let reservations = this.manager
			.get("WWSUcalendar")
			.getReservations(this.manager.get("WWSUhosts").client.lockDown);

		// Remove reservations more than 5 minutes away from start time; should not allow logging in for these
		reservations = reservations.filter((reservation) => {
			return moment(this.meta ? this.meta.meta.time : undefined)
				.add(5, "minutes")
				.isSameOrAfter(moment(reservation.start));
		});

		// No reservations? Bail with an error.
		if (!reservations || reservations.length === 0) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			cb({
				title: "No scheduled bookings",
				error:
					"There are no bookings scheduled at this time. You may not log in any more than 5 minutes before your booking time. Please contact a director if you think this is an error.",
			});
			return;
		}
		// Find the member record of the username DJ
		let query = {};
		query[this.manager.get("djReq").usernameField] = username;
		let DJ = this.manager.get(this.manager.get("djReq").db).find(query, true);
		if (!DJ) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			cb({
				title: "Error finding member",
				error:
					"There was an error finding that member in the system. Please contact the engineer.",
			});
			return;
		}

		// Filter reservations even further to only include those which lists this member
		reservations = reservations.filter((reservation) => {
			if (reservation.hostDJ === DJ.ID) return true;
			if (reservation.cohostDJ1 === DJ.ID) return true;
			if (reservation.cohostDJ2 === DJ.ID) return true;
			if (reservation.cohostDJ3 === DJ.ID) return true;
			return false;
		});

		// No reservations? Bail with an error.
		if (!reservations || reservations.length === 0) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			cb({
				title: "No scheduled bookings",
				error:
					"You are not scheduled to use this machine at this time. You may not log in any more than 5 minutes before your booking time. Please contact a director if you think this is an error.",
			});
			return;
		}

		// At this point, the member may log in, so try to authorize.
		this.manager.get("djReq")._authorize(username, password, (token) => {
			if (token === 0) {
				if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
				cb({
					title: "Error Authorizing",
					error:
						"There was an error authorizing. Did you type your password in correctly?",
				});
				return;
			} else if (
				typeof token.errToken !== `undefined` ||
				typeof token.token === "undefined"
			) {
				if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
				cb({
					title: "Error Authorizing",
					error: `${
						typeof token.errToken !== `undefined`
							? `Failed to authenticate; please try again. ${token.errToken}`
							: `Failed to authenticate; unknown error.`
					}`,
				});
				return;
			} else {
				this.manager.get("djReq")._tryRequest(
					this.endpoints.clockDJ,
					{
						data: {
							ID: this.manager.get("WWSUhosts").client.ID,
						},
					},
					{},
					(body2, resp) => {
						if (resp.statusCode < 400) {
							this.lockdown = {
								type: "DJ",
								ID: DJ.ID,
							};
							$(document).Toasts("create", {
								class: "bg-success",
								title: "Logged In!",
								body: `You have logged in! The computer will auto-lock ${
									reservations[0].type.endsWith("-booking")
										? `at ${moment
												.tz(
													reservations[0].end,
													this.manager.get("WWSUMeta")
														? this.manager.get("WWSUMeta").meta.timezone
														: moment.tz.guess()
												)
												.format("h:mm A")}`
										: `when you end your broadcast, or at ${moment
												.tz(
													reservations[0].end,
													this.manager.get("WWSUMeta")
														? this.manager.get("WWSUMeta").meta.timezone
														: moment.tz.guess()
												)
												.format("h:mm A")} if you do not start one.`
								}<br /><strong>Click LOG OUT when you are done using the computer</strong> and have signed out of everything and saved your work.`,
								autohide: true,
								delay: 30000,
							});
							this.emitEvent("lockDown", [false]);
							$(this.logout).removeClass("d-none");
							cb(true);
						} else {
							cb({
								title: "Error Logging in",
								error: body2,
							});
						}
					}
				);
			}
		});
	}

	/**
	 * Try logging in as a director.
	 *
	 * @param {string} username Name of the director
	 * @param {string} password Password provided
	 * @param {function} cb Callback. Parameter is true if successful, {title: string, error: string} if failed.
	 */
	tryDirectorLogin(username, password, cb) {
		// Find the director record of the username Director
		let query = {};
		query[this.manager.get("directorReq").usernameField] = username;
		let director = this.manager
			.get(this.manager.get("directorReq").db)
			.find(query, true);
		if (!director) {
			if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
			cb({
				title: "Error finding director",
				error:
					"There was an error finding that director in the system. Please contact the engineer.",
			});
			return;
		}

		// try to authorize.
		this.manager.get("directorReq")._authorize(username, password, (token) => {
			if (token === 0) {
				if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
				cb({
					title: "Error Authorizing",
					error:
						"There was an error authorizing. Did you type your password in correctly?",
				});
				return;
			} else if (
				typeof token.errToken !== `undefined` ||
				typeof token.token === "undefined"
			) {
				if (this.manager.has("WWSUehhh")) this.manager.get("WWSUehhh").play();
				cb({
					title: "Error Authorizing",
					error: `${
						typeof token.errToken !== `undefined`
							? `Failed to authenticate; please try again. ${token.errToken}`
							: `Failed to authenticate; unknown error.`
					}`,
				});
				return;
			} else {
				this.manager.get("directorReq")._tryRequest(
					this.endpoints.clockDirector,
					{
						data: {
							ID: this.manager.get("WWSUhosts").client.ID,
						},
					},
					{},
					(body2, resp) => {
						if (resp.statusCode < 400) {
							this.lockdown = {
								type: "director",
								ID: director.ID,
							};
							$(document).Toasts("create", {
								class: "bg-success",
								title: "Logged In!",
								body: `You have logged in! The machine will auto-lock when you go inactive for 10 minutes.<br /><strong>Click LOG OUT when you are done using the computer</strong> and have signed out of everything and saved your work.`,
								autohide: true,
								delay: 30000,
							});
							this.emitEvent("lockDown", [false]);
							this.emitEvent("lockDownEnabled", [false]);
							$(this.logout).removeClass("d-none");
							cb(true);
						} else {
							cb({
								title: "Error Logging In",
								error: body2,
							});
						}
					}
				);
			}
		});
	}

	/**
	 * Get logs of lockdown system / computer use.
	 *
	 * @param {string} dom DOM query string of the table to lock while fetching
	 * @param {object} data Parameters to be passed to the API
	 * @param {function} cb Callback to call with results from the server
	 */
	getLogs(dom, data, cb) {
		this.manager
			.get("hostReq")
			.request(this.endpoints.get, { data }, { dom }, (body, resp) => {
				if (resp.statusCode < 400 && body.constructor === Array) {
					cb(body);
				}
			});
	}

	/**
	 * Populate the logs table with records
	 *
	 * @param {string} date The date of logs to view
	 */
	showLogs(date) {
		console.log(date);
		this.getLogs(`#section-logs-lockdown-table`, { date }, (records) => {
			console.log(records);
			this.table.clear();
			records.map((record) => {
				let host = this.manager
					.get("WWSUhosts")
					.find({ ID: record.host }, true);
				if (host) {
					host = host.friendlyname;
				} else {
					host = `Unknown host ${record.host}`;
				}

				let person = "Unknown";
				if (record.type === "director") {
					person = this.manager
						.get("WWSUdirectors")
						.find({ ID: record.typeID }, true);
					if (person) {
						person = `<span class="p-1 badge badge-warning">Director: </span>${person.name}`;
					} else {
						person = `<span class="p-1 badge badge-warning">Director: </span> Unknown ${record.typeID}`;
					}
				} else if (record.type === "DJ") {
					person = this.manager
						.get("WWSUdjs")
						.find({ ID: record.typeID }, true);
					if (person) {
						person = `<span class="p-1 badge badge-primary">Member:</span> ${person.name}`;
					} else {
						person = `<span class="p-1 badge badge-primary">Member:</span> Unknown ${record.typeID}`;
					}
				}
				this.table.rows.add([
					[
						record.ID,
						host,
						person,
						moment
							.tz(
								record.clockIn,
								this.manager.get("WWSUMeta").meta.timezone || moment.tz.guess()
							)
							.format("h:mm A"),
						record.clockOut
							? moment
									.tz(
										record.clockOut,
										this.manager.get("WWSUMeta").meta.timezone ||
											moment.tz.guess()
									)
									.format("h:mm A")
							: "LOGGED IN",
					],
				]);
			});

			this.table.draw(false);
		});
	}

	/**
	 * View logs in a modal window
	 *
	 * @param {object} data data to be passed to the API when getting logs
	 */
	showLog(data) {
		this.logsModal.iziModal("open");
		this.logsModal.body = `<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
			this.manager.get("WWSUMeta")
				? this.manager.get("WWSUMeta").meta.timezone
				: moment.tz.guess()
		}.</p><table id="modal-${
			this.logsModal.id
		}-body-log" class="table table-striped display responsive" style="width: 100%;"></table>`;
		this.manager
			.get("WWSUutil")
			.waitForElement(`#modal-${this.logsModal.id}-body-log`, () => {
				this.getLogs(
					`#modal-${this.logsModal.id}-body-log`,
					data,
					(records) => {
						let table;
						table = $(`#modal-${this.logsModal.id}-body-log`).DataTable({
							paging: true,
							data: records.map((record) => {
								let host = this.manager
									.get("WWSUhosts")
									.find({ ID: record.host }, true);
								if (host) {
									host = host.friendlyname;
								} else {
									host = `Unknown host ${record.host}`;
								}

								let person = "Unknown";
								if (record.type === "director") {
									person = this.manager
										.get("WWSUdirectors")
										.find({ ID: record.typeID }, true);
									if (person) {
										person = `<span class="p-1 badge badge-warning">Director: </span>${person.name}`;
									} else {
										person = `<span class="p-1 badge badge-warning">Director: </span> Unknown ${record.typeID}`;
									}
								} else if (record.type === "DJ") {
									person = this.manager
										.get("WWSUdjs")
										.find({ ID: record.typeID }, true);
									if (person) {
										person = `<span class="p-1 badge badge-primary">Member:</span> ${person.name}`;
									} else {
										person = `<span class="p-1 badge badge-primary">Member:</span> Unknown ${record.typeID}`;
									}
								}
								return [
									record.ID,
									host,
									person,
									moment
										.tz(
											record.clockIn,
											this.manager.get("WWSUMeta").meta.timezone ||
												moment.tz.guess()
										)
										.format("lll"),
									record.clockOut
										? moment
												.tz(
													record.clockOut,
													this.manager.get("WWSUMeta").meta.timezone ||
														moment.tz.guess()
												)
												.format("lll")
										: "LOGGED IN",
								];
							}),
							columns: [
								{ title: "ID" },
								{ title: "Host" },
								{ title: "Person" },
								{ title: "Log In" },
								{ title: "Log Out" },
							],
							pageLength: 50,
							buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
							order: [[0, "desc"]],
						});

						table
							.buttons()
							.container()
							.appendTo(
								$(
									`#modal-${this.logsModal.id}-body-log_wrapper .col-md-6:eq(0)`
								)
							);
					}
				);
			});
	}

	/**
	 * Do a confirmation before locking down.
	 * @param {function} cb Optional callback containing undefined if successful, else body string if error.
	 */
	promptLockDown(cb) {
		// Ignore if not logged in
		if (!this.lockdown || !this.lockdown.type) return;

		this.manager
			.get("WWSUutil")
			.confirmDialog(
				`Are you sure you want to log out? <br /><br /><strong>Make sure you saved all your work and you logged out of all personal accounts.</strong>`,
				null,
				() => {
					this.lockDown(cb);
				}
			);
	}

	/**
	 * Log out of the currently logged in user and lock down.
	 * @param {function} cb Optional callback containing undefined if successful, else body string if error.
	 */
	lockDown(cb) {
		// Ignore if not logged in
		if (!this.lockdown || !this.lockdown.type) return;

		// Try again in 15 seconds if we have an issue
		this.lockdownTimeForced = 15;

		// Send a log out request
		this.manager.get("hostReq").request(
			this.endpoints.clockOut,
			{
				data: {
					ID: this.manager.get("WWSUhosts").client.ID,
				},
			},
			{},
			(body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") cb(body);
				} else {
					// Lock down the system
					this.lockdownTimeForced = undefined;
					this.prevIdleTime = Infinity;
					this.emitEvent("lockDown", [true]);
					this.emitEvent("lockDownEnabled", [true]);
					this.lockdown = { type: null, ID: null };
					$(this.logout).addClass("d-none");

					if (typeof cb === "function") cb();
				}
			}
		);
	}

	/**
	 * Update the table of upcoming reservations. Also runs checks for active reservations and starts locking down if someone runs out of time.
	 *
	 * @returns {Promise<object>} {reservations: Array, message: string, time: string}. Message and time will be undefined if we are not supposed to show a lockdown message.
	 */
	updateReservations() {
		return new Promise((resolve, reject) => {
			// Do not bother making any calculations if this host is not set to lock down or if it is set to directors only
			if (
				!this.manager.get("WWSUhosts").client.lockDown ||
				this.manager.get("WWSUhosts").client.lockDown === "director"
			)
				return resolve({ reservations: [] });

			this.manager
				.get("WWSUcalendar")
				.getReservations(
					this.manager.get("WWSUhosts").client.lockDown,
					(events) => {
						// Check for expired reservations

						// Ignore if we are not to lock down, this host started the current broadcast, if we are pending to lock down, or if a director is logged in
						if (
							!this.manager.get("WWSUhosts").client.lockDown ||
							!this.lockdown ||
							!this.lockdown.type ||
							this.lockdown.type === "director" ||
							this.lockdownTimeForced ||
							this.manager.get("WWSUhosts").isHost
						) {
							return resolve({ reservations: events });
						}

						// Find a reservation where the currently logged in member is a part of it.
						let authorized = events
							.filter((event) => {
								return moment(this.manager.get("WWSUMeta").meta.time)
									.add(5, "minutes")
									.isSameOrAfter(moment(event.start));
							})
							.find(
								(event) =>
									[
										event.hostDJ,
										event.cohostDJ1,
										event.cohostDJ2,
										event.cohostDJ3,
									].indexOf(this.lockdown.ID) !== -1
							);

						// If an event was found, the member is allowed to remain logged in
						if (authorized) return resolve({ reservations: events });

						// At this point, they are not allowed to be logged in anymore. Start a 5-minute lockdown timer.
						this.lockdownTimeForced = 60 * 5;
						return resolve({
							reservations: events,
							message: `Your reservation time is up! This computer will automatically lock in 5 minutes. <strong>Please save all your work and log out of all personal accounts.</strong>`,
							time: moment()
								.add(this.lockdownTimeForced, "seconds")
								.format("LTS"),
						});
					}
				);
		});
	}

	/**
	 * Check whether or not we should auto-lock the computer. Should be called every second.
	 *
	 * @param {number} idleTime Number of seconds since last activity
	 * @returns {object|boolean} If a notification should be launched, an object will be returned containing obj.time (when lockdown will occur) and obj.message (for notification). Otherwise, this will return a boolean: true if any existing messages should remain on screen, false if they should be closed.
	 */
	checkIdleTime(idleTime) {
		// Close everything and bail if this client does not lock down
		if (
			!this.manager.get("WWSUhosts").client.lockDown ||
			!this.lockdown ||
			!this.lockdown.type
		) {
			this.lockdownTimeForced = undefined;
			this.prevIdleTime = 0;
			return false;
		}

		// Also bail, but do not reset lockdownTimeForced, if this host started the current broadcast
		if (this.manager.get("WWSUhosts").isHost) {
			this.prevIdleTime = 0;
			return false;
		}

		// If forced lockdown is enabled, process that instead of idle time.
		if (typeof this.lockdownTimeForced !== "undefined") {
			this.lockdownTimeForced--;

			// Time is up! Lock down.
			if (this.lockdownTimeForced < 1) {
				this.lockDown();
				return false;
			}

			// One minute warning
			if (this.lockdownTimeForced === 60) {
				return {
					message:
						"This is your ONE MINUTE warning! This computer will lock down in one minute!",
					time: moment().add(this.lockdownTimeForced, "seconds").format("LTS"),
				};
			}

			return true;
		}

		// After 10 minutes of idle time, request a warning
		if (this.prevIdleTime < 60 * 10 && idleTime >= 60 * 10) {
			this.prevIdleTime = 60 * 10;
			return {
				message:
					"This computer has been idle for 10 minutes. You will be automatically locked out in one minute unless you become active again.",
				time: moment()
					.add(60 * 10 - idleTime + 60, "seconds")
					.format("h:mm:ss A"),
			};
		}

		// After 11 minutes, auto lock down
		if (this.prevIdleTime < 60 * 10 + 60 && idleTime >= 60 * 10 + 60) {
			this.lockDown();
			return false;
		}

		// If we became active again, close all windows
		if (idleTime < this.prevIdleTime) {
			this.prevIdleTime = idleTime;
			return false;
		}

		// At this point, we are idle. Keep any open windows open.
		this.prevIdleTime = idleTime;
		return true;
	}

	/**
	 * Lock down if we have an enforced counter going.
	 *
	 * @returns {boolean} True if we locked down, false if we did not.
	 * @param {function} cb Optional callback containing undefined if successful, else body string if error, called only if we tried locking down.
	 */
	lockDownIfEnforced(cb) {
		// Ignore if we are not supposed to lock down
		if (
			!this.manager.get("WWSUhosts").client.lockDown ||
			!this.lockdown ||
			!this.lockdown.type ||
			!this.lockdownTimeForced
		) {
			return false;
		}

		this.lockDown(cb);
		return true;
	}

	/**
	 * Check if the currently logged in member or director has a certain permission.
	 *
	 * @param {string} permission Permission to check (org, book, live, sports, or remote)
	 * @returns {boolean} True if the logged in member has permission
	 */
	checkPermission(permission) {
		// No one logged in? No permission!
		if (!this.lockdown.type) return false;

		// Member is logged in
		if (this.lockdown.type === "DJ") {
			let dj = this.manager
				.get("WWSUdjs")
				.find({ ID: this.lockdown.ID, active: true }, true);
			if (!dj || dj.permissions.indexOf(permission) === -1) {
				return false;
			}

			// Director is logged in
		} else if (this.lockdown.type === "director") {
			let director = this.manager
				.get("WWSUdirectors")
				.find({ ID: this.lockdown.ID }, true);
			if (!director) {
				return false;
			}

			// We need to get the org member of the director to see what permissions they have
			let dj = this.manager
				.get("WWSUdjs")
				.find({ realName: director.name, active: true }, true);
			if (!dj || dj.permissions.indexOf(permission) === -1) {
				return false;
			}
		}

		return true;
	}
}
