"use strict";

// This class manages push notification subscriptions with OneSignal.

// TODO: fix removal to allow removing unique or ID individually.
// TODO: Extend with WWSUdb. (??)

// REQUIRES these WWSUmodules: WWWSUconfig, WWSUdb, noReq (WWSUreq), WWSUrecipientsweb, WWSUanimations, WWSUutil, WWSUmodal
// ALSO REQUIRES OneSignal SDK to be included.
class WWSUsubscriptions extends WWSUdb {
	/**
	 * Construct the class.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super();

		this.manager = manager;

		this.endpoints = {
			get: "GET /api/subscribers/web/:device",
			subscribe: "POST /api/subscribers/web/:device",
			unsubscribe: "DELETE /api/subscribers/web/:device",
		};

		this.assignSocketEvent("subscribers", this.manager.socket);

		this.initialized = false; // Track when we fully loaded OneSignal
		this.device = null; // Track this user's OneSignal Id
		this.canShowPushNotifications; // Track if this device supports notifications
		this.permission = "denied"; // Track the user's browser notification permission setting
		this.pendingCb; // Track a callback function waiting to be called when a user grants notification permissions.

		this.table;

		this.on("change", "WWSUsubscriptions", (db) => {
			this.updateTable();
		});

		this.modals = {
			askByUnique: new WWSUmodal(`Choose Notification Type`, null, ``, true, {
				headerColor: "",
				zindex: 1100,
			}),
		};
	}

	/**
	 * Get the timezone we should use.
	 */
	get timezone() {
		return this.manager.has("WWSUMeta")
			? this.manager.get("WWSUMeta").timezone
			: moment.tz.guess();
	}

	/**
	 * Initialize the push notifications system with OneSignal.
	 */
	init() {
		let appID = this.manager.get("WWSUconfig").basic.oneSignalAppID;

		// If OneSignal is not configured, bail immediately.
		if (!appID || appID === "") return;

		// Do not re-initialize OneSignal. Instead, just re-fetch subscriptions and update device Id with WWSU.
		if (this.initialized) {
			this.updateSubscriptions();

			this.manager
				.get("WWSUrecipientsweb")
				.editRecipientWeb({ device: this.device });
			return;
		}

		// Initialize OneSignal.
		OneSignal.push(() => {
			OneSignal.init({
				appId: appID,
				welcomeNotification: {
					disable: true, // We do not want to bug new visitors with a welcome prompt; let them find the subscribe buttons themselves.
				},
				safari_web_id: "",
			});
			this.initialized = true;
			console.log(`OneSignal: INITIALIZED.`);

			// Also get the device Id
			OneSignal.getUserId((userId) => {
				this.device = userId;
				console.log(`OneSignal: Device Id is ${this.device}`);

				this.manager
					.get("WWSUrecipientsweb")
					.editRecipientWeb({ device: this.device });

				this.updateSubscriptions();
			});
		});

		// Check if this browser allows and supports push notifications.
		OneSignal.push(() => {
			this.canShowPushNotifications = OneSignal.isPushNotificationsSupported();
			console.log(
				`OneSignal: Can show push notifications on this device? ${this.canShowPushNotifications}`
			);
		});

		// Get the current state of the user's notification settings.
		OneSignal.push([
			"getNotificationPermission",
			(permission) => {
				this.permission = permission;
				console.log(
					`OneSignal: Push Notification permissions currently at ${this.permission}.`
				);
			},
		]);

		// Also monitor for changes in browser notification settings
		OneSignal.push(() => {
			OneSignal.on("notificationPermissionChange", (permission) => {
				this.permission = permission.to;
				console.log(
					`OneSignal: Push Notification permissions changed to ${this.permission}.`
				);

				// Clear pending cbs if we denied permission
				if (this.permission === "denied") {
					this.pendingCb = undefined;
					this.device = null;
					this.manager
						.get("WWSUrecipientsweb")
						.editRecipientWeb({ device: null });

					this.updateSubscriptions();
				} else {
					$(document).Toasts("create", {
						class: "bg-warning",
						title: "Might need to refresh",
						body: "You have approved notifications! <br />If you requested to subscribe to notifications and do not get a prompt in the next few seconds, please refresh the page and try again.",
						autohide: true,
						delay: 30000,
					});
				}
			});
		});

		// Also track the state of push notification subscriptions
		OneSignal.push(() => {
			OneSignal.on("subscriptionChange", (isSubscribed) => {
				// If subscribed, update our device Id and execute any pending cbs.
				if (isSubscribed) {
					OneSignal.getUserId((userId) => {
						this.device = userId;
						console.log(`OneSignal: Device Id is ${this.device}`);

						this.manager
							.get("WWSUrecipientsweb")
							.editRecipientWeb({ device: this.device });

						this.updateSubscriptions();

						if (this.pendingCb) {
							this.pendingCb();
							setTimeout(() => {
								this.pendingCb = undefined;
							}, 3000);
						}
					});
				} else {
					this.device = null;
					this.manager
						.get("WWSUrecipientsweb")
						.editRecipientWeb({ device: null });

					this.updateSubscriptions();
					this.pendingCb = undefined;
				}

				console.log(
					`OneSignal: Subscription status changed to ${isSubscribed}.`
				);
			});
		});
	}

	/**
	 * Re-check what subscriptions are present for this device.
	 */
	updateSubscriptions() {
		if (this.device) {
			this.replaceData(this.manager.get("noReq"), this.endpoints.get, {
				device: this.device,
			});
		} else {
			this.db().remove();
			this.updateTable();
		}
	}

	/**
	 * Initialize table for managing notification subscriptions
	 *
	 * @param {string} table DOM query string of the div where to place the table
	 */
	initTable(table) {
		this.manager.get("WWSUanimations").add("subscriptions-init-table", () => {
			// Init html
			$(table).html(
				`<table id="subscriptions-table" class="table table-striped display responsive" style="width: 100%;"></table>`
			);

			this.manager
				.get("WWSUutil")
				.waitForElement(`#subscriptions-table`, () => {
					// Generate table
					this.table = $(`#subscriptions-table`).DataTable({
						paging: true,
						data: [],
						columns: [{ title: "Description" }, { title: "Actions" }],
						columnDefs: [{ responsivePriority: 1, targets: 1 }],
						pageLength: 25,
						drawCallback: () => {
							// Action button click events
							$(".btn-subscription-delete").unbind("click");

							$(".btn-subscription-delete").click((e) => {
								let subscription = this.find().find(
									(subsc) =>
										subsc.ID === parseInt($(e.currentTarget).data("id"))
								);

								if (!subscription) return;

								this.manager
									.get("WWSUutil")
									.confirmDialog(
										`Are you sure you want to stop receiving notifications for ${subscription.description}?`,
										null,
										() => {
											this.unsubscribe(subscription.type, subscription.subtype);
										}
									);
							});
						},
					});

					// Update with information
					this.updateTable();
				});
		});
	}

	/**
	 * Update the subscriptions table if it exists
	 */
	updateTable() {
		this.manager.get("WWSUanimations").add("subscription-update-table", () => {
			if (this.table) {
				this.table.clear();

				// Default uneditable subscriptions
				if (this.device) {
					this.table.row.add([
						`High-priority WWSU notifications (such as station downtime or big events)`,
						``,
					]);
					this.table.row.add([`Your track requests begin playing on WWSU`, ``]);
					this.table.row.add([
						`Chat messages from show hosts (only if you visited the site in the last few hours)`,
						``,
					]);

					this.find().forEach((record) => {
						let description = record.description;

						// Do not use database description for calendar subscriptions; details might have changed.
						if (record.type === "calendar") {
							let temp = record.subtype.split("-");
							let calendarEvent = this.manager
								.get("WWSUcalendar")
								.calendar.find({ ID: parseInt(temp[0]) }, true);
							if (!temp[1]) {
								description = `${
									calendarEvent ? calendarEvent.name : `Unknown`
								} (all broadcasts)`;
							} else if (!temp[2] || temp[1] !== "null") {
								let event = this.manager
									.get("WWSUcalendar")
									.getEventsByUnique(record.subtype);
								if (event[0]) {
									description = `${event[0].name} for ${moment(
										event[0].start
									).format(
										"LLLL"
									)} (station time) (will be auto-removed after broadcast airs on this date/time).`;
								}
							} else if (temp[2]) {
								let scheduleEvent = this.manager
									.get("WWSUcalendar")
									.schedule.find({ ID: parseInt(temp[2]) }, true);

								description = `${
									calendarEvent ? calendarEvent.name : `Unknown`
								} ${this.manager
									.get("WWSUcalendar")
									.generateScheduleText(scheduleEvent)} (station time)`;
							}
						}

						this.table.row.add([
							description,
							`<button class="btn btn-sm btn-danger btn-subscription-delete" title="Stop receiving these notifications" data-id="${record.ID}"><i class="fas fa-bell-slash"></i></button>`,
						]);
					});
				} else {
					this.table.row.add([
						`<strong>You are not currently subscribed to receive push notifications.</strong> To subscribe, click any "subscribe" button throughout the website to be prompted.`,
						``,
					]);
				}
				this.table.draw(false);
			}
		});
	}

	/**
	 * Check if this user can use push notifications. Show relevant errors or prompts otherwise.
	 * @param {function} cb Function to execute once (and if) we have the ability to send notifications.
	 */
	checkStatus(cb) {
		if (typeof cb !== "function") throw new Error("cb must be a function.");

		// OneSignal not active? Exit.
		if (!this.manager.get("WWSUconfig").basic.oneSignalAppID || this.manager.get("WWSUconfig").basic.oneSignalAppID === "") {
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "Not Available",
				body: "WWSU has not enabled / configured push notifications at this time.",
				autohide: true,
				delay: 15000,
			});
			return;
		}

		// Not initialized yet? Show an error and bail.
		if (!this.initialized) {
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "Notifications Not Ready",
				body: "The push notifications system, OneSignal, has not yet initialized. Please try again in several seconds. If you continue to have issues, please report this to the engineer at wwsu4@wright.edu.",
				autohide: true,
				delay: 30000,
			});
			return;
		}

		// Browser does not support push notifications. Bail with an error.
		if (!this.canShowPushNotifications) {
			$(document).Toasts("create", {
				class: "bg-danger",
				title: "Notifications Not Supported",
				body: "Sorry, but your device / browser does not support push notifications. Please contact the engineer at wwsu4@wright.edu if you think this is a bug.",
				autohide: true,
				delay: 20000,
			});
			return;
		}

		// If the user denied notifications, we cannot proceed
		if (this.permission === "denied") {
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "Notifications Denied",
				body: "You have denied notifications for WWSU. Please adjust your notification settings for our website in your browser or on your device.",
				autohide: true,
				delay: 20000,
			});
			return;
		}

		// User is not subscribed for push notifications, but can be subscribed / did not explicitly deny notification permissions
		if (!this.device) {
			OneSignal.push(() => {
				this.pendingCb = cb;
				OneSignal.showNativePrompt();
				if (this.permission === "granted") {
					$(document).Toasts("create", {
						class: "bg-warning",
						title: "Refresh the page",
						body: "Oops! Something went wrong. Please try refreshing the webpage to re-load the notifications system.",
						autohide: true,
						delay: 10000,
					});
				}
			});
			return;
		}

		// At this point, the user is able to do notifications, so execute the callback.
		cb();
	}

	/**
	 * Subscribe to an event
	 *
	 * @param {string} type calendar-once for single occurrence, or calendar-all for all occurrences
	 * @param {string} subtype occurrence unique string for calendar-once type, or calendar ID for calendar-all type
	 * @param {string} description Human-friendly description of this subscription.
	 */
	subscribe(type, subtype, description) {
		this.checkStatus(() => {
			this.manager.get("noReq").request(
				this.endpoints.subscribe,
				{
					data: { device: this.device, type, subtype, description },
				},
				{},
				(body, resp) => {
					if (resp.statusCode < 400) {
						$(document).Toasts("create", {
							class: "bg-success",
							title: "Subscribed!",
							autohide: true,
							delay: 30000,
							body: `<p>You were subscribed for push notifications!</p><p>You can unsubscribe at any time by reviewing your settings on the "subscriptions" page.</p><p><strong>Visitors may stop receiving notifications if they go 3 or more months without visiting the website, or if they clear their browser cache.</strong></p>`,
							icon: "fas fa-bell fa-lg",
						});
					}
				}
			);
		});
	}

	/**
	 * Unsubscribe from an event.
	 *
	 * @param {string} type Type to unsubscribe
	 * @param {string} subtype Subtype to unsubscribe
	 */
	unsubscribe(type, subtype) {
		this.manager.get("noReq").request(
			this.endpoints.unsubscribe,
			{
				data: { device: this.device, type, subtype },
			},
			{},
			(body, resp) => {
				if (resp.statusCode < 400) {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "Un-subscribed!",
						body: "You successfully unsubscribed from those notifications.",
						autohide: true,
						delay: 10000,
					});
				}
			}
		);
	}

	/**
	 * Prompt what kind of notifications the user wants to receive based on the calendarUnique of an event.
	 *
	 * @param {?string} calendarUnique The unique calendar event ID. Null = error saying this broadcast cannot be subscribed to.
	 */
	askByUnique(calendarUnique = null) {
		if (!calendarUnique) {
			$(document).Toasts("create", {
				class: "bg-warning",
				title: "Cannot Subscribe to this Event",
				body: "Sorry, but push notifications are not available for this broadcast / event.",
				autohide: true,
				delay: 10000,
			});
			return;
		}

		let event = this.manager
			.get("WWSUcalendar")
			.getEventsByUnique(calendarUnique);
		if (!event[0]) {
			$(document).Toasts("create", {
				class: "bg-danger",
				title: "Error subscribing to event",
				body: "There was an error trying to subscribe to that event. Please contact the engineer at wwsu4@wright.edu.",
				autohide: true,
				delay: 20000,
			});
			return;
		} else {
			event = event[0];
		}

		this.checkStatus(() => {
			this.modals.askByUnique.iziModal("open");

			$(this.modals.askByUnique.body).alpaca({
				schema: {
					title: `Choose Notification Type for ${event.name}`,
					type: "object",
					properties: {
						notificationType: {
							type: "string",
							required: true,
							enum: ["Event", "Schedule", "Occurrence"],
						},
					},
				},
				options: {
					form: {
						buttons: {
							submit: {
								title: "Subscribe",
								click: (form, e) => {
									form.refreshValidationState(true);
									if (!form.isValid(true)) {
										if (this.manager.has("WWSUehhh"))
											this.manager.get("WWSUehhh").play();
										form.focus();
										return;
									}
									let value = form.getValue();

									// Process what kind of subscription we want
									let subtype = ``;
									let description = ``;

									switch (value.notificationType) {
										case "Event":
											subtype = `${event.calendarID}`;
											description = `${event.name} (all broadcasts)`;
											break;
										case "Schedule":
											subtype = `${event.calendarID}-null-${event.scheduleID}`;
											description = `${event.name} ${this.manager
												.get("WWSUcalendar")
												.generateScheduleText(event)} (station time).`;
											break;
										case "Occurrence":
											subtype = calendarUnique;
											description = `${event.name} for ${moment(
												event.start
											).format(
												"LLLL"
											)} (station time) (will be auto-removed when the broadcast airs on this date/time).`;
											break;
									}

									this.subscribe("calendar", subtype, description);
									this.modals.askByUnique.iziModal("close");
								},
							},
						},
					},
				},
			});

			$(this.modals.askByUnique.body)
				.prepend(`<div class="callout callout-info">
      <h3>Explanation of subscription types</h3>
      <p><strong>Event:</strong> Receive all push notifications for ${
				event.name
			} (when it goes on the air, re-schedules, cancellations, news, etc)</p>
        <p><strong>Occurrence:</strong> Only receive push notifications specifically for the broadcast date/time of <strong>${moment(
					event.start
				).format(
					"LLLL"
				)}</strong> (station timezone). You will be auto-unsubscribed after the broadcast airs on this date/time.</p>
        <p><strong>Schedule:</strong> Receive push notifications for ${
					event.name
				} only for the ${
				event.type
			} schedule (station timezone)... ${this.manager
				.get("WWSUcalendar")
				.generateScheduleText(event)}</p>
      </div>`);
		});
	}
}
